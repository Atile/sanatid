# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
# Don't declare `role :all`, it's a meta role
role :app, %w{deployer@95.85.60.40}
role :web, %w{deployer@95.85.60.40}
role :db,  %w{deployer@95.85.60.40}

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server
# definition into the server list. The second argument
# something that quacks like a hash can be used to set
# extended properties on the server.
set :app_environment, 'production'
set :domain, 'sanatid.dk'
set :deploy_to, "/home/deployer/sanatid/#{fetch :app_environment}/sanatid"

SSHKit.config.command_map[:rake] = "bundle exec rake"

server '95.85.60.40', user: 'deployer', roles: %w{web app}

set :ssh_options, {
  forward_agent: true,
  port: 58222
}
