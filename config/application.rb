require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

CONF = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../application.yml', __FILE__))))

module Sanatid
  class Application < Rails::Application
    config.time_zone = 'Copenhagen'
    config.active_record.default_timezone = :local
    config.i18n.default_locale = :da
    config.i18n.load_path += Dir[Rails.root.join('config/locales/config', '*.yml').to_s]

    # Auoload modules in lib folder.
    config.autoload_paths += Dir["#{config.root}/lib/**/"]
    config.assets.precompile += %w(application-primary.js application-staff.js application-primary.css application-staff.css da.js leaflet.js nicEdit/nicEdit.js)

    config.assets.paths << Rails.root.join('vendor', 'assets', 'bower_components')

    config.middleware.use Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get]
      end
    end
  end
end
