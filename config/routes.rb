Sanatid::Application.routes.draw do
  class FormatTest
    attr_accessor :mime_type

    def initialize(format)
      @mime_type = Mime::Type.lookup_by_extension(format)
    end

    def matches?(request)
      request.format = :html unless request.format == Mime::Type.lookup_by_extension(:json)
      request.format == mime_type
    end
  end

  get 'robots.:format', to: 'application#robots'

  constraints subdomain: ['staff', 'devstaff'] do
    scope module: 'staff', as: 'staff' do
      root to: 'staff_application#index'

      get 'login', to: 'sessions#new', as: 'login'
      get 'logout', to: 'sessions#destroy', as: 'logout'

      resources :sessions
      resources :companies do
        resources :product_subscriptions
        resources :invoices
        get 'cancel_subscription/:id', to: 'product_subscriptions#cancel_subscription', as: 'cancel_subscription'
      end
      get 'maps/companies', to: 'companies#maps', as: 'company_map'
      resources :customers
      resources :staffs
      resources :service_fields
      resources :services
      resources :blogs
      resources :pages
      resources :staffs
      resources :submissions
      resources :newsletter_subscribers
      resources :products
      resources :emails
      resources :product_subscriptions
      resources :clips
      resources :product_families
      resources :coupons
      get 'my_leads', to: 'leads#my_leads'
      get 'leads/signups', to: 'leads#signups', as: 'leads_signups'
      post 'leads/:id/send_presentation', to: 'leads#send_presentation', as: 'lead_send_presentation'
      post 'leads/assign', to: 'leads#assign_leads', as: 'assign_leads'
      resources :leads
      resources :lead_events
      resources :reminders
      resources :uploads
    end
  end

  get 'terms/company', to: 'api/payments#terms_company'
  get 'product_subscriptions/invoice/:id', to: 'api/product_subscriptions#invoice'
  get 'sitemap.xml', to: 'api/sitemaps#index', as: 'sitemap', defaults: { format: :xml }
  get '*path', :to => 'application#index', :constraints => FormatTest.new(:html)
  get '/', :to => 'application#index', :constraints => FormatTest.new(:html)

  namespace :api do
    resources :service_fields, defaults: { format: 'json' }
    resources :services, defaults: { format: 'json' }
    resources :customers, defaults: { format: 'json' }
    resources :companies, defaults: { format: 'json' }
    get '/frontpage_promotions/:id', to: 'companies#frontpage_promotions'
    get '/flow_promotions', to: 'companies#flow_promotion'
    resources :vouchers, defaults: { format: 'json' }
    resources :clips, defaults: { format: 'json' }
    resources :newsletter_subscribers, defaults: { format: 'json' }
    resources :blogs, defaults: { format: 'json' }
    resources :pages, defaults: { format: 'json' }
    resources :staffs, defaults: { format: 'json' }
    resources :submissions, defaults: { format: 'json' }
    resources :products, defaults: { format: 'json' }
    resources :product_families, defaults: { format: 'json' }
    resources :product_subscriptions, defaults: { format: 'json' }
    resources :messages, defaults: { format: 'json' }
    resources :reviews, defaults: { format: 'json' }
    resources :commission_lines, defaults: { format: 'json' }
    resources :menu_items, defaults: { format: 'json' }
    resources :facility_images, defaults: { format: 'json' }
    resources :bookings, defaults: { format: 'json' }

    resources :payments, defaults: { format: 'json' }
    match '/payment' => 'payments#dispatch_handler', via: 'post'
    match '/payment/list_invoices/:id' => 'payments#list_invoices', via: 'get'

    # Product Subscription.
    post '/product_subscriptions/change_subscription_product/:id', to: 'product_subscriptions#change_subscription_product'
    post '/product_subscriptions/cancel_subscription/:id', to: 'product_subscriptions#cancel_subscription'
    post '/product_subscriptions/signup_subscription/:id', to: 'product_subscriptions#signup_subscription'
    post '/product_subscriptions/reactivate_subscription/:id', to: 'product_subscriptions#reactivate_subscription'

    post '/demo/init', to: 'demo#init_demo'

    resources :sessions, except: [:new, :edit, :destroy]
    post 'session' => 'sessions#create', defaults: { format: :json }
    post 'session/reset' => 'sessions#reset', defaults: { format: :json }
  end
end
