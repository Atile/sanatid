Sanatid::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false


  # Global enable/disable all memcached usage
  config.perform_caching = false
  # Disable/enable fragment and page caching in ActionController
  config.action_controller.perform_caching = false
  # The underlying cache store to use.
  #config.cache_store = :dalli_store, 'localhost:11211'
  # The session store is completely different from the normal data cache
  #config.session_store = :dalli_store, 'localhost:11211'

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  config.action_mailer.default_options = {from: 'Sanatid Budbringer <budbringer@sanatid.dk>', template_path: 'emails'}
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.default_url_options = { host: 'dev.sanatid.dk:3000' }
  config.action_mailer.asset_host = 'dev.sanatid.dk:3000'
  config.action_mailer.smtp_settings = { address: 'localhost',
                                         port: 1025 }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  config.ember.variant = :development

  # Serve assets faster.
  config.middleware.insert 0, TurboDevAssets

  config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx
end
