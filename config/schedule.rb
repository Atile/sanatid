set :output, "#{path}/log/cron.log"

every :day, at: '11:15 PM' do
  runner 'ProductSubscription.refresh_subscriptions'
end

every :day, at: '00:00 AM' do
  runner 'Clip.flag_old_clips_as_confirmed'
end

every :day, at: '10:00 PM' do
  runner 'ProductSubscription.notify_nearly_expired'
end

every :month, at: 'start of the month at 1:00am' do
  runner 'ProductSubscription.collect_monthly_commission'
end

if @environment == 'production'
  every 10.minutes do
    runner 'Reminder.notify'
  end
end

if @environment == 'demo'
  every 20.minutes do
    rake 'cleanup_demo_content'
  end
end