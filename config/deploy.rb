# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'sanatid'
set :repo_url, 'git@bitbucket.org:Atile/sanatid.git'
set :use_sudo, false

# Default branch is :master
ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/application.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system, public/uploads}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute 'sudo /etc/init.d/nginx restart'
    end
  end
  after 'nginx:setup_conf', :restart

  desc 'Run rake task, which updates system mails'
  task :init_emails do
    on roles(:web) do
      with rails_env: fetch(:app_environment) do
        within "#{fetch :deploy_to}/current" do
          execute :rake, 'email:init_system_emails'
        end
      end
    end
  end
  after :restart, :init_emails

  desc 'Clear memcached'
  task :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      within release_path do
        with rails_env: fetch(:app_environment) do
          execute :rake, 'cache:clear'
        end
      end
    end
  end
  after :restart, :clear_cache

  desc 'Install bower'
  task :bower_install do
    on roles(:all) do
      within release_path do
        execute :rake, 'bower:install'
      end
    end
  end
  before 'deploy:assets:precompile', :bower_install


  task :restart_delayed_jobs do
    invoke 'delayed_job:restart'
  end
  after 'deploy:publishing', 'restart_delayed_jobs'
end
