module Slugifier
  def generate_slug(string)
    # Clone so we don't overwrite the provided value.
    string = string.dup
    string.gsub!(/\s/, '-')
    string.gsub!(/ø/, 'o')
    string.gsub!(/æ/, 'ae')
    string.gsub!(/å/, 'a')
    string.gsub!(/[^0-9a-zA-Z-]/, '')
    string.downcase
  end

  def decode_slug(string)
    string.gsub!(/\-/, ' ')
    string.gsub!(/oe/, 'ø')
    string.gsub!(/ae/, 'æ')
    string.gsub!(/aa/, 'å')
    string
  end
end