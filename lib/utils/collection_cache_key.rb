module CollectionCacheKey
  class << self
    def cache_key_for(*models)
      max_updated_at = nil
      models.each do |model|
        scope = Object.const_get model.classify
        max = scope.maximum(:updated_at).try(:to_s, :number)
        max_updated_at = max if max_updated_at.nil? || max > max_updated_at
      end
      "#{models.first.downcase}/all-#{max_updated_at}"
    end
  end
end