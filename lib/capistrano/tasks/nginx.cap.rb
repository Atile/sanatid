namespace :nginx do
  task :setup_conf do
    on roles(:web) do
      erb = File.read(File.expand_path("../templates/nginx_conf.erb", __FILE__))

      puts Dir.pwd
      # Compile and save locally.
      tmpConf = File.open('tmp/nginx_conf', 'w+') { |file| file.puts(ERB.new(erb).result(binding)) }
      # Upload file.
      upload! 'tmp/nginx_conf', '/home/deployer/tmp'
      # Remove local tmp file.
      #tmpConf.delete

      # move conf in place on server.
      execute "sudo mv /home/deployer/tmp/nginx_conf /etc/nginx/sites-enabled/#{fetch :application}-#{fetch :app_environment}"

      execute "rm -f /etc/nginx/sites-enabled/default"
    end
  end
  after "deploy:publishing", :setup_conf
end