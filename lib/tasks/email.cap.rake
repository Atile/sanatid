namespace :email do
  task init_system_emails: :environment do
    mails = YAML.load_file("#{Rails.root}/lib/tasks/system_emails.yml")
    mails['system_emails'].each do |email, properties|
      current_mail = Email.find_by_machine_name(email)
      # Create mail, if not in the system.
      if current_mail.nil?
        puts "Created new system email: #{email}"
        Email.create machine_name: email, description: properties['description']
        # If the mail was found, update the description, if is has changed.
      elsif current_mail.present? && current_mail.description != properties['description']
        puts "Updated system email: #{email}"
        current_mail.description = properties['description']
        current_mail.save
      end
    end

    # Delete mails which is not among the system emails.
    machine_names = mails['system_emails'].collect { |email, properties| email }
    deprecated_mails = Email.where.not(machine_name: machine_names)
    deprecated_mails.each do |email|
      puts "Deleted system email: #{email.machine_name}"
      email.destroy
    end
  end
end