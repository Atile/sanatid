namespace :cache do
  desc 'Clear memcached'
  task clear: :environment do
    Rails.cache.clear
  end
end