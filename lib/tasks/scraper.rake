desc 'Scrape stuff'
task scraper: :environment do
  require 'nokogiri'
  require 'mechanize'

  agent = Mechanize.new

  krak = [
    {service_field: 'Massage', url: 'http://www.krak.dk/mass%C3%B8rer/p:1/s%C3%B8g.cs'},
    {service_field: 'Coaching', url: 'http://www.krak.dk/coaches/p:1/s%C3%B8g.cs'},
    {service_field: 'Terapi', url: 'http://www.krak.dk/psykologer/p:1/s%C3%B8g.cs'},
    {service_field: 'Terapi', url: 'http://www.krak.dk/terapi/p:1/s%C3%B8g.cs'},
    {service_field: 'Terapi', url: 'http://www.krak.dk/akupunktur/p:1/s%C3%B8g.cs'},
    {service_field: 'Terapi', url: 'http://www.krak.dk/kiropraktor/p:1/s%C3%B8g.cs'},
    {service_field: 'Terapi', url: 'http://www.krak.dk/psykolog/p:1/s%C3%B8g.cs'},
    {service_field: 'Terapi', url: 'http://www.krak.dk/psykoterapeuter/p:1/s%C3%B8g.cs'}
  ]

  krak.each do |search|
    agent.get(search[:url])

    page_count = agent.page.search('.page-count').text[/\d+/].to_i

    (1..page_count).each do |i|
      agent.get(search[:url].gsub!(/p:\d+/, "p:#{i}"))

      agent.page.search('#hit-list li article').each do |profile|

        new_lead = Lead.new category: 'scraper'

        # Company name.
        new_lead.company_name = profile.search('.profile-page-link').text.strip

         # Phone.
        new_lead.phone = profile.search('.hit-phone-number').text.strip

        # City.
        new_lead.city = profile.search('.address-line').text.strip

        # Address.
        new_lead.address = profile.search('.street-address').text

        # Homepage.
        new_lead.homepage = profile.search('.hit-homepage a').text

        begin
          cords = JSON.parse(profile.search('.hit-company-location').first['data-coordinate'])
          new_lead.longitude = cords['coordinate']['lon']
          new_lead.latitude  = cords['coordinate']['lat']
        rescue
          puts 'A company did\'t have coordinates'
        end

        # Service field.
        new_lead.service_field = ServiceField.find_by_name(search[:service_field])

        if new_lead.company_name.present? && new_lead.address.present? && new_lead.city.present?
          unless Lead.find_by_homepage(new_lead.homepage) && Lead.find_by_phone(new_lead.phone)
            new_lead.save
          end
        end
      end

      puts search[:url]
    end
  end
end