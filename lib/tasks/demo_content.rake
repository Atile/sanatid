desc 'Generates demo content on demo environment'
task demo_content: :environment do
  require 'json'

  # If demo user is already there, we just want to reset password and skip content creation.
  company = Company.find_by_email(ENV['demo_email'])
  skip_generation = company ? true : false

  # Create company demo tester.
  company ||= Company.new name: ENV['demo_name'], email: ENV['demo_email'], company_name: ENV['demo_company_name'], address: 'Langdraget 27, Vanløse, Danmark', subscription_status: 'active'
  company.description = 'Velkommen til vores demo profil, hos Santid.dk'
  company.opening_hours = {"monday"=>"8:00-16:00", "tuesday"=>"8:00-16:00", "wednesday"=>"8:00-16:00", "thursday"=>"8:00-16:00", "friday"=>"8:00-16:00", "saturday"=>"Lukket", "sunday"=>"Lukket"}
  company.product_subscriptions << ProductSubscription.new(active: true, product: Product.last)
  Service.all.each {|service| company.services << service }

  password = Digest::SHA2.hexdigest("#{Time.now} - #{ENV['demo_name']} - DITANAS")[0..20]
  company.password = password
  company.password_confirmation = password

  company.save

  if skip_generation
    credentials = { password: password, email: ENV['demo_email'] }
    puts credentials.to_json
    # Skip exits the rake script.
    next
  end

  # Create customers.
  customer_1 = Customer.find_by_email('cu1@dev.com') || Customer.create(name: 'Torben Hansen', email: 'cu1@dev.com', phone: 18282828, password: '123', password_confirmation: '123')
  customer_2 = Customer.find_by_email('cu2@dev.com') || Customer.create(name: 'Frank Nielsen', email: 'cu2@dev.com', phone: 28282828, password: '123', password_confirmation: '123')
  customer_3 = Customer.find_by_email('cu3@dev.com') || Customer.create(name: 'Mads Andersen', email: 'cu3@dev.com', phone: 38282828, password: '123', password_confirmation: '123')

  # Create 3 open jobs.
  description = 'Denne opgave vil typisk være beskrevet med disse og derefter en beskrivelse. Klientens beskrivelser kan varierer meget. Derfor har vi gjort det muligt for din praksis at stille ind med ét spørgsmål til klienten, så i han få svar på ting i føler i mangler for at give et kvalificeret tilbud på opgaven.'
  Job.create description: description, customer: customer_1, available_next: true, address: 'Marielystvej 14, 2000 Frederiksberg, Danmark', service_field: ServiceField.find_by_name('Massage'), state: 'open'
  Job.create description: description, customer: customer_3, available_next: true, address: 'Peter Bangs Vej 53, 2000 Frederiksberg, Danmark', service_field: ServiceField.find_by_name('Coaching'), state: 'open'
  Job.create description: description, customer: customer_2, available_from: 2.days.from_now, address: 'Peter Bangs Vej 53, 2000 Frederiksberg, Danmark', service_field: ServiceField.find_by_name('Terapi'), state: 'open'

  # Create 2 Reserved jobs.
  description = 'Denne opgave er reserveret til dig, dette sker enten ved at du har stillet et spørgsmål til opgaven, eller at kunden har fundet din profil og meddelt at et budene skal være fra din praksis'

  reserved_job_1 = Job.create description: description, customer: customer_3, address: 'Peter Bangs Vej 53, 2000 Frederiksberg, Danmark', available_next: true, service_field: ServiceField.find_by_name('Coaching'), state: 'open'
  Reservation.create job: reserved_job_1, company: company, reservation_type: 'job_question', expires_at: 3.hours.from_now

  reserved_job_2 = Job.create description: description, customer: customer_2, address: 'Peter Bangs Vej 53, 2000 Frederiksberg, Danmark', available_next: true, service_field: ServiceField.find_by_name('Terapi'), services: [Service.find_by_name('Parterapi')], state: 'open'
  Reservation.create job: reserved_job_2, company: company, reservation_type: 'job_question', expires_at: 12.hours.from_now

  # Create 2 won jobs.
  description = 'Dette er en vundet opgave, og du har her overblik over opgaven, korrespondance mellem dig og kunden, samt klippekort status'
  won_job_1 = Job.create description: description, customer: customer_3, available_next: true, service_field: ServiceField.find_by_name('Terapi'), services: [Service.find_by_name('Akupunktur'), Service.find_by_name('Ergoterapi')], address: 'Peter Bangs Vej 53, 2000 Frederiksberg, Danmark', accepted_at: 1.week.ago, state: 'accepted'
  won_job_1_offer = Offer.create company: company, job: won_job_1, offer_type: 'voucher', price: (rand(5000) + 2000)
  won_job_1_offer.voucher = Voucher.create(customer: customer_1, company: company, clip_count: (rand(8) + 2) )
  won_job_1.winning_offer = won_job_1_offer
  won_job_1.save

  won_job_2 = Job.create description: description, customer: customer_1, available_from: 1.day.from_now, service_field: ServiceField.find_by_name('Coaching'), services: [Service.find_by_name('Livscoaching'), Service.find_by_name('NLP Coaching')], address: 'Peter Bangs Vej 53, 2000 Frederiksberg, Danmark', accepted_at: Time.now, state: 'accepted'
  won_job_2_offer = Offer.create company: company, job: won_job_2, offer_type: 'voucher', price: (rand(5000) + 2000)
  won_job_2_offer.voucher = Voucher.create(customer: customer_2, company: company, clip_count: (rand(8) + 2) )
  won_job_2.winning_offer = won_job_2_offer
  won_job_2.save

  # Create 1 lost job.
  description = 'Denne opgave er tabt, men der er stadig noget at hente. Her kan du se hvordan du klarede dig mod de andre 3 bud som kunden modtog. Det kan være med til at forbedre dine konkurrence evner.'

  lost_job = Job.create description: description, customer: customer_1, available_from: 1.day.from_now, service_field: ServiceField.find_by_name('Massage'), address: 'Peter Bangs Vej 53, 2000 Frederiksberg, Danmark', state: 'accepted', accepted_at: Time.now

  company_1 = Company.find_by_name('Klaas Fisker') || Company.create(name: 'Klaas Fisker', email: 'com1@dev.com', phone: 11111111, company_name: 'Demo praksis 1', address: 'Frederikssunds vej 243, 2700', password: '123', password_confirmation: '123')
  Service.all.each {|service| company_1.services << service }
  company_1_offer = Offer.create company: company_1, job: lost_job, offer_type: 'voucher', price: 5000
  company_1_offer.voucher = Voucher.create company: company_1, clip_count: 8
  company_1_offer.save

  company_2 = Company.find_by_name('Jonas Larsen') || Company.create(name: 'Jonas Larsen', email: 'com2@dev.com', phone: 22222222, company_name: 'Demo praksis 2', address: 'Peter Bangs Vej 87, 2000 Frederiksberg', password: '123', password_confirmation: '123')
  Service.all.each {|service| company_2.services << service }
  company_2_offer = Offer.create company: company_2, job: lost_job, offer_type: 'voucher', price: 8000
  company_2_offer.voucher = Voucher.create company: company_2, clip_count: 10
  company_2_offer.save

  demo_company_offer = Offer.create company: company, job: lost_job, offer_type: 'voucher', price: 10000
  demo_company_offer.voucher = Voucher.create company: company_2, clip_count: 10
  demo_company_offer.save

  lost_job.winning_offer = company_1_offer
  company_1_offer.voucher.customer = customer_1
  company_1_offer.save

  lost_job.save

  # Create 2 jobs, with active offers on.

  description = 'Denne opgave har du allerede budt for, og kan derfor holde øje med det under opgave fanen.'

  job_1 = Job.create description: description, customer: customer_1, available_next: true, address: 'Nørrebrogade 42', service_field: ServiceField.find_by_name('Massage'), services: [Service.find_by_name('Thaimassage')], state: 'open'
  job_1_offer_1 = Offer.create company: company, job: job_1, offer_type: 'voucher', price: 5000
  job_1_offer_1.voucher = Voucher.create company: company, clip_count: 8
  job_1_offer_2 = Offer.create company: company, job: job_1, offer_type: 'single', price: 800
  job_1_offer_2.voucher = Voucher.create company: company, clip_count: 1

  job_2 = Job.create description: description, customer: customer_2, available_next: true, address: 'Nørrebrogade 42', service_field: ServiceField.find_by_name('Massage') , services: [Service.find_by_name('Fodmassage')], state: 'open'
  job_2_offer_1 = Offer.create company: company, job: job_2, offer_type: 'voucher', price: 4000
  job_2_offer_1.voucher = Voucher.create company: company, clip_count: 2
  job_2_offer_2 = Offer.create company: company, job: job_2, offer_type: 'single', price: 699.95
  job_2_offer_2.voucher = Voucher.create company: company, clip_count: 1

  credentials = { password: password, email: ENV['demo_email'] }
  puts credentials.to_json
end

desc 'Cleanup demo content'
task cleanup_demo_content: :environment do
  # Time to live, for content.
  ttl = 20.minutes.ago

  # Destroy stuff older than TTL.
  models = %w{ApiKey Job Offer Voucher Clip JobQuestion Reservation Company Customer}
  models.each { |m| m.constantize.where('created_at < ?', ttl).destroy_all }
end