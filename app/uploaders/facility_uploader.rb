# encoding: utf-8

class FacilityUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :compressed do
    process quality: 70
    process resize_to_fill: [900, 600]

  end

  version :thumb do
    process resize_to_fill: [260, 80]
  end
end
