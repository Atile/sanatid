# encoding: utf-8

class ContentImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  storage :file

  def store_dir
    "uploads/content/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :compressed do
    process :quality => 70
  end
end
