class Api::ProductFamiliesController < ApplicationController
  respond_to :json

  def index
    respond_with ProductFamily.all
  end
end
