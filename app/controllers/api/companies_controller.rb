class Api::CompaniesController < ApplicationController
  before_filter :ensure_authenticated_user, only: [:update]
  before_filter :ensure_active_token, only: [:show]
  respond_to :json

  def index
    if params[:slug].present?
      company = Company.active.where(slug: params[:slug]).presence || Lead.public.where(slug: params[:slug])

      return respond_with company
    end

    if params[:service_field].present? && params[:lat].present? && params[:lon].present?
      cords = params[:lat], params[:lon]

      companies = Company.active
        .joins(:service_fields)
        .where(service_fields: {name: 'Massage'} )
        .near(cords, 80)
        .limit(15)

      companies_arr = ActiveModel::Serializer.build_json(self, companies, cords: cords).as_json

      final_json = Hash.new
      final_json['companies'] = companies_arr['companies']

      return render json: final_json.to_json

    elsif params[:lat].present? && params[:lon].present?
      cords     = params[:lat], params[:lon]
      companies = Company.active.near(cords, 80).limit(15)

      companies_arr = ActiveModel::Serializer.build_json(self, companies, cords: cords).as_json

      final_json = Hash.new
      final_json['companies'] = companies_arr['companies']

      return render json: final_json.to_json
    end

    page_limit = params[:limit] || 20
    respond_with Company.active.newest.page(params[:page]).per(page_limit), each_serializer: BasicCompanySerializer
  end

  def show
    respond_with Company.active.find(params[:id])
  end

  # When a company wants to be created, we create a lead, which our salesmen then can contact.
  def create
    company = Company.new company_params
    # Generate a new password for the company.
    password = Digest::SHA2.hexdigest(rand(1000000).to_s)[0..8]
    company.password = company.password_confirmation = password

    if company.save
      # Send welcome mail to company, with login information.
      CompanyMailer.delay(priority: 10).welcome_company(company, password)
      respond_with :api, company
    else
      respond_with company
    end
  end

  def frontpage_promotions
    first_row = Company.find_by_frontpage_promotion(1, 4)
    second_row = Company.find_by_frontpage_promotion(2, 4)
    third_row = Company.find_by_frontpage_promotion(3, 4)
    rows = first_row + second_row + third_row
    is_clear = rows.empty?

    frontpage_promotions = {
      frontpage_promotion: {
        id: 1,
        # isEmpty is reserved in emberjs.
        is_clear: is_clear,
        first_row_ids: first_row.map(&:id),
        second_row_ids: second_row.map(&:id),
        third_row_ids: third_row.map(&:id)
      },
      companies: ActiveModel::ArraySerializer.new(rows, each_serializer: BasicCompanySerializer)
    }

    respond_with frontpage_promotions
  end

  def update
    company = Company.find(params[:id])

    if company == current_user
      company.update_attributes(company_params)
    end

    respond_with company
  end

  private

  def company_params
    params.require(:company).permit(
      :name,
      :logo,
      :email,
      :phone,
      :description,
      :password,
      :password_confirmation,
      :address,
      :company_name,
      :company_email,
      :cvr,
      payment_methods: [
        [:name, :enabled]
      ],
      opening_hours: [
        :monday,
        :tuesday,
        :wednesday,
        :thursday,
        :friday,
        :saturday,
        :sunday
      ],
      service_field_ids: []
    )
  end
end
