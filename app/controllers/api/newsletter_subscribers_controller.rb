class Api::NewsletterSubscribersController < ApplicationController
  respond_to :json
  before_filter :ensure_authenticated_user, only: [:show]

  @@mailchimp = Gibbon::API.new(CONF['mailchimp']['api_key'], throws_exceptions: false) if Rails.env.production?

  def show
    respond_with NewsletterSubscriber.find(params[:id])
  end

  def create
    unless newsletter_subscriber_params[:user_id]
      subscriber = NewsletterSubscriber.where(email: newsletter_subscriber_params[:email]).first_or_create
      @@mailchimp.lists.subscribe(id: CONF['mailchimp']['anonymous_list_id'], email: {email: newsletter_subscriber_params[:email]}, double_optin: false) if Rails.env.production?
    else
      subscriber = NewsletterSubscriber.where(newsletter_subscriber_params).first_or_create
      # Company : Customer
      id = subscriber.user.company? ? CONF['mailchimp']['company_list_id'] : CONF['mailchimp']['customer_list_id']
      @@mailchimp.lists.subscribe(id: id, email: {email: subscriber.email}, merge_vars: {FNAME: subscriber.user.name[/[øØåÅÆæa-zA-Z]+/], LNAME: subscriber.user.name[/(?<=\s)[øØåÅÆæa-zA-Z\s]+/]}, double_optin: false) if Rails.env.production?
    end

    if subscriber.save
      NewsletterMailer.delay(priority: 10).newsletter_confirmation(subscriber.email)
      render json: { newsletter_subscriber: subscriber }, status: 201
    else
      render json: { error: subscriber.errors }, status: 400
    end
  end

  def destroy
    # Either find by id, or email.
    if params[:newsletter_subscriber].present?
      subscriber = NewsletterSubscriber.find_by_email(newsletter_subscriber_params[:email])
      list_id = CONF['mailchimp']['anonymous_list_id']
    else
      subscriber = NewsletterSubscriber.find(params[:id])
      list_id = subscriber.user.company? ? CONF['mailchimp']['company_list_id'] : CONF['mailchimp']['customer_list_id']
    end

    @@mailchimp.lists.unsubscribe(:id => list_id, :email => {:email => subscriber.email}, send_notify: false) if Rails.env.production?

    if subscriber.user.present?
      if current_user == subscriber.user
        subscriber.destroy
        head :ok and return
      else
        head :not_authorized and return
      end
    else
      subscriber.destroy
      render json: { status: 'success' }, status: 200
    end
  end

  private

  def newsletter_subscriber_params
    params.required(:newsletter_subscriber).permit(:email, :user_id)
  end
end
