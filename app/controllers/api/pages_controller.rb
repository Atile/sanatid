class Api::PagesController < ApplicationController
  respond_to :json

  def index
    # Support request with slugs instead of id.
    if params[:slug].present?
      return respond_with Page.published.where('slug ILIKE ?', params[:slug])
    end

    if %w(footer guide).include? params[:category]
      respond_with Page.published.where(category: params[:category]).load
    else
      respond_with Page.published.load
    end
  end

  def show
    respond_with Page.published.find(params[:id])
  end
end
