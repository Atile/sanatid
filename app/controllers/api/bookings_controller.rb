class Api::BookingsController < ApplicationController
  respond_to :json

  def create
    mailchimp = Gibbon::API.new(CONF['mailchimp']['api_key'], throws_exceptions: false) if Rails.env.production?

    booking = Booking.new(booking_params)

    if params[:booking][:newsletter]
      NewsletterSubscriber.where(email: booking.email).first_or_create
      mailchimp.lists.subscribe(id: CONF['mailchimp']['anonymous_list_id'], email: {email: booking.email}, double_optin: false) if Rails.env.production?
    end

    if booking.save
      # Send mail to customer, and to company
      BookingMailer.delay(priority: 10).company_new_booking(booking)
      BookingMailer.delay(priority: 10).customer_booking_confirmation(booking)

      respond_with :api, booking
    else
      respond_with booking
    end
  end

  private

  def booking_params
    params.require(:booking).permit(:name, :email, :phone, :comment, :menu_item_id, :company_id, :starts_at)
  end
end
