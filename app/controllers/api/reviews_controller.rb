class Api::ReviewsController < ApplicationController
  before_filter :ensure_authenticated_user, only: [:create, :update]
  respond_to :json

  def create
    if current_user.customer?
      review = Review.new(review_params)
      review.customer = current_user # Make sure customer is, who he say he is.

      if review.job.customer == current_user && review.save
        ReviewMailer.delay.new_review(review)
        respond_with :api, review, status: :created
      else
        render json: { errors: review.errors }, status: 403
      end
    end
  end

  def show
    respond_with Review.find(params[:id])
  end

  private

  def review_params
    params.require(:review).permit(:company_id, :customer_id, :job_id, :content, :rating)
  end
end
