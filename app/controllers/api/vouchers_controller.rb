class Api::VouchersController < ApplicationController
  before_filter :ensure_authenticated_user
  respond_to :json

  def index
    respond_with current_user.vouchers
  end

  def create
    voucher = Voucher.new voucher_params
    voucher.company = current_user

    if voucher.save
      render json: { voucher: voucher }, status: 201
    else
      render json: { errors: voucher.errors.messages }, status: 422
    end
  end

  def show
    voucher = Voucher.find(params[:id])
    # Show only if user is related to voucher.
    if current_user == voucher.customer || voucher.company == current_user
      respond_with voucher
    else
      head :unauthorized
    end
  end

  private

  def voucher_params
    params.required(:voucher).permit(:customer_id, :company_id, :clip_count)
  end
end
