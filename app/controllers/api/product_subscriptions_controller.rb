class Api::ProductSubscriptionsController < ApplicationController
  respond_to :json
  before_filter :ensure_authenticated_user, except: :invoice

  def index
    respond_with ProductSubscription.where(company: current_user)
  end

  def show
    subscription = ProductSubscription.find(params[:id])

    if subscription.company == current_user
      respond_with subscription
    else
      head 403
    end
  end

  def signup_subscription
    product = Product.find(params[:id])

    # Setup a new subscription, according to the selected product.
    subscription = ProductSubscription.new(company: current_user, product: product, active: true)
    subscription.renewed_at = Time.now.beginning_of_day
    subscription.next_renewal_at = product.billing_period_in_days.days.from_now.end_of_day

    if subscription.save
      current_user.active_invoice.add_line_item(subscription)
      # Flag company as subscription active, if the subscription is fundamental.
      current_user.update_attributes(subscription_status: 'active') if subscription.fundamental?

      # Respond with the current user, so ember will update the model, with the new subscription.
      respond_with :api, current_user
    else
      render json: subscription.errors.messages, status: 404
    end
  end

  def change_subscription_product
    subscription = ProductSubscription.find(params[:id])
    if subscription.company == current_user
      product = Product.find(product_subscription_params[:product_id])
      # Only change product, if its the same product family.
      if product.product_family == subscription.product.product_family
        # Alter the current invoice_line_item, to match the time of the previous product.
        current_user.active_invoice.replace_line_item(subscription.invoice_line_items.last)
        # Update the subscription product.
        subscription.update_attributes(product: product)
        # Finaly add a new invoice line for that new product.
        current_user.active_invoice.add_line_item(subscription)

        respond_with :api, current_user and return
      end
    end
    head :not_found
  end

  def reactivate_subscription
    subscription = ProductSubscription.find(params[:id])
    if subscription.company == current_user

      if subscription.update_attributes(expires_at: nil, next_renewal_at: subscription.expires_at)
        respond_with :api, current_user
      else
        head :not_found
      end
    end
  end

  def cancel_subscription
    subscription = ProductSubscription.find(params[:id])

    if subscription.company == current_user
      # Set the expire date, to next renewal date (ie. end of period).
      subscription.expires_at = subscription.next_renewal_at
      subscription.next_renewal_at = nil

      if subscription.save
        respond_with :api, current_user
      else
        head :not_found
      end
    end
  end

  def invoice
    @invoice = Invoice.find(params[:id])
    render layout: 'clean_slate'
  end

  private

  def product_subscription_params
    params.require(:product_subscription).permit(:product_id)
  end
end
