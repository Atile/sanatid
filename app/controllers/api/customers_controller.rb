class Api::CustomersController < ApplicationController
  before_filter :ensure_authenticated_user, only: [:show]
  before_filter :ensure_active_token, only: [:show]
  respond_to :json

  @@mailchimp = Gibbon::API.new(CONF['mailchimp']['api_key'], throws_exceptions: false) if Rails.env.production?

  def show
    respond_with Customer.find(params[:id])
  end

  def create
    customer = Customer.new(customer_params)
    if customer.save
      if params[:customer][:newsletter]
        NewsletterSubscriber.create user: customer, email: customer.email
        @@mailchimp.lists.subscribe(id: CONF['mailchimp']['customer_list_id'], email: {email: customer.email}, merge_vars: {FNAME: customer.name[/[øØåÅÆæa-zA-Z]+/], LNAME: customer.name[/(?<=\s)[øØåÅÆæa-zA-Z\s]+/]}, double_optin: false) if Rails.env.production?
      end

      CustomerMailer.delay.welcome_customer(customer)
      render json: { api_key: customer.session_api_key, user_type: 'customer' }, status: 201
    else
      render json: { errors: customer.errors.messages }, status: 422
    end
  end

  def update
    customer = Customer.find(params[:id])
    if customer == current_user && customer.update_attributes(customer_params)
      respond_with customer
    else
      render json: {errors: customer.errors}
    end
  end

  private

  def customer_params
    params.require(:customer).permit(:name, :email, :phone, :password, :password_confirmation)
  end
end
