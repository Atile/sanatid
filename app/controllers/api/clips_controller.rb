class Api::ClipsController < ApplicationController
  before_filter :ensure_authenticated_user
  respond_to :json

  def index
    if current_user.customer?
      if params[:filter].present?
        case params[:filter]
          when 'undecided'
            render json: Clip.undecided.joins(:voucher).where(vouchers: { customer_id: current_user })
        end
      else
        render json: Clip.joins(:voucher).where(vouchers: { customer_id: current_user }), status: 200
      end
    end
  end

  def create
    # Use only voucher id, as status might be malicious set.
    clip = Clip.new voucher_id: clip_params[:voucher_id]

    if current_user.company? && clip.save
      VoucherMailer.delay(priority: 5).clip_confirmation(clip)
      render json: { clip: clip }, status: 201
    end
  end

  def update
    if current_user.customer?
      # Exclude voucher_id, changing clip owner, is not allowed.
      render json: Clip.find(params[:id]).update_attributes(clip_params.except(:voucher_id))
    end
  end

  def clip_params
    params.require(:clip).permit(:status, :voucher_id)
  end
end
