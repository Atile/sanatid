class Api::BlogsController < ApplicationController
  require 'collection_cache_key'

  respond_to :json

  def index
    json = Rails.cache.fetch CollectionCacheKey::cache_key_for('Blog') do
      blogs = Blog.all
      ActiveModel::Serializer.build_json(self, blogs, {}).to_json
    end

    respond_with json
  end

  def show
    respond_with Blog.find params[:id]
  end
end

