class Api::SubmissionsController < ApplicationController
  def create
    sub = Submission.new submission_params

    if sub.save
      render json: {submission: sub}, status: 201
    else
      render json: {errors: sub.errors}, status: 400
    end
  end

  private

  def submission_params
    params.require(:submission).permit(:form, :name, :email, :subject, :message)
  end
end
