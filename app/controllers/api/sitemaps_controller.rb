class Api::SitemapsController < ApplicationController

  def index
    @pages = []
    ServiceField.published.each do |sf|
      @pages << "wiki/#{sf.slug}"
      sf.services.published.each do |s|
        @pages << "wiki/#{sf.slug}/#{s.slug}"
      end
    end

    Blog.published.each do |b|
      @pages << "blog/#{b.slug}"
    end

    Company.active.each do |c|
      @pages << "#{c.slug}"
    end

    Lead.public.active.each do |c|
      @pages << "#{c.slug}"
    end

    @host = request.host

    respond_to do |format|
      format.xml
    end
  end
end
