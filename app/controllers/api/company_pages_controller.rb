class Api::CompanyPagesController < ApplicationController
  before_filter :ensure_authenticated_user, except: :show
  respond_to :json

  def create
    page = CompanyPage.new company_page_params

    # No creating of pages, for other companies.
    company_page_params[:company_id] = current_user.id

    if page.save
      render json: { company_page: page }, status: 201
    end
  end

  def update
    page = CompanyPage.find params[:id]

    # No changing of owner.
    company_page_params[:company_id] = current_user.id

    if page.update_attributes company_page_params
      render json: { company_page: page }, status: 201
    end
  end

  def show
    respond_with CompanyPage.find(params[:id])
  end

  def destroy
    if CompanyPage.find(params[:id]).destroy
      head 204
    end
  end

  private

  def company_page_params
    params.require('company_page').permit(:title, :content, :weight, :parent_page, :company_id)
  end
end
