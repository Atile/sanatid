class Api::MessagesController < ApplicationController
  before_filter :ensure_authenticated_user
  respond_to :json

  def index
    job = Job.find(message_params[:job_id])
    if job && job.is_user_involved?(current_user)
      render json: job.messages
    else
      head 403
    end
  end

  def create
    job = Job.find(message_params[:job_id])
    # Make sure the the user is involved in the job, so user can't spam other users, which they have no relationship to.
    if job && job.is_user_involved?(current_user)
      message = Message.new(message_params)
      message.dispatcher = current_user

      if message.save
        # Notify by email, send mail depending on recipient user type.
        MessageMailer.delay(priority: 15).company_received_message(message) if message.recipient.company?
        MessageMailer.delay(priority: 15).customer_received_message(message) if message.recipient.customer?
        return respond_with :api, message, status: :created
      else
        return render json: { errors: message.errors }, status: 400
      end
    end

    head 403, content_type: 'text/html'
  end

  private

  def message_params
    params.require(:message).permit(:content, :recipient_id, :dispatcher_id, :job_id)
  end
end
