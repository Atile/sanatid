class Api::MenuItemsController < ApplicationController
  before_filter :ensure_authenticated_user
  respond_to :json

  def create
    if current_user.id == menu_item_params[:company_id].to_i
      respond_with :api, MenuItem.create(menu_item_params), status: 201
    else
      head 403
    end
  end

  def update
    menu_item = MenuItem.find(params[:id])

    if current_user == menu_item.company
      menu_item.update_attributes(menu_item_params)
      head 200
    else
      head 403
    end
  end

  def destroy
    menu_item = MenuItem.find(params[:id])

    if current_user == menu_item.company
      menu_item.destroy
      head 200
    else
      head 403
    end
  end

  private

  def menu_item_params
    params.require(:menu_item).permit(:name, :description, :price, :duration, :company_id)
  end
end
