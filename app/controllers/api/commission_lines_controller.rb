class Api::CommissionLinesController < ApplicationController
  before_filter :ensure_authenticated_user
  respond_to :json

  def index
    return respond_with current_user.commission_lines.order('created_at')
  end
end
