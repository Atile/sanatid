class Api::SessionsController < ApplicationController
  respond_to :json

  def create
    customer = Customer.find_by_email(params[:session][:email])
    if customer && customer.authenticate(params[:session][:password])
      render json: { api_key: customer.session_api_key, user_type: 'customer' }, status: 201
    else
      company = Company.find_by_email(params[:session][:email])
      if company && company.authenticate(params[:session][:password])
        return render json: { api_key: company.session_api_key, user_type: 'company' }, status: 201
      end

      render json: {error: 'invalid information'}, status: 401
    end
  end

  def reset
    user = User.find_by_email params[:session][:email]

    # Return if the user was not found in the system.
    return head :not_found unless user

    # Create a new password for the user.
    password = Digest::SHA2.hexdigest(rand(1000000).to_s)[0..8]
    user.password = user.password_confirmation = password

    if user.save
      # send email.
      UserMailer.delay.reset_password(user, password)
      render json: {status: 'success'}
    end
  end
end
