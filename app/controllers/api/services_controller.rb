class Api::ServicesController < ApplicationController
  respond_to :json

  def index
    # Support request with slugs instead of id.
    if params[:slug].present?
      return respond_with Service.published.where('slug ILIKE ?', params[:slug])
    end

    respond_with Service.published.load
  end

  def show
    respond_with Service.published.find(params[:id])
  end
end
