class Api::ServiceFieldsController < ApplicationController
  require 'collection_cache_key'
  respond_to :json

  def index
    # Support request with slugs instead of id.
    if params[:slug].present?
      respond_with ServiceField.published.where('slug ILIKE ?', params[:slug]) and return
    end

    json = Rails.cache.fetch CollectionCacheKey::cache_key_for('ServiceField', 'Service') do
      services_fields = ServiceField.includes(:services)
      ActiveModel::Serializer.build_json(self, services_fields, {}).to_json
    end

    respond_with json
  end

  def show
    respond_with ServiceField.published.find params[:id]
  end
end