class Api::FacilityImagesController < ApplicationController
  respond_to :json

  before_filter :ensure_authenticated_user

  def create
    image = FacilityImage.new(facility_image_params)
    image.company = current_user

    if image.save
      respond_with :api, image
    else
      render json: {errors: image.errors}
    end
  end

  def destroy
    image = FacilityImage.find(params[:id])

    if image.company == current_user && image.destroy
      head :ok
    end
  end

  private

  def facility_image_params
    params.require(:facility_image).permit(:name, :description, :image)
  end
end
