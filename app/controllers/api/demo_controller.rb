class Api::DemoController < ApplicationController
  respond_to :json

  def init_demo
    auth = `bundle exec rake demo_content demo_name="#{params[:name]}" demo_email=#{params[:email]} demo_company_name="#{params[:company_name]}" RAILS_ENV=demo`

    # Create a lead, if not already in the system.
    Lead.create(name: params[:name], email: params[:email], phone: params[:phone], company_name: params[:company_name], category: 'demo') unless Lead.find_by_email(params[:email])

    # Remove newline, added by output.
    render json: auth.delete!("\n")
  end
end
