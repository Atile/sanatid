class Api::StaffsController < ApplicationController
  respond_to :json

  def index
    respond_with Staff.active.published.order(:created_at).load
  end
end
