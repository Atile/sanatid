class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  serialization_scope :current_user

  def ensure_authenticated_user
    head :unauthorized unless current_user
  end

  def ensure_company_active_subscription
    return true if current_user.company? && current_user.subscription_status == 'active'
    head :unauthorized
  end

  def ensure_active_token
    # if there is a token available, check if it is active.
    if token
      api_key = ApiKey.active.where(access_token: token).first
      unless api_key
        return head :unauthorized
      end
    end
  end

  def current_user
    api_key = ApiKey.active.where(access_token: token).first
    if api_key
      # Refresh token, if token is older than 1 hour, and since here, still active.
      api_key.set_expire_date
      return api_key.user
    end

    nil
  end

  def token
    bearer = request.headers['HTTP_AUTHORIZATION']

    bearer ||= request.headers['rack.session'].try(:[], 'Authorization')

    if bearer.present?
      bearer.split.last
    else
      nil
    end
  end

  def index
    @current_domain = domain_to_lang (request.host || '').split('.').last
  end

  def robots
  end

  def domain_to_lang(domain)
    case domain
      when 'com'
        'en'
      when 'dk'
        'da'
      else
        'en'
    end
  end
end
