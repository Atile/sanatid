class Staff::ProductFamiliesController < Staff::StaffApplicationController
  def new
    @product_family = ProductFamily.new
  end

  def create
    @product_family = ProductFamily.new product_family_params

    if @product_family.save
      redirect_to staff_products_path, success: 'Product family was successfully created'
    else
      render 'new'
    end
  end

  def edit
    @product_family = ProductFamily.find(params[:id])
  end

  def update
    @product_family = ProductFamily.find(params[:id])

    if @product_family.update_attributes(product_family_params)
      redirect_to staff_products_path, success: 'Product family was successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    if ProductFamily.find(params[:id]).destroy
      redirect_to staff_products_path, success: 'Product family was successfully deleted'
    end
  end

  private

  def product_family_params
    params.require(:product_family).permit(:name, :description)
  end
end
