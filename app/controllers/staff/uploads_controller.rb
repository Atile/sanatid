class Staff::UploadsController < Staff::StaffApplicationController
  protect_from_forgery with: :null_session #without it controller rises an error
  skip_before_filter :authorize


  def create
    picture = Upload.new
    picture.image = params[:image]

    if picture.save
      render json: {upload: { links: { original: picture.image.compressed.url } } }
    end
  end

  private

  def upload_params
    params.permit(:image)
  end
end
