class Staff::SessionsController < Staff::StaffApplicationController
  before_filter :authorize, except: [:new, :create]

  def new
  end

  def create
    staff = Staff.find_by_email(session_params[:email])

    if staff && staff.authenticate(session_params[:password])
      session[:staff_id] = staff.id
      redirect_to staff_root_path, success: "Welcome, #{staff.name}"
    else
      flash.now[:error] = 'Invalid password or email'
      render 'new'
    end
  end

  def destroy
    session[:staff_id] = nil
    redirect_to staff_root_path, success: 'You have been logged out'
  end

  private

  def session_params
    params.require(:staff_session).permit(:email, :password)
  end
end
