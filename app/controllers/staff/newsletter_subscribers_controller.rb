class Staff::NewsletterSubscribersController < Staff::StaffApplicationController
  def index
    @subscribers = NewsletterSubscriber.all.order(:created_at)
  end

  def destroy
    if NewsletterSubscriber.find(params[:id]).destroy
      redirect_to staff_newsletter_subscribers_path, success: 'Unsubscribed client'
    end
  end
end
