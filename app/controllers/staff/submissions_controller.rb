class Staff::SubmissionsController < Staff::StaffApplicationController
  def index
    @submissions = Submission.filtered(params[:form], params[:subject])
  end

  def show
    @submission =  Submission.find params[:id]
  end

  def destroy
    if Submission.find(params[:id]).destroy
      redirect_to staff_submissions_path, success: 'Submission was successfully deleted'
    end
  end
end
