class Staff::RemindersController < Staff::StaffApplicationController
  def create
    reminder = Reminder.new reminder_params
    reminder.staff = current_user

    if reminder.save
      redirect_to session.delete(:return_to), success: 'Reminder was successfully created'
    end
  end

  def destroy
    if Reminder.find(params[:id]).destroy
      redirect_to staff_my_leads_path, success: 'Reminder was successfully created'
    end
  end

  private

  def reminder_params
    params.require(:reminder).permit(:staff_id, :lead_id, :comment, :date, :notify, :notify_at)
  end
end