class Staff::ProductSubscriptionsController < Staff::StaffApplicationController
  before_filter :if_nested

  # If shown within a company, load and use nested layout.
  def if_nested
    if params[:company_id].present?
      @company = Company.find(params[:company_id])
      self.class.layout 'nested/company'
    end
  end

  def index
    session[:return_to] ||= request.referer
    if params[:company_id].present?
      @product_subscriptions = ProductSubscription.where(company_id: params[:company_id])
    else
      @product_subscriptions = ProductSubscription.all
    end
  end

  def cancel_subscription
    subscription = ProductSubscription.find(params[:id])

    # Set the expire date, to next renewal date (ie. end of period).
    subscription.expires_at = subscription.next_renewal_at
    subscription.next_renewal_at = nil

    if subscription.save
      redirect_to session.delete(:return_to), success: "Subscription was canceled, and will expire #{subscription.expires_at}"
    end
  end

  def new
    @product_subscription = ProductSubscription.new
    @product_families = ProductFamily.all
  end

  def create
    subscription = ProductSubscription.new product_subscription_params
    subscription.company = @company
    subscription.staff = current_user
    subscription.start_subscription

    if subscription.save
      return redirect_to staff_company_product_subscriptions_path(@company), success: 'Subscription was created'
    end

    render 'new'
  end

  def show
    @subscription = ProductSubscription.find(params[:id])
  end

  private

  def product_subscription_params
    params.require(:product_subscription).permit(:product_id, :price)
  end
end
