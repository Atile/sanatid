class Staff::PagesController < Staff::StaffApplicationController
  def index
    @pages = Page.order(:weight).where(category: params[:category], parent_id: nil)
    session[:return_to] = request.original_url
  end

  def show
    @page = Page.find params[:id]
  end

  def new
    @parents = Page.where(category: params[:category], parent_id: nil)
    @page = Page.new
  end

  def create
    page = Page.new page_params

    if page.save
      redirect_to session.delete(:return_to), success: 'Page was successfully created'
    end
  end

  def edit
    @parents = Page.where(category: params[:category], parent_id: nil)
    @page = Page.find params[:id]
  end

  def update
    page = Page.find params[:id]

    if page.update_attributes page_params
      redirect_to session.delete(:return_to), success: 'Page was successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    if Page.find(params[:id]).destroy
      redirect_to session.delete(:return_to), success: 'Page was successfully deleted'
    end
  end

  private

  def page_params
    params.require(:page).permit(:category, :title, :weight, :content, :slug, :published, :parent_id)
  end
end
