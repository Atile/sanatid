class Staff::EmailsController < Staff::StaffApplicationController

  def index
    @emails = Email.all
  end

  def edit
    @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, tables: true, autolink: true, space_after_headers: true, hard_wrap: true)
    @email = Email.find params[:id]
  end

  def update
    email = Email.find params[:id]

    if email.update_attributes(email_params)
      redirect_to staff_emails_path, success: 'Email was updated'
    end
  end

  def show
    @email = Email.find params[:id]
  end

  private

  def email_params
    params.require(:email).permit(:subject, :title, :content)
  end
end
