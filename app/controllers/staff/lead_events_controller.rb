class Staff::LeadEventsController < Staff::StaffApplicationController
  def create
    lead_event = LeadEvent.new lead_event_params
    lead_event.staff = current_user

    if lead_event.save
      redirect_to session.delete(:return_to), success: 'Lead event was successfully created'
    end
  end

  private

  def lead_event_params
    params.require(:lead_event).permit(:staff_id, :lead_id, :comment, :event_type, :reason)
  end
end
