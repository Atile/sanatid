class Staff::ServicesController < Staff::StaffApplicationController
  def index

  end

  def new
    @service = Service.new
    @service_fields = ServiceField.all
  end

  def create
    service = Service.new service_params

    if service.save
      redirect_to staff_service_fields_path, success: 'Service was successfully created'
    else
      render 'new'
    end
  end

  def edit
    @service = Service.find params[:id]
    @service_fields = ServiceField.all
  end

  def update
    service = Service.find params[:id]

    if service.update_attributes(service_params)
      redirect_to staff_service_fields_path, success: 'Service field was successfully updated'
    end
  end

  def destroy
    Service.find(params[:id]).destroy
    redirect_to staff_service_fields_path, success: 'Service field was successfully deleted'
  end

  private

  def service_params
    params.require(:service).permit(:name, :practitioner_term, :short_description, :description, :content, :image, :service_field_id, :published, :flow_active, :meta_description)
  end
end
