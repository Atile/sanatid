class Staff::StaffsController < Staff::StaffApplicationController
  def index
    @staffs = Staff.active.all
  end

  def show
    @staff = Staff.find params[:id]
  end

  def new
    @staff = Staff.new
    @roles = StaffRole.all
  end

  def create
    @staff = Staff.new staff_params

    if @staff.save
      redirect_to staff_staffs_path, success: 'Staff member was successfully created'
    else
      render 'new'
    end
  end

  def edit
    @staff = Staff.find params[:id]
    @roles = StaffRole.all
  end

  def update
    @staff = Staff.find params[:id]

    if @staff.update_attributes(staff_params)
      redirect_to staff_staffs_path, success: 'Staff member was successfully updated'
    end
  end

  def destroy
    if Staff.update(params[:id], active: false)
      redirect_to staff_staffs_path, success: 'Staff member was successfully deleted'
    end
  end

  private

  def staff_params
    params.require(:staff).permit(:name, :description, :job_title, :image, :email, :password, :password_confirmation, :phone, :published, :twitter, :linkedin, :staff_role_id)
  end
end
