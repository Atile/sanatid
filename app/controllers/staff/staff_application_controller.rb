class Staff::StaffApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout 'staff_application'

  before_filter :authorize

  add_flash_types(:warning, :success, :error)

  I18n.default_locale = :da

  def index
  end

  private

  helper_method :current_user
  def current_user
    @current_user = Staff.find(session[:staff_id]) if session[:staff_id].present?
  end

  def authorize
    redirect_to staff_login_url, warning: 'Not authorized' if current_user.nil?
  end
end
