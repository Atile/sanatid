class Staff::InvoicesController < Staff::StaffApplicationController
  before_filter :if_nested

  # If shown within a company, load and use nested layout.
  def if_nested
    if params[:company_id].present?
      @company = Company.find(params[:company_id])
      self.class.layout 'nested/company'
    end
  end

  def index
    @prev_invoices = @company.invoices.where.not(state: 'open')
  end

end
