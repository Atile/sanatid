class Staff::LeadsController < Staff::StaffApplicationController
  def index
    @service_fields = ServiceField.all.select(:name, :id)
    @zips = ['All'] + Lead.select(:city).group(:city).order('city').map(&:city)
    @regions = ['All'] + Lead.select(:region).group(:region).order('region').map(&:region)
    @categories = ['All'] + Lead.select(:category).order(:category).distinct.map(&:category)
    @staffs = Staff.all

    # Apply filters.
    @leads = Lead.active.unassigned.not_converted.filtered(params[:service_field], params[:zip], params[:region], params[:contacted].to_i, params[:category]).page(params[:page]).order('company_name')
  end

  def signups
    @leads = Lead.where(category: 'signup').page(params[:page])
  end

  def show
    @lead = Lead.find(params[:id])
    @staffs = Staff.active
    @lead_event = LeadEvent.new
    @reminder = Reminder.new

    @convert_query = {
      name: @lead.name,
      company_name: @lead.company_name,
      service_field_ids: [@lead.service_field.id],
      phone: @lead.phone.delete(' '),
      homepage: @lead.homepage,
      cvr: @lead.cvr,
      address: "#{@lead.address}#{@lead.city}",
      lead_id: @lead.id
    }

    session[:return_to] = request.referer
  end

  def destroy
    if Lead.find(params[:id]).update_attributes(archived: true)
      redirect_to staff_leads_path, success: 'Lead was successfully deleted'
    end
  end

  def new
    @lead = Lead.new
  end

  def create
    @lead = Lead.new lead_params
    @lead.category = 'manual'

    if @lead.save
      redirect_to staff_leads_path, success: 'Lead was successfully created'
    else
      render 'new'
    end
  end

  def edit
    @lead = Lead.find(params[:id])
  end

  def update
    @lead = Lead.find(params[:id])

    if @lead.update_attributes(lead_params)
      redirect_to staff_lead_path(params[:id]), success: 'Lead was successfully updated'
    else
      render 'edit'
    end
  end

  def assign_leads
    if params[:staff_id] && params[:ids]
      Lead.where(id: params[:ids]).update_all(staff_id: params[:staff_id])
      return redirect_to request.referer, success: 'Assigned leads'
    end

    redirect_to request.referer, error: 'Could not assign leads'
  end

  def my_leads
    @lead_events = current_user.lead_events.page
    @reminders = current_user.reminders.where(date: 2.day.ago..6.months.from_now).order(:date)
  end

  def send_presentation
    lead = Lead.find(params[:id])

    recipient = params[:recipient] || lead.email

    if recipient && lead
      LeadMailer.delay.sanatid_presentation(lead, current_user, recipient)
      flash[:success] = 'The presentation material was send'

      remind_in = params[:reminder].to_i
      if remind_in > 0
        reminder = Reminder.new notify: true, lead: lead, staff: current_user, date: remind_in.days.from_now
        lead_link = "<a href='#{staff_lead_path(lead)}'>#{lead.company_name}</a>"
        reminder.comment = "I send presentation material to #{lead_link}, #{remind_in} days ago. Time to follow up."

        reminder.save
      end
    else
      flash[:error] = 'Recipient or lead was missing'
    end

    redirect_to staff_lead_path(lead)
  end

  private

  def lead_params
    params.require(:lead).permit(:name, :company_name, :email, :phone, :cvr, :address, :zip, :homepage, :service_field, :staff_id)
  end
end
