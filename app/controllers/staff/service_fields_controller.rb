class Staff::ServiceFieldsController < Staff::StaffApplicationController
  def index
    @service_fields = ServiceField.all
  end

  def new
    @service_field = ServiceField.new
  end

  def create
    service_field = ServiceField.new service_field_params

    if service_field.save
      redirect_to staff_service_fields_path, success: 'Service field was successfully created'
    else
      render 'new'
    end
  end

  def edit
    @service_field = ServiceField.find params[:id]
  end

  def update
    service_field = ServiceField.find params[:id]

    if service_field.update(service_field_params)
      redirect_to staff_service_fields_path, success: 'Service field was successfully updated'
    end
  end

  def destroy
    ServiceField.find(params[:id]).destroy
    redirect_to staff_service_fields_path, success: 'Service field was successfully deleted'
  end

  private

  def service_field_params
    params.require(:service_field).permit(:name, :short_description, :description, :content, :practitioner_term, :image, :published, :flow_active, :meta_description)
  end
end
