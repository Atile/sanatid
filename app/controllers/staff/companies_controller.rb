class Staff::CompaniesController < Staff::StaffApplicationController
  layout :resolve_layout

  def index
    @companies = Company.all
  end

  def maps
    @companies = Company.all
    @companies = @companies.joins(:services).where(services: {id: params[:service]}) if params[:service]
    if params[:service_field]
      @companies = @companies.joins(:services).joins('JOIN service_fields ON services.service_field_id = service_fields.id').where(service_fields: {id: params[:service_field]})
    end
    @service_fields = ServiceField.all

    @company_cords = @companies.select(:latitude, :longitude).map do |c|
      {latitude: c.latitude, longitude: c.longitude} if c.latitude && c.longitude
    end.compact.to_json

    respond_to do |format|
      format.html
      format.json { render json: @company_cords}
    end
  end

  def show
    @company = Company.find params[:id]
    @service_fields = ServiceField.all
  end

  def edit
    session[:return_to] ||= request.referer
    @company = Company.find params[:id]
    @service_fields = ServiceField.all
  end

  def update
    @company = Company.find params[:id]

    if @company.update_attributes company_params
      redirect_to session.delete(:return_to), success: 'Company was successfully updated'
    else
      @service_fields = ServiceField.all
      render 'edit'
    end
  end

  def new
    @company = Company.new company_params
    @service_fields = ServiceField.all
  end

  def create
    @company = Company.new company_params
    # Generate a new password for the company.
    password = Digest::SHA2.hexdigest(rand(1000000).to_s)[0..8]
    @company.password = @company.password_confirmation = password

    if @company.save
      # Archive the lead, converted from.
      @company.lead.update_attributes(archived: true) if @company.lead.present?

      # Send welcome mail to company, with login information.
      CompanyMailer.delay(priority: 10).welcome_company(@company, password)

      redirect_to staff_companies_path, success: 'Company was successfully created'
    else
      @service_fields = ServiceField.all
      render 'new'
    end
  end

  def destroy
    if Company.find(params[:id]).update_attributes(active: false)
      redirect_to staff_companies_path, success: 'Company was successfully deleted'
    end
  end

  private

  def company_params
    return if params[:company].nil?
    params.require(:company).permit(:name, :email, :phone, :homepage, :address, :cvr, :company_email, :company_name, :description, :password, :password_confirmation, :lead_id, service_field_ids: [])
  end

  def resolve_layout
    case action_name
      when 'show', 'edit'
        'nested/company'
      else
        'staff_application'
    end
  end
end
