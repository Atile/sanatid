class Staff::BlogsController < Staff::StaffApplicationController
  def index
    @blogs = Blog.all
  end

  def edit
    @blog = Blog.find params[:id]
  end

  def update
    blog = Blog.find params[:id]

    if blog.update_attributes blog_params
      redirect_to staff_blogs_path, success: 'Blog was successfully updated'
    else
      render 'edit'
    end
  end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new blog_params
    @blog.staff = current_user

    if @blog.save
      redirect_to staff_blogs_path, success: 'Blog was successfully created'
    else
      render 'new'
    end
  end

  def destroy
    if Blog.find(params[:id]).destroy
      redirect_to staff_blogs_path, success: 'Blog was successfully deleted'
    end
  end

  private

  def blog_params
    params.require(:blog).permit(:title, :summary, :content, :image, :published, :meta_description)
  end
end
