class Staff::ClipsController < Staff::StaffApplicationController
  def index
    @clips = Clip.all
  end

  def show
    @clip = Clip.find(params[:id])
  end

  def edit
    @clip = Clip.find(params[:id])
  end

  def update
    clip = Clip.find(params[:id])

    if clip.update_attributes clip_params
      redirect_to staff_clips_path, success: 'Clip was successfully updated'
    end
  end

  def destroy
    if Clip.find(params[:id]).destroy
      redirect_to staff_clips_path, success: 'Clip was successfully deleted'
    end
  end

  private

  def clip_params
    params.require(:clips).permit(:status)
  end
end
