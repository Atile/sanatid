class Staff::ProductsController < Staff::StaffApplicationController
  respond_to :html, :json

  def index
    @product_families = ProductFamily.all.order(:id)
  end

  def show
    @product = Product.find(params[:id])

    respond_to do |format|
      format.html
      format.json { respond_with @product }
    end
  end

  def new
    @product_families = ProductFamily.all
    @product = Product.new
  end

  def create
    product = Product.new product_params

    if product.save
      redirect_to staff_products_path, success: 'Product was successfully created'
    else
      render 'new'
    end
  end

  def edit
    @product_families = ProductFamily.all
    @product = Product.find params[:id]
  end

  def update
    product = Product.find params[:id]

    if product.update_attributes product_params
      redirect_to staff_products_path, success: 'Product was successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    if Product.find(params[:id]).destroy
      redirect_to staff_products_path, success: 'Product was successfully deleted'
    end
  end

  private

  def product_params
    params.require(:product).permit :name, :description, :fundamental, :duration, :billing_period, :price, :commission, :active, :handle, :product_family_id, :weight, :icon_class
  end
end
