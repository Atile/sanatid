class MessageMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def company_received_message(message)
    @email = Email.find_by_machine_name('company_received_message')
    @message = message

    mail(:to => @message.recipient.email, subject: @email.subject)
  end

  def customer_received_message(message)
    @email = Email.find_by_machine_name('customer_received_message')
    @message = message

    mail(:to => @message.recipient.email, subject: @email.subject)
  end
end
