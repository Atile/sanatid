class ReminderMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def notify_staff(reminder)
    @email = Email.find_by_machine_name('notify_staff')
    @reminder = reminder

    mail(:to => @reminder.staff.email, subject: @email.subject)
  end
end
