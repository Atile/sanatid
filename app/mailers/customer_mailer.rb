class CustomerMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def welcome_customer(customer)
    @email = Email.find_by_machine_name('welcome_customer')
    @customer = customer
    mail(to: @customer.email, subject: @email.subject)
  end
end
