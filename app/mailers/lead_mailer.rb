class LeadMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def sanatid_presentation(lead, staff, recipient)
    @email = Email.find_by_machine_name('sanatid_presentation')
    @lead  = lead
    @staff = staff

    mail(to: recipient, from: staff.email, subject: @email.subject)
  end
end
