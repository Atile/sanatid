class VoucherMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def clip_confirmation(clip)
    @email = Email.find_by_machine_name('clip_confirmation')

    @company = clip.voucher.company
    @customer = clip.voucher.customer
    @clip = clip

    mail(to: @customer.email, subject: @email.subject)
  end
end
