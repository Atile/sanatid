class SubscriptionMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def subscription_expired(subscription)
    @email = Email.find_by_machine_name('subscription_expired')
    @subscription = subscription

    mail(to: subscription.company.email, subject: @email.subject)
  end

  def subscription_canceled(subscription)
    @email = Email.find_by_machine_name('subscription_canceled')
    @subscription = subscription

    mail(to: subscription.company.email, subject: @email.subject)
  end

  def subscription_renewed(subscription)
    @email = Email.find_by_machine_name('subscription_renewed')
    @subscription = subscription

    mail(to: subscription.company.email, subject: @email.subject)
  end

  def subscription_nearly_expired(subscription)
    @email = Email.find_by_machine_name('subscription_nearly_expired')

    @subscription = subscription
    mail(to: subscription.company.email, subject: @email.subject)
  end
end
