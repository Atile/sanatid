class NewsletterMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def newsletter_confirmation(email)
    @email = Email.find_by_machine_name('newsletter_confirmation')

    mail(:to => email, subject: @email.subject)
  end
end
