class BookingMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def customer_booking_confirmation(booking)
    @email = Email.find_by_machine_name('customer_booking_confirmation')
    @booking = booking

    mail(to: booking.email, subject: @email.subject)
  end

  def company_new_booking(booking)
    @email = Email.find_by_machine_name('company_new_booking')
    @booking = booking

    mail(to: booking.email, subject: @email.subject)
  end
end
