class UserMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def reset_password(user, password)
    @email = Email.find_by_machine_name('reset_password')
    @user = user
    @password = password
    mail(to: @user.email, subject: @email.subject)
  end
end
