require 'erb'

class CompanyMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def welcome_company(company, password)
    @email = Email.find_by_machine_name('welcome_company')
    @company = company
    @password = password

    mail(:to => @company.email, subject: @email.subject)
  end
end
