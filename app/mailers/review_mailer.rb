class ReviewMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  def new_review(review)
    @email = Email.find_by_machine_name('new_review')
    @review = review

    mail(:to => @review.company.email, subject: @email.subject)
  end
end
