class InvoiceMailer < ActionMailer::Base
  layout 'mail_layout'
  helper :staff_application

  after_action :set_send_state

  def send_invoice(invoice, pdf)
    @email = Email.find_by_machine_name('send_invoice')
    @invoice = invoice

    attachments['invoice.pdf'] = pdf
    mail(to: invoice.company.email, subject: @email.subject)
  end

  private

  def set_send_state
    # If the invoice total is 0, flag it as payed.
    state = @invoice.total > 0 ? 'send' : 'payed'
    @invoice.update_attributes(state: state)
  end
end
