class NewsletterSubscriberSerializer < ActiveModel::Serializer
  attributes :id, :email, :user
  embed :ids

  # Polymorphic type, can be both Customer or Company.
  def user
    # Check if present, as user might be on the anonymous list.
    return unless object.user.present?
    {
        id: object.user.id,
        type: object.user.type
    }
  end
end
