class FacilityImageSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :original, :thumb
  has_one :company
  embed :ids

  def original
    object.image_url(:compressed)
  end

  def thumb
    object.image_url(:thumb)
  end
end
