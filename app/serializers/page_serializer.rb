class PageSerializer < ActiveModel::Serializer
  attributes :id, :title, :content, :slug
  has_one :parent
  embed :ids
end
