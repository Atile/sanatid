class ProductSubscriptionSerializer < ActiveModel::Serializer
  attributes :id, :expires_at, :renewed_at, :next_renewal_at, :created_at, :active
  has_one :company
  has_one :product
  embed :ids, include: true
end
