class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :fundamental, :price, :duration, :commission, :billing_period, :product_family, :icon_class
  has_one :product_family
  embed :ids, include: true
end
