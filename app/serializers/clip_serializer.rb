class ClipSerializer < ActiveModel::Serializer
  attributes :id, :created_at, :status
  has_one :voucher
  embed :ids, include: true
end
