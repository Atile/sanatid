class ProductFamilySerializer < ActiveModel::Serializer
  attributes :id, :name, :description
  has_many :products
  embed :ids, include: true
end
