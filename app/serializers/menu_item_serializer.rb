class MenuItemSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :price, :duration
  has_one :company
  embed :ids
end
