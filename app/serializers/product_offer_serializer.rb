class ProductOfferSerializer < ActiveModel::Serializer
  attributes :id, :confirmed, :confirmed_at, :expire_at, :description, :active
  has_one :company
  has_one :staff
  has_many :products
  embed :ids, include: true
end
