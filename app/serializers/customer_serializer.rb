class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :phone
  has_one :newsletter_subscriber
  embed :ids, include: true

  def include_associations!
    if scope.present? && scope.id == object.id
      include! :newsletter_subscriber
    end
  end

  def attributes
    hash = super
    if scope.present? && current_user == object
      hash['email'] = object.email
      hash['phone'] = object.phone
    end
    hash
  end
end
