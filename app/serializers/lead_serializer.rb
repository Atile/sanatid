class LeadSerializer < ActiveModel::Serializer
  attributes :id, :company_name, :region, :slug, :address, :logo, :is_lead, :distance_to, :phone, :homepage, :email
  has_one :service_field, embed: :ids, include: true

  def distance_to
    object.distance_to(@options[:cords]).round(2) if @options[:cords].present?
  end

  def is_lead
    true
  end

  def logo
    ActionController::Base.helpers.asset_path('company/default_profile_logo.png')
  end
end
