class VoucherSerializer < ActiveModel::Serializer
  attributes :id, :clip_count, :created_at
  has_many :clips
  has_one :customer
  has_one :company
  embed :ids, include: true

  def include_associations!
    include! :customer if scope.company?
    include! :company if scope.customer?
    include! :clips
  end
end
