class CommissionLineSerializer < ActiveModel::Serializer
  attributes :id, :total, :percentage
  has_one :company
  embed :ids, include: true
end
