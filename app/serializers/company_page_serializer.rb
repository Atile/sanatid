class CompanyPageSerializer < ActiveModel::Serializer
  attributes :id, :title, :content, :weight
  has_one :parent_page, include: true, root: :parent_page
  has_one :company, embed: :id
end
