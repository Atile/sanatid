class ReviewSerializer < ActiveModel::Serializer
  attributes :id, :content, :rating, :created_at
  has_one :company
  has_one :customer, include: true
  embed :ids
end
