class BlogSerializer < ActiveModel::Serializer
  attributes :id, :title, :summary, :content, :meta_description, :created_at, :updated_at, :slug, :avatar, :small_avatar
  has_one :staff
  embed :id, include: true

  def avatar
    object.image_url :huge_avatar if object.image.present?
  end

  def small_avatar
    object.image_url :big_avatar if object.image.present?
  end
end
