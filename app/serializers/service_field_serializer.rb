class ServiceFieldSerializer < ActiveModel::Serializer
  attributes :id, :name, :short_description, :description, :meta_description, :content, :practitioner_term, :avatar, :small_avatar, :big_avatar, :huge_avatar, :slug, :flow_active
  has_many :services
  embed :id, include: true

  def services
    object.services.published
  end

  def avatar
    object.image_url :avatar if object.image.present?
  end

  def small_avatar
    object.image_url :small_avatar if object.image.present?
  end

  def big_avatar
    object.image_url :big_avatar if object.image.present?
  end

  def huge_avatar
    object.image_url :huge_avatar if object.image.present?
  end
end
