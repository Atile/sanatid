class MessageSerializer < ActiveModel::Serializer
  attributes :id, :subject, :content, :created_at, :recipient, :dispatcher
  embed :ids

  # Polymorphic type, can be both Customer or Company.
  def recipient
    {
      id: object.recipient.id,
      type: object.recipient.type
    }
  end

  # Polymorphic type, can be both Customer or Company.
  def dispatcher
    {
      id: object.dispatcher.id,
      type: object.dispatcher.type
    }
  end
end
