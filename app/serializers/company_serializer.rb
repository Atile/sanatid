class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :company_name, :address, :slug, :description, :average_rating, :review_count, :opening_hours, :logo, :payment_methods
  has_one :newsletter_subscriber
  has_many :services
  has_many :service_fields
  has_many :reviews
  has_many :menu_items
  has_many :facility_images
  embed :ids, include: true

  def include_associations
    if scope.present? && scope.id == object.id
      include! :newsletter_subscriber
    end
    include! :services
    include! :reviews
    include! :menu_items
    include! :facility_images
  end

  def average_rating
    (object.reviews.average(:rating).to_f / 5 * 10).round(1) if object.reviews
  end

  def review_count
    object.reviews.count
  end

  def attributes
    hash = super
    if scope.present? && scope.id == object.id
      hash['email'] = object.email
      hash['company_email'] = object.company_email
      hash['phone'] = object.phone
      hash['address'] = object.address
      hash['cvr'] = object.cvr
      hash['subscription_status'] = object.subscription_status
    end
    hash['logo'] = object.logo.present? ? object.logo_url(:company_logo) : ActionController::Base.helpers.asset_path('company/default_profile_logo.png')
    hash['distance_to'] = object.distance_to(@options[:cords]).round(2) if @options[:cords].present?
    hash
  end
end
