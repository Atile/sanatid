class StaffSerializer < ActiveModel::Serializer
  attributes :id, :name, :job_title, :description, :email, :phone, :avatar, :small_avatar, :twitter, :linkedin

  def avatar
    object.image_url :huge_avatar if object.image.present?
  end

  def small_avatar
    object.image_url :big_avatar if object.image.present?
  end
end
