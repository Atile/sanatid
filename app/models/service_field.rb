class ServiceField < ActiveRecord::Base
  include Slugifier

  has_many :services
  has_many :leads
  has_and_belongs_to_many :companies

  mount_uploader :image, AvatarUploader

  default_scope { order('name ASC') }
  scope :published, -> { ServiceField.where(published: true) }

  private

  def create_slug
    unless self.slug.present?
      self.slug = self.generate_slug(self.name)
    end
  end
end
