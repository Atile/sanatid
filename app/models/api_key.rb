class ApiKey < ActiveRecord::Base
  validates :scope, inclusion: {in: %w(session api)}
  before_create :set_expire_date, :generate_token
  belongs_to :user

  scope :session, -> { where(scope: 'session') }
  scope :api, -> { where(scope: 'api') }
  scope :active, -> { where('expired_at >= ?', Time.now) }

  def set_expire_date
    if self.scope == 'session'
      self.expired_at = 5.hours.from_now
    else
      self.expired_at = 30.days.from_now
    end
  end

  private

  def generate_token
    self.access_token = SecureRandom.hex
  end
end
