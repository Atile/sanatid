class Voucher < ActiveRecord::Base
  belongs_to :company
  belongs_to :customer
  has_many :clips

  def active?
    # If a customer is not set, the voucher is not active.
    if self.customer.present?
      return true
    end
    false
  end
end
