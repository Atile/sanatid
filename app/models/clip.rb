class Clip < ActiveRecord::Base
  belongs_to :voucher

  scope :undecided, -> { self.where(status: 'undecided') }

  validates :status, inclusion: { in: %w(confirmed declined undecided), message: "%{value} is not a valid status" }

  after_validation :check_status

  # If customer don't confirm clips after 10 days, we do it automatically.
  def self.flag_old_clips_as_confirmed
    self.where('status = ? AND created_at < ?', 'undecided', 10.days.ago).update_all(status: :confirmed)
  end

  private
  # Set confirmed_at date, if the clip was confirmed, or declined.
  def check_status
    self.status_updated_at = Time.now if self.status_changed?
  end
end
