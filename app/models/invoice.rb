class Invoice < ActiveRecord::Base
  belongs_to :company
  has_many :invoice_line_items
  has_many :commission_lines

  STATES = %w{open send paid overdue}

  scope :open, -> { self.where(state: 'open') }

  def add_line_item(subscription)
    line_item = InvoiceLineItem.new invoice: self, product_subscription: subscription
    # Calculate how many days by counting time from now, to next_renewal_at.
    line_item.quantity = ((subscription.next_renewal_at || subscription.expires_at).to_date - DateTime.now.beginning_of_day.to_date).to_i
    line_item.quantity_type = 'Days'
    line_item.total = (subscription.product.price / subscription.product.billing_period_in_days) * line_item.quantity
    line_item.total = line_item.total.round(2)
    line_item.tax = true
    line_item.description = "#{subscription.product.name} - Period #{I18n.l DateTime.now.beginning_of_day, format: :clean} to #{I18n.l(subscription.next_renewal_at, format: :clean) || I18n.l(subscription.expires_at, format: :clean)}"
    line_item.currency = 'dkk'
    line_item.save
  end

  def replace_line_item(line_item)
    subscription = line_item.product_subscription
    # Set the quantity, to equal the time it has been active.
    line_item.quantity = (DateTime.now.beginning_of_day.to_date - line_item.created_at.beginning_of_day.to_date).to_i
    line_item.quantity_type = 'Days'
    line_item.total = (subscription.product.price / subscription.product.billing_period_in_days) * line_item.quantity
    line_item.total = line_item.total.round(2)
    line_item.tax = true

    from = DateTime.now.beginning_of_day - line_item.quantity.days
    to = DateTime.now.end_of_day

    line_item.description = "#{subscription.product.name} - Period #{I18n.l from, format: :clean} to #{I18n.l( to, format: :clean)}"
    line_item.currency = 'dkk'
    line_item.save
  end

  # TODO: Change to booking instead.
  def add_commission_line(job)
    line = CommissionLine.new company: company, job: job, invoice: self
    line.percentage = company.fundamental_subscription.product.commission
    line.total = (job.winning_offer.price / 100) * line.percentage
    line.description = "(Opgave ##{job.id}) #{line.percentage}% Kommission af #{job.winning_offer.price}"
    line.tax = true
    line.currency = 'dkk'
    line.save
  end

  def preamble
    self.reference_id = "#{Digest::SHA2.hexdigest(id.to_s)[0..8]}-#{company.id}"
    self.due_date = 7.days.from_now.end_of_day
  end

  def total
    Invoice.joins(:invoice_line_items).where(id: id).sum(:total) + Invoice.joins(:commission_lines).where(id: id).sum(:total)
  end

  def total_vat
    vat = 0
    self.invoice_line_items.each do |item|
      vat += (item.total / 100) * 25 if item.tax
    end
    self.commission_lines.each do |item|
      vat += (item.total / 100) * 25 if item.tax
    end
    vat
  end

  def self.send_invoices
    # Find all invoices, which end date is exceeded.
    Invoice.open.where('invoices.end_date <= ?', Time.now).each do |invoice|
      # Prepare for sending.
      invoice.preamble
      invoice.save

      ac = ActionController::Base.new
      pdf = ac.render_to_string pdf: "faktura-#{invoice.id}", template: 'api/product_subscriptions/invoice', layout: 'clean_slate', locals: {:@invoice => invoice}
      InvoiceMailer.delay(priority: 15).send_invoice(invoice, pdf)
    end
  end
end
