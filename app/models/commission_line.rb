class CommissionLine < ActiveRecord::Base
  belongs_to :company
  belongs_to :invoice
end
