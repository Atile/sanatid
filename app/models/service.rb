class Service < ActiveRecord::Base
  include Slugifier

  belongs_to :service_field

  mount_uploader :image, AvatarUploader

  default_scope { order('name ASC') }
  scope :published, -> { Service.where(published: true) }

  private

  def create_slug
    unless self.slug.present?
      self.slug = self.generate_slug(self.name)
    end
  end
end
