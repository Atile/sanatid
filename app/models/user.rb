class User < ActiveRecord::Base
  has_secure_password

  has_many :api_keys
  has_one :newsletter_subscriber
  has_many :recipients, class_name: 'Message', foreign_key: 'recipient_id'
  has_many :dispatchers, class_name: 'Message', foreign_key: 'dispatcher_id'

  validates :email, presence: true, uniqueness: true
  validates :name, presence: true

  geocoded_by :address
  after_validation :geocode, if: :address_changed?

  def session_api_key
    api_keys.active.session.first_or_create
  end

  def company?
    self.is_a? Company
  end

  def customer?
    self.is_a? Customer
  end
end
