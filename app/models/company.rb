class Company < User
  include Slugifier

  has_many :vouchers
  has_many :company_pages
  has_and_belongs_to_many :services
  has_and_belongs_to_many :service_fields
  has_many :product_subscriptions
  has_many :reviews
  has_many :reservations
  belongs_to :lead
  has_many :invoices
  has_many :commission_lines
  has_many :menu_items
  has_many :calendar_events
  has_many :facility_images
  has_many :bookings

  serialize :opening_hours
  serialize :payment_methods
  mount_uploader :logo, LogoUploader

  validates :subscription_status, inclusion: { in: %w(active canceled expired passive), message: "%{value} is not a valid status" }
  validates :phone, :company_name, presence: true
  validates :email, format: { with: /\A(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})\z/i }
  validates :phone, format: { with: /\A\d{8}/ }

  before_create :create_slug, :init_payment_methods
  before_save :sanitize_description

  scope :active, -> { where(active: true) }
  scope :newest, -> { order('created_at DESC') }
  scope :active_subscription, -> { where(subscription_status: 'active') }
  scope :address_contains, ->(address) { where('address like ?', "%#{address}%") }

  def sanitize_description
    allowed_elements = Array.new Sanitize::Config::RELAXED[:elements]
    allowed_elements.delete('a')
    self.description = Sanitize::fragment(description, Sanitize::Config.merge(Sanitize::Config::RELAXED,
                                                                         elements: allowed_elements))
  end

  def fundamental_subscription
    self.product_subscriptions.active.each do |subscription|
      return subscription if subscription.product && subscription.product.fundamental
    end
    nil
  end

  def active_invoice
    invoice = self.invoices.open.where('invoices.end_date > ?', Time.now).first
    unless invoice.present?
      return init_invoice
    end
    invoice
  end

  def init_invoice
    if self.fundamental_subscription.present?
      invoice = Invoice.new company: self, state: 'open'

      previous_invoice = self.invoices.last
      if previous_invoice.present?
        # Should start the day after, the last expired.
        invoice.start_date = previous_invoice.end_date + 1.day
        invoice.end_date = previous_invoice.end_date + 31.days
      else
        invoice.start_date = fundamental_subscription.renewed_at
        invoice.end_date = 30.days.from_now.end_of_day
      end
      invoice.save
      invoice
    end
  end

  def self.find_by_frontpage_promotion(level, num)
    product_handle = "frontpage-promotion-#{level}"

    self.includes(:services).joins(product_subscriptions: [:product])
    .where(products: {handle: product_handle}, product_subscriptions: {active: true})
    .order('RANDOM()').limit(num)
  end

  def self.find_by_flow_promotion(level, num, cord)
    product_handle = "flow-promotion-#{level}"

    self.joins(product_subscriptions: [:product])
    .where(products: {handle: product_handle}, product_subscriptions: {active: true})
    .order('RANDOM()').limit(num)
    .near(cord, 15)
  end

  private

  def create_slug
    unless self.slug.present?
      self.slug = self.generate_slug(self.company_name)
    end
  end

  def init_payment_methods
    self.payment_methods = []

    I18n.t('config.company_payment_methods').each do |payment_method|
      self.payment_methods << {name: payment_method, enabled: false}
    end
  end
end