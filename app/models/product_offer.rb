class ProductOffer < ActiveRecord::Base
  has_and_belongs_to_many :products
  belongs_to :staff
  belongs_to :company

  scope :active, -> { ProductOffer.where(active: true) }
end
