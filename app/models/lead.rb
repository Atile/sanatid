class Lead < ActiveRecord::Base
  has_many :lead_events
  has_many :reminders
  has_one :company
  belongs_to :service_field
  belongs_to :staff

  scope :unassigned, -> { self.where(staff_id: nil) }
  scope :active, -> { self.where(archived: false) }
  scope :not_converted, -> { self.select('leads.*').joins('LEFT JOIN users ON users.lead_id = leads.id').where('users.id IS NULL') }
  scope :fresh, -> { joins('left join lead_events le on leads.id = le.lead_id').where('le.lead_id is null') }
  scope :public, -> { where(public: true) }

  geocoded_by :address

  before_create -> { self.slug = self.company_name.parameterize }

  def link
    unless self.homepage =~ /http:\/\//
      return "http://#{homepage}"
    end
    homepage
  end

  def self.filtered(service_field, city, region, contacted_in, category)
    leads = Lead.all
    leads.where!(service_field_id: service_field) if service_field.present? && service_field != 'All'
    leads.where!(city: city) if city.present? && city != 'All'
    leads.where!(category: category) if category.present? && category != 'All'
    leads.where!(region: region) if region.present? && region != 'All'
    if contacted_in.present? && contacted_in != 0
      leads.joins!('LEFT JOIN lead_events ON lead_events.lead_id = leads.id').where!('lead_events.created_at > ? OR lead_events.created_at IS NULL AND leads.lead_type = "contacted"', contacted_in.to_i.months.from_now)
    end
    leads
  end
end