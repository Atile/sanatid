class MenuItem < ActiveRecord::Base
  belongs_to :company
  has_many :bookings
end
