class ProductSubscription < ActiveRecord::Base
  belongs_to :product
  belongs_to :company
  belongs_to :staff
  has_many :invoice_line_items

  scope :active, -> { self.where(active: true) }
  scope :inactive, -> { self.where(active: false) }

  def fundamental?
    self.product.fundamental
  end

  def start_subscription
    self.renewed_at = Time.now.beginning_of_day
    self.next_renewal_at = self.product.billing_period_in_days.days.from_now.end_of_day
    self.active = true
  end

  def self.refresh_subscriptions
    # Expire subscriptions.
    self.active.where('expires_at <= NOW()').each do |subscription|
      # If fundamental, set the status of the company.
      subscription.company.update_attributes(subscription_status: 'expired')
      # Set subscription to inactive.
      subscription.active = false
      subscription.save
    end

    # Renew subscriptions.
    self.active.where('next_renewal_at <= NOW()').each do |subscription|
      subscription.renewed_at = subscription.next_renewal_at
      subscription.next_renewal_at = subscription.next_renewal_at + subscription.product.billing_period_in_days.days
      subscription.save

      # Add the line to invoice, for next month.
      subscription.company.active_invoice.add_line_item(subscription)
    end

    puts 'Refresh subscriptions was ran.' unless Rails.env.test?
  end

  def self.notify_nearly_expired
    self.active.where(expires_at: 2.days.from_now..3.days.from_now).each do |subscription|
      SubscriptionMailer.delay(priority: 10).subscription_nearly_expired(subscription)
    end
  end

  def self.collect_monthly_commission
    Company.active.active_subscription.each do |company|
      # Traverse bookings here instead.
    end
  end
end
