class Review < ActiveRecord::Base
  belongs_to :customer
  belongs_to :company
  default_scope { order('created_at DESC') }

  validate :ensure_one_rating_per_job, on: :create
  validate :ensure_rating_range, on: :create

  def ensure_rating_range
    unless self.rating.between?(1, 5)
      errors.add(:rating, 'Rating should be between 1 and 5')
    end
  end

  def ensure_one_rating_per_job
    if self.job.review.present?
      errors.add(:job, 'Job is already reviewed')
    end
  end
end
