class CalendarEvent < ActiveRecord::Base
  belongs_to :company

  scope :within, ->(start_date, end_date) { where(end_date: start_date..end_date) }
end
