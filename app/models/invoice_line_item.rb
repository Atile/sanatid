class InvoiceLineItem < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :product_subscription
end
