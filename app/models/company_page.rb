class CompanyPage < ActiveRecord::Base
  include Slugifier

  belongs_to :company
  belongs_to :parent_page, class_name: 'CompanyPage', foreign_key: 'parent_page_id'

  before_create :create_slug
  before_save :sanitize_content

  def sanitize_content
    allowed_elements = Array.new Sanitize::Config::RELAXED[:elements]
    allowed_elements.delete('a')
    self.content = Sanitize::fragment(content, Sanitize::Config.merge(Sanitize::Config::RELAXED,
                                                                      elements: allowed_elements))
  end

  private

  def create_slug
    unless self.slug.present?
      self.slug = self.generate_slug(self.title)
    end
  end
end
