class Reminder < ActiveRecord::Base
  belongs_to :staff
  belongs_to :lead

  def self.notify
    all.where('notify = ? and notified = ? and date < ?', 't', 'f', Time.now).each do |reminder|
      ReminderMailer.notify_staff(reminder).deliver
      # Mark as notified, so it won't be send multiple times.
      reminder.update_attributes(notified: true)
    end
  end
end
