class Staff < ActiveRecord::Base
  has_secure_password

  has_many :blogs
  has_many :leads
  has_many :lead_events
  has_many :reminders
  has_many :product_subscriptions
  belongs_to :staff_role

  validates :email, uniqueness: true, presence: true

  scope :active, -> { Staff.where(active: true) }
  scope :published, -> { Staff.where(published: true) }

  mount_uploader :image, AvatarUploader

  def has_role?(role)
    if staff_role.present?
      return staff_role.name == role || staff_role.name == 'administrator'
    end

    false
  end
end
