class Submission < ActiveRecord::Base
  default_scope { order('created_at DESC') }

  def self.filtered(form, subject)
    submissions = Submission.all
    submissions.where!(subject: subject) if subject.present?
    submissions.where!(form: form) if form.present?
    submissions
  end
end
