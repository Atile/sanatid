class Product < ActiveRecord::Base
  has_many :product_subscriptions
  belongs_to :product_family

  def billing_period_in_days
    case self.billing_period
    when 'monthly'
      30
    when 'quarterly'
      90
    when 'yearly'
      365
    end
  end
end
