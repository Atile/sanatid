class Blog < ActiveRecord::Base
  include Slugifier

  belongs_to :staff

  default_scope { order('created_at DESC') }
  scope :published, -> { Blog.where(published: true) }

  mount_uploader :image, AvatarUploader

  before_create :create_slug

  private

  def create_slug
    unless self.slug.present?
      self.slug = self.generate_slug(self.title)
    end
  end
end
