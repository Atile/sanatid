class ProductFamily < ActiveRecord::Base
  has_many :products, -> { self.order(:weight) }
end
