class FacilityImage < ActiveRecord::Base
  belongs_to :company

  mount_uploader :image, FacilityUploader
end
