class Customer < User
  has_many :vouchers
  has_many :reviews
end
