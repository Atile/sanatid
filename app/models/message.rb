class Message < ActiveRecord::Base
  belongs_to :recipient, class_name: 'User', foreign_key: 'recipient_id'
  belongs_to :dispatcher, class_name: 'User', foreign_key: 'dispatcher_id'
end
