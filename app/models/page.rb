class Page < ActiveRecord::Base
  include Slugifier
  belongs_to :parent, class_name: 'Page', foreign_key: 'parent_id'
  has_many :children, :class_name => 'Page', foreign_key: 'parent_id'

  before_create :create_slug
  scope :published, -> { Page.where(published: true) }

  private

  def create_slug
    unless self.slug.present?
      self.slug = self.generate_slug(self.title)
    end
  end
end
