class Booking < CalendarEvent
  belongs_to :company
  belongs_to :menu_item

  validates :name, :phone, :email, presence: true
  validates :email, format: { with: /\A(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})\z/i }
  validates :phone, format: { with: /\A\d{8}/ }
end
