Sanatid.FeedbackView = Ember.View.extend
  templateName: 'feedback'
  visible: false
  toTADA: null

  didInsertElement: ->
    $('.feedback > .button').click ->
      setTimeout ->
        $('.ui.dropdown').dropdown()
      , 100

    @set('toTADA', setTimeout =>
      @$('.fixed-box').transition('tada')
    , 5000)

  didSendSubmission: ( ->
    if @get('controller.submitted')
      @set('controller.submitted', false)
      setTimeout =>
        @toggleProperty('visible')
      , 1500
  ).observes('controller.submitted')

  actions:
    toggle: ->
      @toggleProperty('visible')
      clearInterval @get('toTADA')
