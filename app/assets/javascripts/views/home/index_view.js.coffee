Sanatid.HomeIndexView = Ember.View.extend

  didInsertElement: ->

    @$('option:first').attr('disabled', true);

    $('select').selectmenu
      width: '100%'
      change: ->
        $('select').trigger('change')
