Sanatid.HomeCompanyReviewsView = Ember.View.extend
  didInsertElement: ->
    @$('.rating').rating('disable')

    # Set width of rating hearts.
    @$('.rating.fill-in').css(width: "#{@get('controller.averageRatingInPercent')}%")