Sanatid.HomeLoginView = Ember.View.extend
  didInsertElement: ->
    if localStorage.getItem('email') && localStorage.getItem('password')
      @set('controller.email', localStorage.getItem('email'))
      @set('controller.password', localStorage.getItem('password'))
      # Reset.
      localStorage.removeItem('email')
      localStorage.removeItem('password')
      # Trigger login.
      @get('controller').send('login')
