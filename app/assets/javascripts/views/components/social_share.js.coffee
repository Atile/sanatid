Sanatid.SocialShareView = Ember.View.extend
  templateName: 'components/social_share'
  # TODO: localize.
  twitterText: "Interessant artikel fra @sanatid"
  twitterRef: "sanatid"

  detectUrlChange: ( ->
    # Refresh facebook widget.
    $('#fb').attr('data-href', window.location)
    try
      FB.XFBML.parse()
    catch e

    # Refresh twitter widget.
    twitterQuery = "https://twitter.com/share?text=#{@get('twitterText')}&related=#{@get('twitterRef')}"
    $('.twitter-container').html('')
    $('.twitter-container').html("<a href='#{twitterQuery}' class='twitter-share-button' data-url='#{window.location.href}'>Tweet</a>");

    try
      twttr.widgets.load()
    catch e

    $.ajax({url: 'https://platform.twitter.com/widgets.js', dataType: 'script', cache:true});

  ).observes('Sanatid.currentPathname')


