Sanatid.CustomerPanelView = Ember.View.extend
  triggeredModal: false

  initModal: ( ->
    if @get('controller.gotUndecidedClips')
      @$('.ui.undecided-clip.modal').modal('setting',
        closable: false,
        transition: 'vertical flip'
      ).modal('show')

  ).on('didInsertElement')

  onComplete: ( ->
    if !@get('controller.gotUndecidedClips') and !@get('controller.review')
      $('.ui.undecided-clip.modal').modal('hide')
      @set('controller.showReview', false)
  ).observes('controller.gotUndecidedClips', 'controller.review')

  initReviewWidget: ( ->
    Ember.run.next =>
      $('.ui.rating').rating()
      $('.ui.rating .icon').click =>
        @set('controller.review.rating', $('.ui.rating').rating('get rating'))
  ).observes('controller.review', 'controller.showReview')
