if window.history && window.history.pushState
  Sanatid.Router.reopen
    location: 'history'

Sanatid.Router.map () ->

  ################
  # Home section #
  ################

  @resource 'home', path: '/', ->
    @route 'login'
    @route 'forgot', path: 'login/forgot-password'
    @route 'employees'
    @route 'contact'
    @route 'how'
    @route 'products', path: 'signup'
    @route 'demo'
    @route 'unsubscribe'

    @resource 'home.companies', path: 'praksisser', ->
      @route 'menu', path: 'order/:company'

    @resource 'home.wiki', path: 'wiki', ->
      @route 'service', path: ':service_field/:service'
      @route 'service_field', path: ':service_field'

    @resource 'home.blog', path: '/blog', ->
      @route 'show', path: ':blog'

    @resource 'home.pages', path: 'info', ->
      @route 'show', path: ':page'

    @resource 'home.guides', path: 'guides', ->
      @route 'show', path: ':page'

    @resource 'home.company', path: ':company', ->
      @route 'company_page', path: ':company_page'
      @route 'services', path: 'ydelser'

    # Four oh Four!
    @route 'fourohfour', path: '*path'

  ####################
  # Customer section #
  ####################

  @resource 'customer_panel', path: 'panel/customer', ->
    @route 'settings'

  ###################
  # Company section #
  ###################

  @resource 'company_panel', path: 'panel/company', ->
    @route 'subscriptions'
    @route 'calendar'

    @resource 'company_panel.settings', path: 'settings', ->
      @route 'services'
      @route 'account'
      @route 'practice'
      @route 'profile'
