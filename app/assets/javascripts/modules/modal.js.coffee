jQuery.fn.extend
  showModal: (options) ->
    if options.content
      $(@).children('.content').html(options.content)

    if options.shade
      $('<div class="shade"></div>').appendTo('body').fadeIn()

    if options.autoHide
      $('.shade').click => $(@).hideModal()

      $(document).keyup (e) =>
        if (e.keyCode == 27)
          $(@).hideModal()

    if options.img
      image = new Image()
      image.src = options.img;

      image.onload = =>
        $(@).css
          'width': image.width
          'margin-left': "-#{image.width / 2}px"
          'margin-top': "-#{image.height / 3}px"
        $(@).children('.content').html(image)

    $(@).fadeIn()

  hideModal: ->
    $(@).fadeOut()
    $('.shade').fadeOut -> $(@).remove();

    $('.shade').unbind('click')
    $(document).unbind('keyup')