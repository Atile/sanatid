jQuery.fn.extend
  showDimmer: ->
    $(@).children('.dimmer').fadeIn()

  hideDimmer: ->
    $(@).children('.dimmer').fadeOut()

  showDimmerSuccess: ->
    $(@).find('.dimmer .content').fadeOut 500, =>
      $(@).find('.dimmer .success').fadeIn()

  resetDimmer: ->
    $(@).find('.dimmer > div:not(.content)').fadeOut =>
      $(@).find('.dimmer .content').fadeIn()
