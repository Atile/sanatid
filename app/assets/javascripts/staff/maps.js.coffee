$(document).ready ->
  # Init map.
  return unless $('#map').length

  map = L.map('map').setView([55.988, 10.533], 8)
  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    maxZoom: 12
  ).addTo(map)
  # Append layers.
  clusterGroup = new L.MarkerClusterGroup()
  map.addLayer(clusterGroup)

  circleGroup = new L.layerGroup()
  map.addLayer(circleGroup)

  L.updateMap = (cords, mode, distance) ->

    # Reset map layers.
    clusterGroup.clearLayers()
    circleGroup.clearLayers()

    $(cords).each (i, c) ->
      circleGroup.addLayer L.circle([c.latitude, c.longitude], distance) if mode == "radius"
      clusterGroup.addLayer L.marker([c.latitude, c.longitude]) if mode == "pins"





