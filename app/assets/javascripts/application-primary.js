// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui/core
//= require jquery-ui/widget
//= require jquery-ui/mouse
//= require jquery-ui/datepicker
//= require jquery-ui/position
//= require jquery-ui/selectmenu
//= require semantic-ui/modules/transition.js
//= require i18n/da
//= require utils/demo_landing
//= require utils/utilities
// *** Ember ***
//= require handlebars
//= require ember
//= require ember-data
//= require ember-validations/index
//= require conf
// *** Bower ***
//= require gl-datepicker/glDatePicker
//= require ember-i18n/lib/i18n
//= require moment/moment
//= require jquery-fileupload/basic
// *** App ***
//= require_self
//= require sanatid


DS.Model.reopen({
  adapterDidInvalidate: function(errors) {
    recordErrors = this.get('errors');
    for (var error in errors) {
      if(!errors.hasOwnProperty(error)) {
        continue;
      }

      recordErrors.add(error, errors[error]);
    }
  }
});

// for more details see: http://emberjs.com/guides/application/
Sanatid = Ember.Application.create({
  conf: config.da,
  currentPath: '',
  currentPathname: ''
});

// Set moment lang.
moment.lang('da');

// Load in translations, and add shortcut to t.
Em.I18n.translations = translations;
window.t = Em.I18n.t;

Sanatid.initializer({
  // Initialize authManager, and make it available in controllers and routes.
  name: 'Init AuthenticationManager',
  initialize: function(container, application) {
    container.register('authentication:manager',  Sanatid.AuthManager);
    container.injection('authentication:manager', 'store', 'store:main');
    container.injection('controller', 'authManager', 'authentication:manager');
    container.injection('route', 'authManager', 'authentication:manager');
  }
});

Sanatid.initializer({
  name: 'Observe url changes',

  initialize: function(container, application) {
    var router = container.lookup('router:main');

    router.addObserver('url', function() {
      var lastUrl = undefined;
      return function() {
        Ember.run.next(function() {
          var url = router.get('url');
          if (url !== lastUrl) {
            lastUrl = url;
            Sanatid.set('currentPathname', window.location.pathname)
          }
        });
      };
    }());
  }
});

Sanatid.Adapter = DS.ActiveModelAdapter.extend({
  namespace: 'api',
  buildURL: function(record, suffix) {
    var url = this._super(record, suffix);
    return url + ".json";
  }
});

Sanatid.ApplicationStore = DS.Store.extend({
  adapter: Sanatid.Adapter
});


// Add translateable attributes to views.
Ember.TextField.reopen(Em.I18n.TranslateableAttributes);
Ember.TextArea.reopen(Em.I18n.TranslateableAttributes);

