Sanatid.NewsletterSubscriber = DS.Model.extend Ember.Validations.Mixin,
  user: DS.belongsTo('user')
  email: DS.attr('string')
  active: DS.attr('boolean')

Sanatid.NewsletterSubscriber.reopen
  validations:
    email:
      presence: true
      presence: { message: t('home.contact.errors.emailPresence') }
      format: { with: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, message: t('home.contact.errors.emailFormat')}