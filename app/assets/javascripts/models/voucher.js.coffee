Sanatid.Voucher = DS.Model.extend
  customer: DS.belongsTo('customer')
  company: DS.belongsTo('company')
  clips: DS.hasMany('clip')
  clipCount: DS.attr('number')
  createdAt: DS.attr('string')