Sanatid.Product = DS.Model.extend
  name: DS.attr('string')
  description: DS.attr('string')
  fundamental: DS.attr('boolean')
  price: DS.attr('number')
  duration: DS.attr('string')
  commission: DS.attr('number')
  billingPeriod: DS.attr('string')
  productFamily: DS.belongsTo('productFamily')
  iconClass: DS.attr('string')

  tBillingPeriod: ( ->
    t("global.billingPeriod.#{@get('billingPeriod')}")
  ).property('billingPeriod')