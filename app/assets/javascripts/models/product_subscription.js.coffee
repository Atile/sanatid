Sanatid.ProductSubscription = DS.Model.extend
  expiresAt: DS.attr('string')
  renewedAt: DS.attr('string')
  nextRenewalAt: DS.attr('string')
  createdAt: DS.attr('string')
  active: DS.attr('boolean')
  company: DS.belongsTo('company')
  product: DS.belongsTo('product')