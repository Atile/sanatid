Sanatid.CommissionLine = DS.Model.extend
  company: DS.belongsTo('company')
  month: DS.attr('number')
  year: DS.attr('number')
  amount: DS.attr('number')
  payed: DS.attr('boolean')
  percentage: DS.attr('number')