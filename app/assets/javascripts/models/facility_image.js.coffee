Sanatid.FacilityImage = DS.Model.extend
  name: DS.attr('string')
  description: DS.attr('string')
  original: DS.attr('string')
  thumb: DS.attr('string')
  company: DS.belongsTo('company')