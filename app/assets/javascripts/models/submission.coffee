Sanatid.Submission = DS.Model.extend Ember.Validations.Mixin,
  form: DS.attr('string')
  subject: DS.attr('string')
  name: DS.attr('string')
  email: DS.attr('string')
  message: DS.attr('string')

Sanatid.Submission.reopen
  validations:
    subject:
      presence: true
      presence: { message: t('home.contact.errors.subjectPresence') }
    name:
      presence: true
      presence: { message: t('home.contact.errors.namePresence') }
    email:
      presence: true
      presence: { message: t('home.contact.errors.emailPresence') }
      format: { with: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, message: t('home.contact.errors.emailFormat')}
    message:
      presence: true
      presence: { message: t('home.contact.errors.messagePresence') }
      length: {minimum: 60, messages: { tooShort: t('home.contact.errors.messageTooShort')} }