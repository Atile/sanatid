Sanatid.MenuItem = DS.Model.extend
  name: DS.attr('string')
  description: DS.attr('string')
  duration: DS.attr('number')
  price: DS.attr('number')
  company: DS.belongsTo('company')
