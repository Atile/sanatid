Sanatid.Company = Sanatid.User.extend
  serviceFields: DS.hasMany('serviceField')
  serviceField: DS.belongsTo('serviceField')
  companyName: DS.attr('string')
  companyEmail: DS.attr('string')
  cvr: DS.attr('number')
  phone: DS.attr('string')
  homepage: DS.attr('string')
  email: DS.attr('string')
  description: DS.attr('string')
  slug: DS.attr('string')
  subscriptionStatus: DS.attr('string')
  reviews: DS.hasMany('review')
  reviewCount: DS.attr('number')
  averageRating: DS.attr('number')
  openingHours: DS.attr()
  logo: DS.attr('string')
  address: DS.attr('string')
  commissionLines: DS.hasMany('commissionLine')
  statistics: DS.attr()
  paymentMethods: DS.attr()
  distanceTo: DS.attr('string')
  isLead: DS.attr('boolean')
  region: DS.attr('string')
  menuItems: DS.hasMany('menuItem')
  facilityImages: DS.hasMany('facilityImage')

  isCompany: true

  city: ( ->
    if @get('address') && @get('address').match(/[\d]{4}\s([A-Za-zÅåøØæÆ]+)/)
      return @get('address').match(/[\d]{4}\s([A-Za-zÅåøØæÆ]+)/)[1]
  ).property('address')

  zip: ( ->
    if @get('address') && @get('address').match(/[\d]{4}/)
      @get('address').match(/[\d]{4}/)[0]
  ).property('address')

  averageRatingInPercent: ( ->
      @get('averageRating') * 10
  ).property('averageRating')
