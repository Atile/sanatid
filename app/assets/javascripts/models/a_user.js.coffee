# Should be loaded before parent objects, therfor the a_ in the name.
Sanatid.User = DS.Model.extend
  name: DS.attr('string')
  email: DS.attr('string')
  password: DS.attr('string')
  passwordConfirmation: DS.attr('string')
  phone: DS.attr('number')
  vouchers: DS.hasMany('voucher')
  newsletterSubscriber: DS.belongsTo('newsletterSubscriber')
  recipients: DS.hasMany('message', inverse: 'recipient')
  dispatchers: DS.hasMany('message', inverse: 'dispatcher')
