Sanatid.Message = DS.Model.extend Ember.Validations.Mixin,
  subject: DS.attr('string')
  content: DS.attr('string')
  createdAt: DS.attr('string')
  recipient: DS.belongsTo('user', polymorphic: true, inverse: 'recipients')
  dispatcher: DS.belongsTo('user', polymorphic: true, inverse: 'dispatchers')

Sanatid.Message.reopen
  validations:
    content:
      presence: true
      presence: { message: t('global.message.contentPresence') }
      length: { minimum: 20, messages: { tooShort: t('global.message.contentTooShort') } }
