Sanatid.Staff = DS.Model.extend
  name: DS.attr('string')
  jobTitle: DS.attr('string')
  description: DS.attr('string')
  email: DS.attr('string')
  phone: DS.attr('number')
  avatar: DS.attr('string')
  smallAvatar: DS.attr('string')
  linkedin: DS.attr('string')
  twitter: DS.attr('string')