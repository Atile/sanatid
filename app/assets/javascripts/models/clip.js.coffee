Sanatid.Clip = DS.Model.extend
  voucher: DS.belongsTo('voucher')
  createdAt: DS.attr('string')
  status: DS.attr('string')

  isUndecided: ( ->
    return true if @get('status') is 'undecided'
    false
  ).property('status')

  isConfirmed: ( ->
    return true if @get('status') is 'confirmed'
    false
  ).property('status')