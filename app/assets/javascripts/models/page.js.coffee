Sanatid.Page = DS.Model.extend
  title: DS.attr('string')
  content: DS.attr('string')
  slug: DS.attr('string')
  category: DS.attr('string')
  parent: DS.belongsTo('page')