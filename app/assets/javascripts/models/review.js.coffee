Sanatid.Review = DS.Model.extend Ember.Validations.Mixin,
  company: DS.belongsTo('company')
  customer: DS.belongsTo('customer')
  createdAt: DS.attr('string')
  content: DS.attr('string')
  rating: DS.attr('number')


Sanatid.Review.reopen
  validations:
    content:
      length: { minimum: 40, messages: { tooShort: t('customer_panel.undecided_clip_modal.review.errors.contentTooShort') }}
    rating:
      presence: true
      presence: { message: t('customer_panel.undecided_clip_modal.review.errors.ratingPresence') }
