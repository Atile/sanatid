Sanatid.ApplicationRoute = Ember.Route.extend
  init: ->
    $('meta[name="description"]').attr('content', t('home.meta.description'))

  setupController: (controller, model) ->
    controller.set('pages', @store.find('page', category: 'footer'))
    @controllerFor('feedback').set('model', @store.createRecord('submission', {form: 'feedback'}))

  controllerChanged: (->
    @controllerFor('application').set('currentController', @controllerFor(Sanatid.get('currentPath')))
  ).observes('Sanatid.currentPath')
