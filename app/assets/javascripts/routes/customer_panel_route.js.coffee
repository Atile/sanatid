Sanatid.CustomerPanelRoute = Sanatid.CustomerAuthenticatedRoute.extend
  renderTemplate: ->

    @render 'customer_panel/navigation',
      outlet: 'navigation'
      into: 'application'

    @render 'home/breadcrumbs',
      controller: 'homeBreadcrumb'
      outlet: 'breadcrumbs'
      into: 'application'

    @render()

  model: ->
    @store.find('customer', localStorage.getItem('user'))
