Sanatid.CustomerAuthenticatedRoute = Ember.Route.extend
  beforeModel: (transition) ->
    unless @authManager.softCheck('customer')
      @redirectToLogin(transition)

    @authManager.initAuthentication().then (user) ->
      console.log('authenticated')
    , =>
      @redirectToLogin(transition)

    # General redirect.
    $(document).ajaxError ( event, request, settings) =>
      if request.status == 401
        @redirectToLogin(transition)

  redirectToLogin: (transition)->
    userController = @controllerFor('home.login')
    userController.set('attemptedTransition', transition)
    @transitionTo('home.login')

  actions:
    logout: ->
      @authManager.reset()
      # Reset application.
      Sanatid.reset()
      @transitionTo('home')

Sanatid.CompanyAuthenticatedRoute = Ember.Route.extend
  beforeModel: (transition) ->
    unless @authManager.softCheck('company')
      @redirectToLogin(transition)

    @authManager.initAuthentication().then (user) ->
      console.log('authenticated')
    , =>
      @redirectToLogin(transition)

    # General redirect.
    $(document).ajaxError ( event, request, settings) =>
      if request.status == 401
        @redirectToLogin(transition)


  redirectToLogin: (transition)->
    userController = @controllerFor('home.login')
    userController.set('attemptedTransition', transition)
    @transitionTo('home.login')

  actions:
    logout: ->
      @transitionTo('home')
      @authManager.reset()
