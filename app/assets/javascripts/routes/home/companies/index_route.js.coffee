Sanatid.HomeCompaniesIndexRoute = Ember.Route.extend Ember.GoogleAnalyticsTrackingMixin,
  model: (params) ->
    if params?
      @store.find('company', { lon: params.lon, lat: params.lat, service_field: params.serviceFieldId })

  beforeModel: ->
    @trackEvent('flow', 'start')

  actions:
    queryParamsDidChange: ->
      @refresh()