Sanatid.HomeWikiRoute = Ember.Route.extend
  deactivate: ->
    $('meta[name="description"]').remove()
    $('head').append "<meta name='description' content='#{Em.I18n.translations.home.meta.description}'>"
    document.title = t('home.meta.title')
