Sanatid.HomeWikiServiceFieldRoute = Ember.Route.extend
  model: (params) ->
    # Fetch only the first object.
    @store.find('serviceField', slug: params.service_field).then (array) ->
      array.get('firstObject')

  setupController: (controller, model) ->
    controller.set('serviceFields', @store.find('serviceField'))
    controller.set('model', model)

  serialize: (model) ->
     service_field: model.get('slug')

  afterModel: (model) ->
    # Update meta description.
    $('meta[name="description"]').remove()
    $('head').append "<meta name='description' content='Sanatid - #{model.get('metaDescription')}'>"
    document.title = 'Sanatid.dk | ' + model.get('name')