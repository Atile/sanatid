Sanatid.HomeRoute = Ember.Route.extend
  renderTemplate: ->

    @render 'home/navigation',
      outlet: 'navigation'
      into: 'application'

    @render()

  beforeModel: ->
    # Init authentication, if a token is present.
    if localStorage.getItem('user')
      @authManager.initAuthentication().then ->
        console.log 'authenticated'
      , ->
        console.log 'denied'

  actions:
    # Called from within the start flow by city component
    globalInitFlow: (params) ->
      @transitionTo('home.companies', queryParams: params)

Sanatid.HomeIndexRoute = Ember.Route.extend
  setupController: (controller) ->
    controller.set('serviceFields', @store.find('serviceField'))
    controller.set('serviceFields.services', @store.find('service'))
    controller.set('companies', @store.find('company', limit: 12))

    @render()