Sanatid.HomeBlogIndexRoute = Ember.Route.extend
  model: ->
    @store.find('blog')

Sanatid.HomeBlogShowRoute = Ember.Route.extend
  model: (params) ->
    @store.find('blog', slug: params.blog).then (array) ->
      array.get('firstObject')

  serialize: (model) ->
    blog: model.get('slug')

  afterModel: (model) ->
    # Update meta description.
    $('meta[name="description"]').remove()
    $('head').append "<meta name='description' content='Sanatid - #{model.get('metaDescription')}'>"
    document.title = 'Sanatid | ' + model.get('title')

  deactivate: ->
    # Reset meta description.
    $('meta[name="description"]').remove()
    $('head').append "<meta name='description' content='#{Em.I18n.translations.home.meta.description}'>"
    document.title = 'Sanatid'
