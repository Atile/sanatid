Sanatid.HomeCompanyRoute = Ember.Route.extend
  model: (params) ->
    @store.find('company', slug: params.company).then (array) ->
      array.get('firstObject')

  afterModel: (model) ->
    unless model
      @transitionTo('home.fourohfour', 'FourOhFour')

    document.title = 'Sanatid.dk | ' + model.get('companyName')

  deactivate: ->
    document.title = t('home.meta.title')

  serialize: (model) ->
    company: model.get('slug')
