Sanatid.CompanyPanelRoute = Sanatid.CompanyAuthenticatedRoute.extend
  renderTemplate: ->
    @render 'company_panel/navigation',
      outlet: 'navigation'
      into: 'application'

    @render 'home/breadcrumbs',
      controller: 'homeBreadcrumb'
      outlet: 'breadcrumbs'
      into: 'application'

    @render()

  model: ->
    @store.find('company', localStorage.getItem('user')).then (company) ->
      if company.get('statistics')
        return company
      company.reload()
