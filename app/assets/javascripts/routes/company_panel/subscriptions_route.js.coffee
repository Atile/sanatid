Sanatid.CompanyPanelSubscriptionsRoute = Ember.Route.extend
  model: (params) ->
    @store.find('productSubscription')

  setupController: (controller, model) ->
    controller.set('model', model)
    controller.set('productFamilies', @store.find('productFamily'))
    controller.set('commissionLines', @store.find('commissionLine'))