Sanatid.CompanyPanelSettingsProfileRoute = Sanatid.CompanyAuthenticatedRoute.extend
  model: ->
    @store.find('company', localStorage.getItem('user'))

  setupController: (controller, model) ->
    controller.set('model', model)

    @store.find('serviceField').then (serviceFields) ->
      serviceFields.forEach (serviceField, index) =>

        # Compare them to the ones of the company.
        controller.get('serviceFields').forEach (currentServiceField, index) ->
          if serviceField is currentServiceField
            serviceField.set('selected', true)

        # If the service did't get selected, deselect it.
        unless serviceField.get('selected')
          serviceField.set('selected', false)


      # Assign modified services to controller.
      controller.set('allServiceFields', serviceFields)



