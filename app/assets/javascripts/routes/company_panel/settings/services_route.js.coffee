Sanatid.CompanyPanelSettingsServicesRoute = Sanatid.CompanyAuthenticatedRoute.extend
  model: ->
    @store.find('company', localStorage.getItem('user'))

  setupController: (controller, model) ->
    controller.set('model', model)
    controller.set('newMenuItem', @store.createRecord('menuItem'))