Sanatid.CompanyPanelIndexRoute = Ember.Route.extend

  beforeModel: ->
    # In the first version of Sanatid, the dashboard is absent.
    @transitionTo('company_panel.settings.profile')

  setupController: (controller, model) ->
    controller.set('model', @store.find('company', localStorage.getItem('user')))
