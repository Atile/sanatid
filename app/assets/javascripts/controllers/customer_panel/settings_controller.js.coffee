Sanatid.CustomerPanelSettingsController = Ember.ObjectController.extend
  title: t('customer_panel.settings.title')
  newsletter: ( ->
    if @get('newsletterSubscriber')
      true
    else
      false
  ).property('newsletterSubscriber')


  actions:
    saveAccount: ->
      $('.ui.dimmable .dimmer.processing')
        .dimmer(
          closable: false
        )
        .dimmer('show')

      @get('model').save().then =>
        $('.ui.dimmable .dimmer.processing').dimmer('hide')
        $('.ui.dimmable .dimmer.success').dimmer('show')

        #reset password fields.
        @set('password', null)
        @set('password_confirmation', null)

        setTimeout ->
          $('.ui.dimmable .dimmer.success').dimmer('hide')
        , 2000

    subscribeNewsletter: ->
      if @get('newsletter')
        @get('newsletterSubscriber').deleteRecord()
        @get('newsletterSubscriber').save()
        # Set manually as Ember won't detect delete as a property change.
        @set('newsletter', false)
      else
        @set('newsletterSubscriber', @store.createRecord('newsletterSubscriber', {email: @get('email')}))
        @get('newsletterSubscriber').save()
        @set('newsletter', true)
