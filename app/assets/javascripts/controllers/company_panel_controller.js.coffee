Sanatid.CompanyPanelController = Ember.ObjectController.extend
  needs: ['application']
  app: Ember.computed.alias('controllers.application')

  subscriptionExpired: (->
    return true if @get('subscriptionStatus') == 'expired'
    false
  ).property('subscriptionStatus')

  subscriptionCanceled: (->
    return true if @get('subscriptionStatus') == 'canceled'
    false
  ).property('subscriptionStatus')

  subscriptionActive: (->
    return true if @get('subscriptionStatus') == 'active'
    false
  ).property('subscriptionStatus')

