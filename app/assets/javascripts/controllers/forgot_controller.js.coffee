Sanatid.HomeForgotController = Ember.ObjectController.extend
  title: 'Reset password'

  email: null
  success: null
  failure: null

  actions:
    reset: ->
      $('.login-form').dimmer('show', {
        closable: false
      })

      $.post('/api/session/reset.json', session: @getProperties('email'), (result) =>
        if result.status == 'success'
          @set('success', true)
          @set('failure', false)

      ).fail( =>
        @set('failure', true)
        @set('success', false)
      ).always =>
        $('.login-form').dimmer('hide')
        @set('email', null)
