Sanatid.ApplicationController = Ember.Controller.extend
  # Public.
  pages: null
  currentController: null
  feedbackSubmission: null

  pathChanged: (->
    Sanatid.set('currentPath', @get('currentPath'))
  ).observes('currentPath')

  currentUser: (->
    @authManager.get('apiKey.user')
  ).property('authManager.apiKey.user')

  isAuthenticated: (->
    @authManager.isAuthenticated()
  ).property('authManager.apiKey')

  isDemoEnv: (->
    if window.env == "demo"
      return true
    false
  ).property('model')

  isFrontpage: (->
    @get('currentPath') == "home.index"
  ).property('currentPath')

  logoLink: ( ->
    if @get('authManager').softCheck('company')
      'company_panel.index'
    else if @get('authManager').softCheck('customer')
      'customer_panel.index'
    else
      'home.index'
  ).property('authManager.apiKey.user')


