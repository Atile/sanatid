Sanatid.CompanyPanelSettingsProfileController = Ember.ObjectController.extend
  title: t('company_panel.settings.profile.title')

  # Public.
  allServiceFields: null

  didSelectServiceField: (->
    return unless @get('allServiceFields')

    @get('allServiceFields').forEach (serviceField) =>
      if serviceField.get('selected')
        @get('serviceFields').pushObject(serviceField)
      else
        @get('serviceFields').removeObject(serviceField)

  ).observes('allServiceFields.@each.selected')

  canUploadFacilityImage: ( ->
    @get('facilityImages.length') < 5
  ).property('facilityImages.@each')

  actions:
    updateProfile: ->
      $('.dimmable').showDimmer()

      @get('model').save().then =>
        $('.dimmable').showDimmerSuccess()

        setTimeout ->
          $('.dimmable').hideDimmer()
        , 2000
      , =>
          #reset password fields.
        @set('password', null)
        @set('password_confirmation', null)

        setTimeout ->
          $('.dimmable').hideDimmer()
        , 500

    removeImage: (image) ->
      $('.dimmable').resetDimmer()
      image.destroyRecord();
