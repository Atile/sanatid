Sanatid.CompanyPanelSettingsAccountController = Ember.ObjectController.extend
  title: t('company_panel.settings.account.title')

  newsletter: ( ->
    if @get('newsletterSubscriber')
      true
    else
      false
  ).property('newsletterSubscriber')

  actions:
    saveAccount: ->
      $('.dimmable').showDimmer()

      @get('model').save().then =>
        $('.dimmable').showDimmerSuccess()

        #reset password fields.
        @set('password', null)
        @set('password_confirmation', null)

        setTimeout ->
          $('.dimmable').hideDimmer()
        , 2000
      , =>
        #reset password fields.
        @set('password', null)
        @set('password_confirmation', null)

        setTimeout ->
          $('.dimmable').hideDimmer()
        , 500




    subscribeNewsletter: ->
      if @get('newsletter')
        @get('newsletterSubscriber').deleteRecord()
        @get('newsletterSubscriber').save()
        # Set manually as Ember won't detect delete as a property change.
        @set('newsletter', false)
      else
        @set('newsletterSubscriber', @store.createRecord('newsletterSubscriber', {email: @get('email')}))
        @get('newsletterSubscriber').save()
        @set('newsletter', true)
