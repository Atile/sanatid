Sanatid.CompanyPanelSettingsPracticeController = Ember.ObjectController.extend
  title: t('company_panel.settings.practice.title')
  serviceFields: null

  actions:
    saveAccount: ->
      $('.ui.dimmable .dimmer.processing')
        .dimmer(
          closable: false
        )
        .dimmer('show')

      @get('model').save().then ->
        $('.ui.dimmable .dimmer.processing').dimmer('hide')
        $('.ui.dimmable .dimmer.success').dimmer('show')

        setTimeout ->
          $('.ui.dimmable .dimmer.success').dimmer('hide')
        , 2000

    toggleService: (service) ->
      # Check if the service is already added.
      removed = false
      @get('services').forEach (currentService, index) =>
        if service == currentService
          @get('services').removeObject(service)
          service.set('selected', false)
          removed = true

      unless removed
        @get('services').pushObject(service)
        service.set('selected', true)