Sanatid.CompanyPanelSettingsServicesController = Ember.ObjectController.extend
  title: 'Services'

  # Public.
  newMenuItem: null

  actions:
    addMenuItem: ->
      @get('model.menuItems').pushObject(@get('newMenuItem'))
      @get('newMenuItem').save()

      @set('newMenuItem', @store.createRecord('menuItem'))

    deleteMenuItem: (menuItem) ->
      confirmDestroy = confirm("Er du sikker på du vil slette #{menuItem.get('name')}")
      if confirmDestroy
        menuItem.destroyRecord()

    editMenuItem: (menuItem) ->
      menuItem.set('editing', true)

    updateMenuItem: (menuItem) ->
      menuItem.save()
      menuItem.set('editing', false)

    cancelEdit: (menuItem) ->
      menuItem.set('editing', false)

    saveAccount: ->
      $('.ui.dimmable .dimmer.processing')
        .dimmer(
          closable: false
        )
        .dimmer('show')

      @get('model').save().then =>
        $('.ui.dimmable .dimmer.processing').dimmer('hide')
        $('.ui.dimmable .dimmer.success').dimmer('show')

        #reset password fields.
        @set('password', null)
        @set('password_confirmation', null)

        setTimeout ->
          $('.ui.dimmable .dimmer.success').dimmer('hide')
        , 2000
