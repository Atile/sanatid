Sanatid.CompanyPanelSubscriptionsController = Ember.ArrayController.extend
  title: t('company_panel.subscriptions.title')

  needs: ['companyPanel']
  root: Ember.computed.alias('controllers.companyPanel')

  # Public.
  commissionLines: []
  publicCommissionLine: null
  productFamilies: null
  confirmProduct: null
  loadingSubscription: false
  showMessage: false

  fundamentalProductFamily: ( ->
    productFamily = null
    # Get subscription state, for family and product. It determines what actions the company can make.
    subscriptionExist = false
    gotInactiveSubscription = null
    subscriptionId = null

    @get('productFamilies').forEach (pf) =>
      pf.get('products').forEach (product) =>
        return unless product.get('fundamental')

        @get('model').forEach (sub) ->
          if sub.get('product') == product
            product.set('subscription', sub)
            subscriptionExist = true
            subscriptionId = sub.get('id')

            gotInactiveSubscription = true
            if sub.get('active')
              gotInactiveSubscription = false
          else
            product.set('subscription', null)

        pf.set('subscriptionId', subscriptionId)
        pf.set('subscriptionExist', subscriptionExist)
        pf.set('gotInactiveSubscription', gotInactiveSubscription)
        productFamily = pf
    productFamily
  ).property('productFamilies.@each')

  nonFundamentalproductFamilies: ( ->
    return unless @get('fundamentalProductFamily')

    families = []
    @get('productFamilies').forEach (pf) =>
      fundamental = false
      pf.get('products').forEach (product) =>
        fundamental = true if product.get('fundamental')
        @get('model').forEach (sub) ->
          if sub.get('product') == product && sub.get('active')
            product.set('subscription', sub)

            pf.set('expiresAt', sub.get('expiresAt')) if sub.get('expiresAt')
            pf.set('nextRenewalAt', sub.get('nextRenewalAt')) if sub.get('nextRenewalAt')
            pf.set('gotSubscription', true)
            pf.set('subscriptionId', sub.get('id'))

      # only push product family, if it doesn't consist of a fundamental family.
      families.pushObject pf unless fundamental
    families
  ).property('fundamentalProductFamily.productComponentFamilies.@each')

  activateConfirmModal: (head, path, data) ->
    $('.ui.modal.confirm .header').html(head)
    $('.ui.modal.confirm').modal('setting',
      transition: 'vertical flip'
      onApprove: =>
        @set('loadingSubscription', true)

        $.post( "https://#{location.host}/#{path}",
          data
        , (result) =>
          @store.pushPayload('company', result) if result?
          @set('showMessage', true)
          $('.message .close').on 'click', =>
            $('.message').fadeOut()
            @set('showMessage', false)

          # Fetch the updated productSubscription.
          @store.find('productSubscription').then =>
            @propertyDidChange('fundamentalProductFamily', 'nonFundamentalproductFamilies')
        ).fail( =>
          alert('Vi kunne desværre ikke ændre dit abonnement, kontakt venligst supporten')
        ).always =>
          @set('loadingSubscription', false)
      onHide: =>
        @set('confirmProduct', null)
    ).modal('show')


  actions:
    changeProduct: (productFamily, product) ->
      @set('confirmProduct', product)

      title = '<i class="circle up icon"></i>Opgrader/Nedgrader produkt'
      data = { product_subscription: { product_id: product.get('id' ) } }
      path = "api/product_subscriptions/change_subscription_product/#{productFamily.subscriptionId}.json"
      @activateConfirmModal(title, path, data)


    cancelSubscription: (productFamily, product) ->
      @set('confirmProduct', product)

      title = '<i class="circle arrow down icon"></i> Afmeld medlemskab'
      path = "api/product_subscriptions/cancel_subscription/#{productFamily.subscriptionId}.json"
      @activateConfirmModal(title, path)


    signupSubscription: (product) ->
      @set('confirmProduct', product)

      title = '<i class="circle add sign icon"></i> Opret medlemskab'
      path = "api/product_subscriptions/signup_subscription/#{product.get('id')}.json"
      @activateConfirmModal(title, path)

    reactivateSubscription: (pf, product) ->
      @set('confirmProduct', product)

      title = '<i class="circle add sign icon"></i> Genaktiver medlemskab'
      path = "api/product_subscriptions/reactivate_subscription/#{pf.get('subscriptionId')}.json"
      @activateConfirmModal(title, path)


    showJobs: (commissionLine) ->
      @set('activeCommissionLine', commissionLine)
      $('.commission-details').transition('pulse')

