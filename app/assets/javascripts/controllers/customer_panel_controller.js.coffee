Sanatid.CustomerPanelController = Ember.ObjectController.extend
  needs: ['application']
  app: Ember.computed.alias('controllers.application')

  # public.
  supportTicket: null
  supportTicketValidated: false
  supportActiveClip: null
  review: null
  showReview: false
  reviewValidated: false

  clips: ( ->
    clips = []
    @get('vouchers').forEach (voucher) ->
      voucher.get('clips').forEach (clip) ->
        clips.push(clip)
    clips
  ).property('voucher.@each')

  gotUndecidedClips: ( ->
    gotUndecided = false
    @get('clips').forEach (clip) ->
      if clip.get('status') is "undecided"
        gotUndecided = true
    gotUndecided
  ).property('clips.@each.status')


  actions:
    acceptClip: (clip) ->
      # Create review instance for customer to forfil.
      @set('review', @store.createRecord('review', {job: clip.get('voucher.offer.job'), customer: @get('app.currentUser'), company: clip.get('voucher.company')}))

      clip.set('status', 'confirmed')
      clip.save().then =>
        # Only let customer review, if it is the last clip on the voucher, or single session. Meaning the job is completed.
        isCompleted = (clip.get('voucher.clipCount') - clip.get('voucher.clips.length')) == 0
        # If customer already written a review, discontinue the created.
        if clip.get('voucher.offer.job.review.rating') or !isCompleted
          @set('review', null)
        else
          # Use showReview in order to avoid, 100 ms of showing review form.
          @set('showReview', true)


    submitReview: ->
      @set('reviewValidated', true)

      @get('review').validate().then =>
        $('.undecided-clip .dimmable .dimmer.processing').dimmer(
          closable: false
        ).dimmer('show')
        @get('review').save().then =>
          @set('reviewSuccess', true)

          # Reset.
          @set('review', null)
          @set('reviewValidated', false)

          $('.ui.dimmable .dimmer.processing').dimmer('hide')
          $('.ui.dimmable .dimmer.success').dimmer('show')

          # Tell customer, thx for the review.
          Sanatid.utils.setMessage('Tak for din bedømmelse', 'Vi har modtaget og gemt din bedømmelse, så den kan komme til gavn for andre brugere.', 'article', 'green')

          setTimeout =>
            $('.ui.dimmable .dimmer.success').dimmer('hide')
            # Reset.
            @set('supportTicket', null)
            @set('supportTicketValidated', false)
          , 2000

    declineClip: (clip) ->
      supportTicket = @store.createRecord 'submission',
        form: 'customer_panel.declined_clip'
        subject: "User declined clip (#{clip.get('id')}), from voucher (#{clip.get('voucher.id')})"
        name: "#{@get('app.currentUser.name')} (#{@get('app.currentUser.id')})"
        email: @get('app.currentUser.email')

      @set('supportActiveClip', clip)
      @set('supportTicket', supportTicket)


    cancelTicketOrReview: ->
      @set('supportTicket', null)
      @set('supportActiveClip', null)
      @set('review', null)

    submitTicket: ->
      @set('supportTicketValidated', true)

      # If valid save submission.
      @get('supportTicket').validate().then =>

        $('.undecided-clip .ui.dimmable .dimmer.processing').dimmer(
          closable: false
        ).dimmer('show')

        @set('supportActiveClip.status', 'declined')

        # Save ticked, then clip.
        @get('supportTicket').save().then =>
          @get('supportActiveClip').save().then =>
            $('.ui.dimmable .dimmer.processing').dimmer('hide')
            $('.ui.dimmable .dimmer.success').dimmer('show')

            setTimeout =>
              $('.ui.dimmable .dimmer.success').dimmer('hide')
              # Reset.
              @set('supportTicket', null)
              @set('supportTicketValidated', false)
            , 2000
