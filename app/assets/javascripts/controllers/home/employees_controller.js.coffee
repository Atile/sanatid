Sanatid.HomeEmployeesController = Ember.ArrayController.extend
  title: t('home.employees.title')

  needs: ['application']
  app: Ember.computed.alias('controllers.application')