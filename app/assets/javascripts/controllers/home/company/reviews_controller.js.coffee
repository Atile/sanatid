Sanatid.HomeCompanyReviewsController = Ember.ObjectController.extend
  title: 'Reviews'

  ratingStatus: ( ->
    score =  @get('averageRatingInPercent')
    status = switch
      when score > 45 && score < 64 then t('home.company_page.reviews.status.45to64')
      when score > 64 && score < 79 then t('home.company_page.reviews.status.64to79')
      when score > 80 && score < 99 then t('home.company_page.reviews.status.80to99')
      when score == 100 then t('home.company_page.reviews.status.100')
  ).property('averageRatingInPercent')
