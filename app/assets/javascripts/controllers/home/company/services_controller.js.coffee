Sanatid.HomeCompanyServicesController = Ember.ObjectController.extend

  selectedService: null
  bookingErrors: []

  # Booking fields.
  name: null
  email: null
  phone: null
  comment: null
  startsAt: null
  newsletter: false

  actions:
    startBooking: (service) ->
      @set('selectedService', service)

      $('.modal.order').showModal(shade: true)
      $('.date-field').datepicker
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,

    completeBooking: ->
      json = {
        booking: {
          name: @get('name'),
          phone: @get('phone'),
          email: @get('email'),
          comment: @get('comment'),
          newsletter: @get('newsletter'),
          starts_at: @get('startsAt'),
          menu_item_id: @get('selectedService.id'),
          company_id: @get('id')
        }
      }

      $.post('/api/bookings', json, (response) =>
        @set('success', true)
        $('.modal.order').hideModal()
        @set('selectedService', null)

      ).fail( (response) =>
        for k, v of response.responseJSON.errors
          @get('bookingErrors').pushObject(v)
      )

    cancelBooking: ->
      $('.modal.order').hideModal()
      @set('selectedService', null)
