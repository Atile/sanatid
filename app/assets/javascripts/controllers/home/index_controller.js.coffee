Sanatid.HomeIndexController = Ember.ObjectController.extend
  serviceFields: null
  promotions: null
  newsletterEmail: null
  companies: null

  flowServiceField: null
  flowAddress: null
  flowCity: null
  flowFormattedAddress: null
  flowCords: null

  searchCity: null

  actions:
    processAddress: ->
      lookup = =>
        intRegex = /^\d+$/
        if @get('flowAddress').length > 5 && @get('flowCity').length >= 4 && intRegex.test(@get('flowCity'))
          geocoder = new google.maps.Geocoder()
          geocoder.geocode {'address': "#{@get('flowAddress')} #{@get('flowCity')} Denmark" }, (results, status) =>
            data = results[0]
            if data?
              # Save the current address.
              @set('flowFormattedAddress', data.formatted_address)
              # Save cords.
              @set('flowCords', lat: data.geometry.location.lat(), lon: data.geometry.location.lng())

              @send('initFlow')
        else
          console.log 'show some error messages here'

      if google?
        lookup()
      else
        $.getScript "https://www.google.com/jsapi", =>
          google.load 'maps','3.6', other_params:'sensor=false', callback: -> lookup()

    processCity: ->
      lookup = =>
        if @get('searchCity').length > 3
          geocoder = new google.maps.Geocoder()
          geocoder.geocode {'address': "#{@get('searchCity')}, Denmark" }, (results, status) =>
            data = results[0]
            if data?
              # Save the current address.
              @set('flowFormattedAddress', data.formatted_address)
              # Save cords.
              @set('flowCords', lat: data.geometry.location.lat(), lon: data.geometry.location.lng())

              @send('initFlow')
        else
          console.log 'show some error messages here'

      if google?
        lookup()
      else
        $.getScript "https://www.google.com/jsapi", =>
          google.load 'maps','3.6', other_params:'sensor=false', callback: -> lookup()

    initFlow: ->
      @transitionToRoute('home.companies', queryParams: {serviceFieldId: @get('flowServiceField.id'), address: @get('flowFormattedAddress'), lat: @get('flowCords.lat'), lon: @get('flowCords.lon')})

    newsletterSubscribe: ->
      subscriber = @store.createRecord('newsletterSubscriber')
      subscriber.set('email', @get('newsletterEmail'))
      subscriber.set('active', true)

      subscriber.validate().then =>
        subscriber.save().then =>
          # Reset form.
          @set('newsletterEmail', '')

          # Feedback for user.
          $('.newsletter').popup(
            content: '<p>Subscribed</p>'
            transition: 'fade'
            on: 'focus'
          ).popup('show')

          setTimeout ->
            $('.newsletter').popup('hide')
          , 3000

      unless subscriber.get('isValid')
        errorMarkup = ""
        subscriber.get('errors.email').forEach  (error)->
          errorMarkup += "<p>#{error}</p>"

        $('.newsletter').popup(
          content: errorMarkup
          transition: 'fade'
          on: 'focus'
        ).popup('show')

        setTimeout ->
          $('.newsletter').popup('hide')
        , 2000
