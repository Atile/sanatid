Sanatid.HomeContactController = Ember.ObjectController.extend
  title: t('home.contact.title')

  needs: ['application']
  app: Ember.computed.alias('controllers.application')

  subjects: ['Spørgsmål', 'Forslag', 'Fejl rapportering', 'Andet']
  validated: false

  actions:
    changeSubject: (subject) ->
      @set('subject', subject)

    submitContact: ->
      # If user is logged in, pick up email manually.

      if @get('app.currentUser')
        @set('email', $('#contact_email').val())

      @set('form', 'home.contact')
      @set('subject', $('#contact-subject').val())
      @set('validated', true)

      # If valid save submission.
      @get('model').validate().then =>
        $('.ui.dimmable .dimmer.processing')
          .dimmer(
            closable: false
          )
          .dimmer('show')

        @get('model').save().then =>
          $('.ui.dimmable .dimmer.processing').dimmer('hide')
          $('.ui.dimmable .dimmer.success')
            .dimmer('show')
          @set('model', @store.createRecord('submission'))
          @set('validated', false)
