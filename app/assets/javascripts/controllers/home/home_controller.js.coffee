Sanatid.HomeController = Ember.ObjectController.extend
  needs: ['application']
  app: Ember.computed.alias('controllers.application')

  dashboardLink: ( ->
    if @get('authManager').softCheck('company')
      'company_panel.index'
    else
      'customer_panel.index'
  ).property('app.currentUser')

  actions:
    logout: ->
      @authManager.reset()
      @transitionToRoute('home')

