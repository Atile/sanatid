Sanatid.HomeCompaniesIndexController = Ember.ArrayController.extend
  title: 'Find praksisser'

  queryParams: ['lat', 'lon', 'address', 'serviceFieldId']

  serviceField: ( ->
    if @get('serviceFieldId')
      @store.find('serviceField', @get('serviceFieldId'))
  ).property('serviceFieldId')

