Sanatid.HomeCompanyController = Ember.ObjectController.extend Ember.GoogleAnalyticsTrackingMixin,
  needs: ['application']
  app: Ember.computed.alias('controllers.application')

  # public
  showContact: false

  absoluteHomepage: (->

    # make sure the website links is a external link
    if /^http/.test @get('homepage')
      return @get('homepage')

    "http://#{@get('homepage')}"

  ).property 'homepage'

  # Show only frontpage content, when not on a page.
  isOnFrontpage: ( ->
    false
    if @get('app.currentPath') == "home.company.index"
      true
  ).property('app.currentPath')

  latestReviews: ( ->
    num = 0
    latest = while (num += 1) > 4
      break unless @get('reviews').objectAt(num)
      @get('reviews').objectAt(num)
    latest
  ).property('reviews')

  ratingStatus: ( ->
    score =  @get('averageRatingInPercent')
    status = switch
      when score > 45 && score < 64 then t('home.company_page.reviews.status.45to64')
      when score > 64 && score < 79 then t('home.company_page.reviews.status.64to79')
      when score > 80 && score < 99 then t('home.company_page.reviews.status.80to99')
      when score == 100 then t('home.company_page.reviews.status.100')
  ).property('averageRatingInPercent')

  averageRatingInPercent: ( ->
    @get('averageRating') * 10
  ).property('averageRating')

  actions:
    viewFacilityImage: (image) ->
      $('.image.modal').showModal
        shade: true
        autoHide: true
        img: image.get('original')

    showContactInformation: ->
      @trackEvent('company', 'information')
      @set('showContact', true)

    # Called from within the start flow component
    initFlow: (params) ->
      @transitionToRoute('home.companies', queryParams: params)



Sanatid.HomeCompanyIndexController = Ember.ObjectController.extend
  needs: ['homeCompany']
  parent: Ember.computed.alias('controllers.homeCompany')

  title: (->
    @get('parent.companyName')
  ).property('parent.companyName')
