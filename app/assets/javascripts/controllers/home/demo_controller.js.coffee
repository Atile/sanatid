Sanatid.HomeDemoController = Ember.ObjectController.extend
  title: t('home.demo.title')

  # Public.
  email: null
  name: null
  companyName: null
  phone: null

  actions:
    start: ->
      $('.demo-form').dimmer('show', {
        closable: false
      })

      $.ajax
        type: 'POST'
        url: "#{location.origin}/api/demo/init.json"
        data:
          name: @get('name')
          company_name: @get('companyName')
          email: @get('email')
          phone: @get('phone')
        success: (response) ->
          window.location = "http://demo.sanatid.dk/login?password=#{response.password}&email=#{response.email}&demo=true"
