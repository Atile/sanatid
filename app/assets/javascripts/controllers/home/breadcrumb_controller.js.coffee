Sanatid.HomeBreadcrumbController = Ember.ObjectController.extend
  needs: ['application']
  app: Ember.computed.alias('controllers.application')

  active: null

  paths: ( ->
    @get('app.currentController.paths')
  ).property('app.currentController.paths')

  active: ( ->
    @get('app.currentController.title')
  ).property('app.currentController.title')
