Sanatid.HomeLoginController = Ember.ObjectController.extend
  title: t("home.login.loginButton")

  email: null
  password: null
  unsuccessfulLogin: false
  # Used if user were trying to access a page, which require login, so we can redirect user back, after login.
  attemptedTransition: null

  actions:
    login: ->
      $('.dimmable').showDimmer()

      $.post('/api/session.json', session: @getProperties('email', 'password'), (result) =>
        @authManager.authenticate result.api_key.access_token, result.api_key.user_id, result.user_type, (status) =>
          if status == 'success'
            if @get('attemptedTransition')
              @get('attemptedTransition').retry()
              @set('attemptedTransition', null)
            else
              # Reset.
              @set('email', null)
              @set('unsuccessfulLogin', null)
              @transitionToRoute('/panel/' +  result.user_type)
      ).fail( =>
        @set('unsuccessfulLogin', true)
      ).always =>
        $('.dimmable').hideDimmer()
        # Reset.
        @set('password', null)
