Sanatid.HomeGuidesShowController = Ember.ObjectController.extend
  paths: ( ->
    return [{path: t('home.guides.title'), linkRoute: 'home.guides.index' }, {path: @get('parent.title'), linkRoute: 'home.guides.show', linkParams: @get('parent.slug')}] if @get('parent')
    [{path: t('home.guides.title'), linkRoute: 'home.guides.index' }]
  ).property('model')
