Sanatid.HomeUnsubscribeController = Ember.ObjectController.extend
  title: t('home.unsubscribe.title')

  # Public.
  email: null

  actions:
    unsubscribe: ->
      $.ajax
        url: '/api/newsletter_subscribers/1'
        type: 'DELETE'
        data: { newsletter_subscriber: { email: @get('email') } }
        success: (result) =>
          $('.message').show().transition('bounce')
          @set('email', null)


