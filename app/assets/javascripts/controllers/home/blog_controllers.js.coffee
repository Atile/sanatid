Sanatid.HomeBlogIndexController = Ember.ArrayController.extend
  title: t('home.blog.title')

  dateSorted: (->
    month = new Date().getMonth()
    # Create array for containing of blogs.
    blogsByMonth = []

    for num in [0..9]
      # If going back from january(-1), reset to december.
      if month == -1
        month = 12

      blogsByMonth.pushObject
        month: moment().subtract('month', num).format('MMMM YYYY')
        blogs: Ember.A([])

      # Traverse each blog, and see if they match the current month, if so add them.
      @get('model').forEach (blog) ->
        if moment(blog.get('createdAt')).month() == month
          blogsByMonth[num].blogs.push(blog)

      # Go back a month.
      month--

    blogsByMonth
  ).property('model.@each')

Sanatid.HomeBlogShowController = Ember.ObjectController.extend
  # Title is implicit, due to model property title.
  paths: ( ->
    [{path: t('home.blog.title'), linkRoute: 'home.blog.index'}]
  ).property('t')

  needs: 'HomeBlogIndex'
  blogs: Ember.computed.alias('controllers.HomeBlogIndex')

  init: ->
    @set 'blogs.model', @store.find('blog')

  refreshCommentsOnUrlChange: ( ->
    comments_html = "<fb:comments href='#{window.location}' numposts='10' colorscheme='light' width='705'></fb:comments>"
    $('#fb-comments').html(comments_html)

    try
      FB.XFBML.parse($('#fb-comments').get(0));
    catch e
  ).observes('Sanatid.currentPathname')