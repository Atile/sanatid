Sanatid.HomeGuidesController = Ember.ArrayController.extend
  ordered: ( ->
    pages = []

    # First find all parents.
    @get('model').forEach (page) ->
      unless page.get('parent')
        # Add children empty array
        page.set('children', [])
        pages.pushObject(page)

    # Then fill in children.
    @get('model').forEach (page) ->
      if page.get('parent')
        page.get('parent.children').pushObject(page)

    pages
  ).property('model')
