Sanatid.HomeProductsController = Ember.ObjectController.extend
  title: t('home.products.title')

  created: false

  actions:
    signup: ->
      $('.dimmable').showDimmer()

      @get('model').save().then =>
        $('.dimmable').showDimmerSuccess()

        # Reset form.
        @set('model', @store.createRecord('company'))

        # show success message to user.
        @set('created', true)

        setTimeout ->
          $('.dimmable').hideDimmer()
        , 2000
      , ->
        # ..

        $('.dimmable').hideDimmer()
