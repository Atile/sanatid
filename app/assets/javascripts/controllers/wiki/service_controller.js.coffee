Sanatid.HomeWikiServiceController = Ember.ObjectController.extend
  title: (->
    @get('name')
  ).property('name')

  paths: (->
    [{path: @get('serviceField.name'), linkRoute: 'home.wiki.service_field', linkParams: @get('serviceField.slug') }]
  ).property('name')

  serviceFields: null
