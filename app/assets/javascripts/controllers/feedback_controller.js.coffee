Sanatid.FeedbackController = Ember.ObjectController.extend
  needs: ['application']
  app: Ember.computed.alias('controllers.application')

  subjects: ['Spørgsmål', 'Forslag', 'Fejl rapportering', 'Andet']
  validated: false
  # Lets FeedbackView know, when to show what.
  submitted: false

  actions:
    changeSubject: (subject) ->
      @set('subject', subject)

    postSubmission: ->
      # If user is logged in, pick up email manually.
      if @get('app.currentUser')
        @set('email', $('#contact_email').val())

      if @get('app.currentUser.name')
        @set('name', $('#contact_name').val())

      @set('form', 'home.contact')
      @set('subject', $('#contact-subject').val())
      @set('validated', true)

      # If valid save submission.
      @get('model').validate().then =>
        $('.ui.dimmable .dimmer.processing')
          .dimmer(
            closable: false
          )
          .dimmer('show')

        @get('model').save().then =>
          $('.ui.dimmable .dimmer.processing').dimmer('hide')
          $('.ui.dimmable .dimmer.success').dimmer('show')
          setTimeout ->
            $('.ui.dimmable .dimmer.success').dimmer('hide')
          , 1500

          @set('model', @store.createRecord('submission'))
          @set('validated', false)
          @set('submitted', true)
