Sanatid.StartFlowComponent = Ember.Component.extend
  needs: ['application']
  app: Ember.computed.alias('controllers.application')

  # Public.
  hideServiceField: false
  serviceField:     false
  address:          null
  city:             null
  formattedAddress: null
  cords:            null

  # Private.
  layoutName:'components/start_flow'

  actions:
    processAddress: ->
      lookup = =>
        intRegex = /^\d+$/
        if @get('address').length > 5 && @get('city').length >= 4 && intRegex.test(@get('city'))
          geocoder = new google.maps.Geocoder()
          geocoder.geocode {'address': "#{@get('address')} #{@get('city')} Denmark" }, (results, status) =>
            data = results[0]
            if data?
              # Save the current address.
              @set('formattedAddress', data.formatted_address)
              # Save cords.
              @set('cords', lat: data.geometry.location.lat(), lon: data.geometry.location.lng())

              @sendAction('globalInitFlow', {serviceFieldId: @get('serviceField.id'), address: @get('formattedAddress'), lat: @get('cords.lat'), lon: @get('cords.lon')})
        else
          console.log 'show some error messages here'

      if google?
        lookup()
      else
        $.getScript "https://www.google.com/jsapi", =>
          google.load 'maps','3.6', other_params:'sensor=false', callback: -> lookup()
