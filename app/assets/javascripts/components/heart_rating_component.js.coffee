Sanatid.HeartRatingComponent = Ember.Component.extend
  # Public.
  averageRating: null
  reviewCount: null
  hideStatus: false
  large: false

  # Private.
  layoutName:'components/heart_rating'

  ratingStatus: ( ->
    score =  @get('averageRating') * 10
    status = switch
      when score > 45 && score < 64 then t('home.company_page.reviews.status.45to64')
      when score > 64 && score < 79 then t('home.company_page.reviews.status.64to79')
      when score > 80 && score < 99 then t('home.company_page.reviews.status.80to99')
      when score == 100 then t('home.company_page.reviews.status.100')
  ).property('averageRating')
