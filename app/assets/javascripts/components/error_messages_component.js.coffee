Sanatid.ErrorMessagesComponent = Em.Component.extend
  errors: Em.computed.filter 'for.errors.content', (error) ->
    error.attribute != 'base'

  baseErrors: Em.computed.filter 'for.errors.content', (error) ->
    error.attribute is 'base'