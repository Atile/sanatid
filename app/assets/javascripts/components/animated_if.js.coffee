Sanatid.AnimatedIfComponent = Ember.Component.extend
  # Public
  condition: null

  updateVisibilityState: ( ->
    if @get('condition')
      @$().fadeIn()
    else
      @$().fadeOut()
  ).on('didInsertElement').observes('condition')