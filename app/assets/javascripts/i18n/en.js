window.translations = {
  "home" : {
    "user": {
      "title": "Login",
      "email": "Email",
      "password": "Password",
      "errorMessage": "Your data did not match, please try again.",
      "successMessage": "Welcome to Sanatid, {{name}}"
    }
  },
  "job": {
    "new": {
      nextButton: "Next",
      "step-1": {
        "title": "Available service fields",
        "selectServiceFieldTitle": "Select a service field",
        "selectServiceFieldButton": "Select",
        "descriptionTitle": "Describe your problem",
        "descriptionImportant": "Your description is important to secure high quality, and a correct treatment",
        "selectServiceTitle": "Does you description relate to any of these categories?",
        "selectServiceValue": "Select category"
      },
      "step-2": {
        "zipTitle": "Enter your postal code, so we can find service provides near you.",
        "timeAvailable": 'When would you like to start?',
        "availableNext": "Soon as possible",
        "availableFrom": "Soon as possible, after this date",
        "availableFromDescription": "We will try to get a offer, closet to this date"
      },
      "step-3": {
        "registerTitle": "Finally, you just need to register, so that you can view the progress of your job",
        "email": "Email:",
        "name": "Name:",
        "phone": "Phone:",
        "password": "Password:",
        "passwordConfirmation": "Password once more:",
        "signup": "Sign up",
        "alreadyRegistered": "Are you already registered? Login here",
        "login": "Login",
        "registerConfirmation": "You are now registered, we have sent you a confirmation on the provided email.",
        "loginConfirmation": "You are now logged in.",
        "createJobButton": "Create job",
        "createdJobStatus": "Created job"
      }
    }
  }
};