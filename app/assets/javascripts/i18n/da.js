window.translations = {
  "global": {
    "loading": "Henter...",
    "billingPeriod": {
      "yearly": "år.",
      "monthly": "md.",
      "full": {
        "yearly": "årligt",
        "monthly": "månedligt"
      }
    },
    "message": {
      "contentPresence": "Du mangler at udfylde beskeden",
      "contentTooShort": "Din besked er for kort"
    },
    "number": {
      "currencyAbbrevation": "Kr.",
      "separator": ",",
      "delimiter": ".",
      "price": ",-",
      "excludingTax" : "ekskl. moms"
    }
  },
  "demo" : {
    "return": "Du kan til hver en tid klikke her, for at afslutte din demo.",
    "welcome": "Velkommen til demo praksis panelet."
  },
  "home" : {
    "meta": {
      "title" : "Sanatid.dk | Find din coach, massør og terapeut nemt og hurtigt",
      "description" : ' Sanatid.dk hjælper dig med hurtigt og overskueligt, at finde en praksis inden for coaching, massage og terapi. Skriv din adresse og søg blandt mere end 8000 praksisser i Danmark.'
    },
    "login" : {
      "loginButton" : "Log på",
      "loggingIn" : "Vi logger dig på..",
      "loginError" : "Øv, dine oplysninger matchede ikke i vores system, prøv igen."
    },
    "newsletter" : {
      "email" : "E-mail",
      "subscribe" : "Tilmeld nyhedsbrev",
      "errors": {
        "emailPresence": 'Du mangler at udfylde din email',
        "emailFormat": 'Din email kan ikke godkendes'
      }
    },
    "header" : {
      "home" : "Forside",
      "blog" : "Blog",
      "contact" : "Kontakt os",
      "how" : "Hvordan virker det?"
    },
    "how" : {
      "title" : "Hvordan virker det?"
    },
    "frontpagePromotion": {
      "showProfile": "Vis profil"
    },
    "blog" : {
      "by" : "Skrevet af",
      "readMore" : "Læs mere",
      "title" : "Sanatid blogger"
    },
    "aboutBox" : {
      "title" : "Webudviklere med interesse for sundhed.",
      "byline" : "Sanatid.dk er lavet af David Hammer og Martin Elvar.",
      "description" : "Vi tror på, at konceptet om gratis tilbud bliver den nye og nemmere måde for kunder, at få hvad de gerne vil have. Det er vores overbevisning, at mennesker gerne vil bruge tid på at tjene penge, hvorfor i alverden skulle de så også bruge tid på at bruge dem?"
    },
    "contact" : {
      "title" : "Kontakt os",
      "name" : "Hvad er dit navn?",
      "email" : "Hvilken E-mail kan du kontaktes på?",
      "subject" : "Emne",
      "message" : "Skriv din besked her.",
      "send" : "Send besked",
      "errors": {
        "namePresence": "Du mangler at vælge et emne",
        "subjectPresence": "Du mangler at udfylde dit navn",
        "emailPresence": 'Du mangler at udfylde din email',
        "emailFormat": 'Din email kan ikke godkendes',
        "messagePresence": "Du mangler at udfylde din besked",
        "messageTooShort": 'Din besked er for kort'
      }
    },
    "user": {
      "title": "Login",
      "email": "E-mail",
      "password": "Adgangskode",
      "errorMessage": "Øv, dine oplysninger matchede ikke i vores system, prøv igen.",
      "successMessage": "Velkommen til Sanatid, {{name}}"
    },
    "footer" : {
      "home" : "Forside",
      "employees" : "Medarbejdere",
      "coaching" : "Coaching",
      "massage" : "Massage",
      "therapy" : "Terapi",
      "howto" : "Hvordan virker det?",
      "blog" : "Læs vores blog",
      "address" : "Langdraget 27, st.tv",
      "city" : "2720 Vanløse",
      "country" : "Danmark",
      "phone" : "(+45) 53 61 73 31",
      "email" : "kontakt@sanatid.dk"
    },
    "employees" : {
      "title" : "Medarbejdere"
    },
    "guides" : {
      "title": "Råd og vejledning"
    },
    "job" : {
      "new" : {
        "nextButton" : "Gå til næste trin",
        "title" : "Få gratis 3 tilbud",
        "step-1" : {
          "title" : "Hvad har du brug for?",
          "serviceFieldDescription" : "Er du i tvivl om hvad du skal vælge? Du kan læse om de forskellige sundhedskategorier på vores wiki",
          "here" : "her",
          "selectServiceFieldTitle" : "Vælg kategori",
          "selectServiceFieldButton" : "Vælg",
          "descriptionTitle" : "Beskriv den hjælp du ønsker",
          "descriptionImportant" : "Det er vigtigt, at din beskrivelse er så grundig som muligt, så du kan få den bedste behandling.",
          "selectServiceTitle" : "Passer din beskrivelse på en af disse kategorier?",
          "selectServiceTitleImportant" : "Du kan trykke på flere af kategorierne, skulle du være i tvivl om hvilke du ønsker hjælp til.",
          "selectServiceValue" : "Vælg kategori",
          "errors" : {
            "descriptionPresence" : "Du manger at udfylde en beskrivelse",
            "descriptionTooShort" : "Din beskrivelse er for kort"
          }
        },
        "step-2" : {
          "title" : "Hvor og hvornår?",
          "zipTitle" : "Hvor bor du?",
          "zipTitleImportant" : "Vi bruger din adresse, så vi kan finde praksisser nærmest dig.",
          "zipCode" : "Dit postnr.",
          "zipCodeExample" : "F.eks. 2720",
          "address" : "Vejnavn og nummer",
          "addressExample" : "F.eks. Langdraget 27",
          "timeAvailable" : "Hvornår har du tid?",
          "mapFoundAddress" : "Vi fandt din adresse",
          "mapAddressNotFound" : "Vi kunne desværre ikke finde din adresse, prøv venligst igen",
          "mapNotice" : "Vi finder praksisser ud fra denne lokation",
          "timeAvailableDescription" : "Du kan få tid med det samme, eller vente til efter en bestemt dato.",
          "availableNext" : "Så hurtigt som muligt",
          "availableFrom" : "Snarest efter denne dato",
          "availableFromExample" : "F.eks. 03-02-2014",
          "availableFromDescription" : "?",
          "errors" : {
            "addressPresence" : "Du mangler at udfylder din adresse",
            "availablePresence" : "Du mangler at udfylde hvornår du har tid"
          }
        },
        "step-3" : {
          "title" : "Opsummering",
          "summaryTitle" : "Opsummering af indtastninger",
          "serviceField" : "Hovedkategori",
          "service" : "Underkategori",
          "description" : "Beskrivelse",
          "availableFrom" : "Snarest efter d.",
          "availableNext" : "Så hurtigt som muligt.",
          "registerTitle" : "Opret en bruger",
          "registerTitleImportant" : "Har du ikke allerede en oprettet bruger? Opret da en her.",
          "name" : "Dit fulde navn:",
          "email" : "Din E-mail:",
          "phone" : "Dit Telefon nummer:",
          "password" : "Din adgangskode:",
          "passwordConfirmation" : "Bekræft din adgangskode",
          "newsletter" : "Tilmeld nyhedsbrev, og få nyheder og tilbud i din indboks",
          "signupButton" : "Opret bruger",
          "loginTitle" : "Log ind",
          "loginTitleImportant" : "Har du allerede en bruger? Log da ind her.",
          "loginButton" : "Log ind",
          "registerConfirmation" : "Du er nu oprettet som bruger. Tryk på knappen for, at sende jobbet i udbud.",
          "loginConfirmation" : "Du er nu logget ind. Tryk på knappen for, at sende jobbet i udbud.",
          "createJobButton" : "Opret udbud og modtag gratis 3 tilbud",
          "createdJobConfirmation" : "Dit udbud er blevet oprettet.",
          "registerErrors" : {
            "namePresence" : "Du mangler at udfylde dit navn",
            "emailPresence" : "Du mangler at udfylde din email",
            "emailFormat" : "Din email kan ikke godkendes, er du sikker på du har tastet rigtigt?",
            "emailInUse" : "Din email er allerede i brug, benyt venligst en anden",
            "phonePresence" : "Du mangler at udfylde dit telefon nummer",
            "phoneOnlyInteger" : "Telefon nummer kan kun bestå af tal",
            "passwordConfirmationPresence" : "Du mangler at udfylde din adgangskode",
            "passwordsMatchAcceptance" : "De 2 indtastet adgangskoder er ikke ens"
          }
        }
      }
    },
    "products": {
      "title": "Sanatid tilbyder"
    },
    "demo": {
      "title": "Prøv en demo",
      "bootingDemo": "Opretter din demo, dette kan tage nogle få minutter.",
      "email": "Email",
      "name": "Navn",
      "companyName": "Firma navn",
      "phone": "Telefon",
      "startDemo": "Start demo"
    },
    "unsubscribe" : {
      "title": "Afmeld nyhedsbrev",
      "header": "Ønsker du at afmelde dig vores nyhedsbrev, så udfyld formen herunder.",
      "success" : "Tak for denne gang, du er nu afmeldt.",
      "email": "Email",
      "submit": "Afmeld"
    },
    "company_page": {
      "information": "Information",
      "employees": "Medarbejdere",
      "paymentMethods": "Betalingsformer",
      "zip": "Postnummer",
      "services": "Ydelser",
      "openingHours": "Åbningstider",
      "reviews": "Bedømmelser",
      "frontpage": "Forside",
      "getOfferButton" : "Få tilbud fra {{name}}",
      "reviews" : {
        "status" : {
          "45to64" : "Udemærket",
          "64to79" : "Henrivende",
          "80to99" : "Fantastisk",
          "100" : "Perfekt"
        },
        "reviews" : "Anmeldelser",
        "noReviews" : "{{companyName}} har desværre ikke modtaget nogle anmeldelser endnu."
      }
    }
  },
  "company_panel": {
    "subscriptionErrors": {
      "expiredTitle": "Dit medlemskab er udløbet",
      "interruptedTitle": "Dit medlemskab er blevet afbrudt",
      "explanation": "Derfor kan du ikke byde på denne opgave.",
      "description": "Ønsker du at tegne et nyt medlemskab, kan du besøge denne side, eller blive ringet op af en sælger."
    },
    "dashboard": {
      "title": "Oversigt",
      "reservedJobsListingTitle": "Opgaver fra kunder, som har valgt dig",
      "jobListingTitle": "Opgaver udbudt nær dig",
      "id": "ID",
      "type": "Opgave",
      "categories": "Kategorier",
      "offers": "Bud",
      "availability": "Kunden har tid",
      "availableNow" : "Snarest muligt",
      "availableAfter" : "Efter d.",
      "reservedTo": "Reserveret til d.",
      "waitingForAnswer": "Afventer kunde svar",
      "details": "Detaljer",
      "showJobButton": "Se og byd",
      "statisticsTitle": "Dine statistikker"
    },
    "jobs": {
      "title": "Opgaver",
      "choose": "Vælg",
      "openJobs" : "Åbne opgaver",
      "jobsPlacedBids": "Opgaver du har budt på",
      "wonJobs": "Vundne opgaver",
      "lostJobs": "Tabte opgaver",
      "id": "ID",
      "type": "Opgavetype",
      "categories": "Kategorier",
      "offers": "Antal tilbud",
      "availability": "Kunden har tid",
      "availableNow": "Snarest muligt",
      "availableAfter": "Efter d.",
      "questions": "Spørgsmål",
      "answered": "Besvaret",
      "unanswered": "Ubesvaret",
      "details": "Detaljer",
      "showAndBidButton": "Vis og byd",
      "showOnlyButton": "Vis",
      "compareButton": "Sammenlign bud",
      "show": {
        title: "Vis opgave",
        "jobDetailsTitle": "Opgavedetaljer",
        "type": "Type",
        "categories": "Kategorier",
        "description": "Beskrivelse",
        "questionInfo": "Du kan stille kunden et spørgsmål, hvis du har brug for mere information inden du kan give et tilbud.",
        "questionTitle": "Du kan stille ét spørgsmål",
        "questionSuccessMessage": "Vi har modtaget dit spørgsmål, vi sender dig en mail så snart kunden har svaret. Du har derefter 3 timer til at svare, inden vi sender udbudet videre.",
        "question_errors": {
          "question_presence": "Du mangler at skrive dit spørgsmål",
          "question_length": "Dit spørgsmål er for kort"
        },
        "bidInfo": "Vær opmærksom på, at du kun kan stille ét spørgsmål, og at du automatisk reserverer et bud indtil kunden har svaret.",
        "askQuestionButton": "Stil spørgsmål",
        "correspondanceTitle": "Korrespondance",
        "question": "Spørgsmål",
        "answer": "Svar",
        "offerSessionButton": "Tilbyd Enkel session",
        "offerVoucherButton": "Tilbyd klippekort",
        "offerDescriptionTitle": "Beskriv dit tilbud",
        "sessionPrice": "Sessionens pris",
        "voucherAmount": "Antal klip",
        "voucherPrice": "Klippekortets pris",
        "makeOfferButton": "Send tilbud",
        "offersTitle": "Dine bud",
        "winningOfferTitle": "Vundet bud!",
        "distanceTitle": "Distance mellem kunde og praksis",
        "reviewsTitle": "Anmeldelser",
        "singleOffer": "Enkelt session",
        "voucherOffer": "Klippekort tilbud",
        "clips": "Klip",
        "offerSuccessMessage": "Vi har sendt dit bud til kunden. Husk at du både kan sende et session bud, samt et klippekorts bud, det tæller kun som et enkelt bud!",
        "offer_errors": {
          "description_presence": "Du mangler at udfylde din beskrivelse",
          "price_presence": "Du mangler at udfylde din pris",
          "price_format": "Din pris skal bestå af enten heltal, eller et decimal. f.eks \"95\" eller \"99,95\"",
          "clip_format": "Antal klip skal være mellem 2 og 80"
        }
      }
    },
    "customers": {
      "title": "Kundekartotek",
      "searchTitle": "Søg i dine kunder",
      "searchDescription": "Du kan søge på enten ordrenummer, kundenummer eller kundens navn.",
      "resetSearchButton": "Nulstil søgning",
      "orderId": "Ordrenummer",
      "customerNo": "Kundenummer",
      "customerName": "Kundens navn",
      "categories": "Kategorier",
      "jobType": "Opgavetype",
      "session": "Enkelt session",
      "voucher": "Klippekort",
      "salesDate": "Salgsdato",
      "details": "Detaljer",
      "showCustomerButton": "Vis kunde",
      "show": {
        "title": "Visning af kundens opgave",
        "jobDetailsTitle": "Kundens opgave",
        "information": "Kunden",
        "description": "Beskrivelse",
        "category": "Hovedkategori",
        "subcategories": "Underkategorier",
        "correspondanceTitle": "Korrespondance",
        "question": "Dit spørgsmål",
        "answer": "Kundens svar",
        "chatTitle": "Chat",
        "writeButton": "Skriv til kunden",
        "sendMessageButton": "Send besked",
        "wonTitle": "Du vandt",
        "offerDescription": "Budbeskrivelse",
        "price": "Pris",
        "offerType": "Budtype",
        "voucherInfo": "klip. Klip tilbage",
        "sessionInfo": "En enkelt session",
        "clipDateTitle": "Dato for klip",
        "confirmedTitle": "Bekræftet af kunden",
        "executionDate": "Udførelsesdato",
        "date": "Dato",
        "done": "Udført",
        "no": "Nej",
        "yes": "Ja",
        "clipVoucherButton": "Tag et klip",
        "sessionDoneButton": "Meld sessionen er udført",
        "confirmClipTitle": "Bekræft klip",
        "confirmClipDescription": "Bekræft venligst klip, dette kan ikke fortrydes.",
        "confirmDoneDescription": "Bekræft venligst handling, dette kan ikke fortrydes.",
        "regretButton": "Fortryd",
        "confirmButton": "Bekræft"
      }
    },
    "subscriptions": {
      "title": "Abonnementer",
      "activeSubscriptionsTitle": "Aktive abonnementer",
      "formerSubscriptionsTitle": "Tidligere abonnementer",
      "type": "Produkttype",
      "expirationDate": "Udløbsdato",
      "lastRenewed": "Sidst fornyet",
      "renewDate": "Fornyelsesdato",
      "paymentPeriod": "Betalingsperiode",
      "price": "Pris",
      "invoice": "Faktura",
      "noExpirationDate": "Ingen udløbsdato",
      "notExtended": "Fornyes ikke",
      "singlePayment": "Engangsbetaling eller opsagt",
      "showInvoiceButton": "Vis"
    },
    "settings": {
      "title": "Indstillinger",
      "account": {
        "title" : "Konto",
        "description": "Her kan du ændre i informationer relateret til kontoen.",
        "name": "Brugernavn",
        "email": "E-mail",
        "phone": "Tlf. nummer",
        "newsletter": "Nyhedsbrev",
        "newsletterDescription": "Modtag tilbud og nyheder via. vores nyhedsbrev.",
        "password": "Kodeord",
        "passwordAgain": "Bekræft kodeord",
        "saveButton": "Gem kontoinstillinger"
      },
      "practice" : {
        "title" : "Praksisinstillinger",
        "description": "Her kan du ændre i din virksomheds informationer.",
        "name": "Virksomhedens navn",
        "number": "CVR-nummer",
        "email": "E-mail",
        "emailDescription": "Ændre denne mail hvis du ønsker, at modtage mails fra Sanatid.dk på en anden e-mail konto.",
        "address": "Adresse",
        "phone": "Tlf. nummer",
        "paymentMethods": "Betalingsformer",
        "paymentMethodsDescription": "Betalingsformer du modtager. F.eks. \"Kontant, Dankort, Bankoverførsel\"",
        "openingHoursTitle": "Åbningstider",
        "hours": "Tidsrum",
        "example" : "Eks. \"8:00-16:00\" eller \"Lukket\"",
        "monday": "Mandag",
        "tuesday": "Tirsdag",
        "wednesday": "Onsdag",
        "thursday": "Torsdag",
        "friday": "Fredag",
        "saturday": "Lørdag",
        "sunday": "Søndag",
        "servicesTitle": "Kategorier du tilbyder",
        "servicesDescription": "Du kan her vælge hvilke kategorier du tilbyder kunder. Vær opmærksom på, at du kun kan byde på opgaver relateret til dine valgte kategorier.",
        "saveButton": "Gem Profilinstillinger"
      },
      "profile" : {
        "title" : "Profilinstillinger",
        "description": "Her kan du ændre i din praksis helt egen side.",
        "coverImageTitle": "Upload coverbillede",
        "coverImageUploadButton": "Upload coverbillede",
        "logoImageTitle": "Upload logo",
        "logoImageUploadButton": "Upload logo",
        "frontpage": "Forside",
        "newPageTitle": "Ny underside",
        "newPageButton": "Opret ny underside",
        "pageDescription": "Tekst på forside",
        "pageTitle": "Sidens titel",
        "pageContent": "Sidens indhold",
        "saveFrontpageButton" : "Gem Forside",
        "savePageButton": "Gem side",
        "deletePageButton": "Slet side"
      }
    }
  },
  "customer_panel": {
    "undecided_clip_modal" : {
      "review" : {
        "review": "Din anmeldelse",
        "errors" : {
          "contentTooShort" : "Din anmeldelse at desværre for kort (min 40 tegn)",
          "ratingPresence" : "Du mangler at angive antal hjerter"
        }
      }
    },
    "dashboard": {
      "title": "Oversigt",
      "openJobsTitle": "Dine åbne opgaver",
      "noOpenJobsTitle": "Du har ingen åbne opgaver",
      "closedJobsTitle": "Dine lukkede opgaver",
      "jobDescriptionTitle": "Opgavens beskrivelse",
      "jobOfferTitle": "Du har fået",
      "jobOffer": "Bud",
      "showJobButton": "Vis opgaven",
      "newJobButton": "Modtag gratis 3 tilbud"
    },
    "jobs": {
      "title": "Opgaver",
      "activeJobsTitle": "Aktive opgaver",
      "type": "Opgave",
      "categories": "Kategorier",
      "offers": "Bud",
      "availability": "Du har tid",
      "availableNow": "Snarest muligt",
      "availableAfter": "Efter d.",
      "questions": "Spørgsmål",
      "noQuestions": "Ingen spørgsmål",
      "receivedQuestions": "Du har modtaget nye spørgsmål",
      "questionsAnswered": "Du har svaret på alle nuværende spørgsmål",
      "details": "Detaljer",
      "showJobButton": "Vis opgave",
      "deleteJobButton": "Slet opgave",
      "earlierJobsTitle": "Tidligere opgaver",
      "sureInfo": "Er du sikker?",
      "sureDeleteInfo": "Er du sikker på du vil slette denne opgave? (Dette kan ikke fortrydes)",
      "keepJobButton": "Nej, behold opgaven",
      "deleteJobButton": "Slet",
      "show": {
        "title": "Din opgave",
        "detailsTitle": "Opgavens detaljer",
        "description": "Beskrivelse",
        "available": "Kunden har tid",
        "type": "Opgavetype",
        "categories": "Kategorier",
        "offerTitle": "Det bud du har accepteret",
        "singleOffer": "Enkel session",
        "amountDescription": "Et klippekort på",
        "clips": "klip",
        "price": "Pris",
        "offerFrom": "Tilbud fra følgende praksis",
        "offers": "Tilbud",
        "offersDescription": "Du har modtaget",
        "sessionOffer": "En enkel session til prisen",
        "voucherOffer": "til prisen",
        "questions": "Spørgsmål",
        "questionsDescription": "Vær opmærksom på, at du skal svare på de spørgsmål der måtte være, før du kan modtage yderligere tilbud.",
        "didNotReceiveQuestions": "Du har ikke modtaget nogle spørgsmål.",
        "didReceiveQuestions": "Du har modtaget følgende spørgsmål",
        "answerButton": "Besvar spørgsmål",
        "answer": "Dig",
        "answer_errors": {
          "answer_presence": "Du mangler at skrive dit svar",
          "answer_length": "Dit svar er for kort"
        }
      },
      "inactive": {

      }
    },
    "accepted_jobs": {
      "title": "Accepterede opgaver",
      "jobId": "Opgave ID",
      "companyName": "Praksis",
      "clipsUsed": "Klip brugt",
      "issuedDate": "Dato for udstedelse",
      "details": "Detaljer",
      "showJobButton": "Vis opgave",
      "show": {
        "typeVoucher": "Klippekort med",
        "clip": "klip på",
        "remainingClipsBefore": "Du har",
        "remainingClipsAfter": "klip tilbage",
        "lastClipsTaken": "Tidligere klip du har brugt"
      }
    },
    "settings": {
      "title" : "Indstillinger",
      "secondaryTitle": "Dine kontoinstillinger",
      "description": "Her kan du ændre i dine personlige informationer.",
      "name": "Brugernavn",
      "email": "E-mail",
      "phone": "Tlf. nummer",
      "password": "Kodeord",
      "passwordAgain": "Bekræft kodeord",
      "saveButton": "Gem instillinger"
    }
  }
};