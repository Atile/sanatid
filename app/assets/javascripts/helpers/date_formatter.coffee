Ember.Handlebars.helper 'dateFormat', (value, format) ->
  if format is 'long'
    return moment(value).format('LLLL');

  if format is 'medium'
    return moment(value).format('LL');

  if format is 'short'
    return moment(value).format('l');

  if format is 'calendar'
    return moment(value).calendar()

  # Default.
  moment(value).format('LL');

