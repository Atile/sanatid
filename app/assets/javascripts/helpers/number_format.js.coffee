Ember.Handlebars.helper 'numberFormat', (value, price, tax) ->
  value = String(value)
  # Replace delimiter.
  value = value.replace /\,+/, t('global.number.delimiter')

  # Replace seperator
  value = value.replace /(\.)(\d\d$)/, "#{t('global.number.separator')}$2"

  value = "#{t('global.number.currencyAbbrevation')} #{value} #{t('global.number.price')}" if price
  value = "#{value} #{t('global.number.excludingTax')}" if tax

  value


