# In case user is landing on the environment for demo purposes, we save the credentials, so we can autologin the user.

$.QueryString = ( (a)->
  return {} if a[0] == ""

  b = {};
  for i in [0..(a.length-1)]
    p=a[i].split('=');
    continue if p.length != 2
    b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "))
  b
  )(window.location.search.substr(1).split('&'))

if $.QueryString.demo
  localStorage.setItem('email', $.QueryString.email)
  localStorage.setItem('password', $.QueryString.password)