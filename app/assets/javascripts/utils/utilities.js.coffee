$(document).ready ->
  Sanatid.utils = {}

  Sanatid.utils.setMessage = (header, content, selector, classes, appendTo) ->
    message = $("<div><i class='close icon'></i><div class='header'>#{header}</div><p>#{content}</p></i></div>")
      .addClass("ui message #{classes}")

    if appendTo
      message.appendTo(selector)
    else
      message.prependTo(selector)

    $(message).click ->
      $(message).fadeOut ->
        $(message).remove()
