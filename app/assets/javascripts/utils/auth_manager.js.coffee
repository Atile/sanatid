Sanatid.AuthManager = Ember.Object.extend
  init: ->
    @_super()

  isAuthenticated: ->
    (!Ember.isEmpty(@get('apiKey.accessToken')) && !Ember.isEmpty(@get('apiKey.user')))

  softCheck: (userType)->
    (!Ember.isEmpty(localStorage.getItem('accessToken')) && !Ember.isEmpty(localStorage.getItem('user')) && localStorage.getItem('userType') == userType )

  initAuthentication: ->
    return new Ember.RSVP.Promise (resolve, reject) =>
      accessToken = localStorage.getItem('accessToken')
      user = localStorage.getItem('user')
      userType = localStorage.getItem('userType')
      if(!Ember.isEmpty(accessToken) && !Ember.isEmpty(user))
        @authenticate accessToken, user, userType, (response) =>
          return resolve(@get('apiKey.user')) if response == 'success'
          reject('denied')
      else
        reject('denied')

  authenticate: (accessToken, userId, userType, callback) ->
    $.ajaxSetup
      headers: { 'Authorization': 'Bearer ' + accessToken }

    @store.find(userType, userId).then (user) =>
      @set('apiKey', Sanatid.ApiKey.create
        accessToken: accessToken
        user: user
        userType: userType
      )
      @set('authenticated', true)
      callback('success') if callback

    , (response) =>
      if response.status == 401
        @reset()
        @set('authenticated', false)
        callback('denied')
      else
        console.log(response)

  reset: ->
    Ember.run.sync()
    Ember.run.next(@, =>
      @set('apiKey', null)
      $.ajaxSetup
        headers: { 'Authorization': 'Bearer none' }
      Ember.run.next(@, ->  Sanatid.reset())
    )

  apiKeyObserver: (->
    if(Ember.isEmpty(@get('apiKey')))
      localStorage.removeItem('accessToken')
      localStorage.removeItem('user')
      localStorage.removeItem('userType')
    else
      localStorage.setItem('accessToken', @get('apiKey.accessToken'))
      localStorage.setItem('user', @get('apiKey.user.id'))
      localStorage.setItem('userType', @get('apiKey.userType'))
  ).observes('apiKey')

