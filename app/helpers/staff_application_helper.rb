module StaffApplicationHelper
  def flash_class(level)
    case level
      when :notice then 'blue'
      when :success then 'green'
      when :error then 'red'
      when :warning then 'yellow'
    end
  end
end
