xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  xml.url do
    xml.loc "https://#{@host}"
    xml.changefreq 'monthly'
  end

  @pages.each do |path|
    xml.url do
      xml.loc "https://#{@host}/#{path}"
      xml.changefreq('monthly')
    end
  end
end