class AddConfirmedToClips < ActiveRecord::Migration
  def change
    add_column :clips, :confirmed, :boolean
    add_column :clips, :confirmed_at, :datetime
  end
end
