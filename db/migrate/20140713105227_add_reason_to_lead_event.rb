class AddReasonToLeadEvent < ActiveRecord::Migration
  def change
    add_column :lead_events, :reason, :string
  end
end
