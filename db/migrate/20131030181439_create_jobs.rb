class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :description
      t.integer :customer_id
      t.integer :service_id

      t.timestamps
    end
  end
end
