class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.integer :product_family_id
      t.string :name
      t.text :description
      t.string :code

      t.timestamps
    end
  end
end
