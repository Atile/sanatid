class CreateNewsletterSubscribers < ActiveRecord::Migration
  def change
    create_table :newsletter_subscribers do |t|
      t.string :email
      t.integer :user_id
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
