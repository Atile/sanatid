class AddImageAndPublishToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :image, :string
    add_column :blogs, :published, :boolean
  end
end
