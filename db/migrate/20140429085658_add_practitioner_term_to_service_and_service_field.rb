class AddPractitionerTermToServiceAndServiceField < ActiveRecord::Migration
  def change
    add_column :service_fields, :practitioner_term, :string
    add_column :services, :practitioner_term, :string
  end
end
