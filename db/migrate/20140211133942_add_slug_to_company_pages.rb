class AddSlugToCompanyPages < ActiveRecord::Migration
  def change
    add_column :company_pages, :slug, :string
  end
end
