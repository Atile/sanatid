class CreateProductSubscriptions < ActiveRecord::Migration
  def change
    create_table :product_subscriptions do |t|
      t.date :expires_at
      t.date :renewal_at
      t.date :next_renewal_at
      t.boolean :active
      t.integer :company_id
      t.integer :product_offer_line_id

      t.timestamps
    end
  end
end
