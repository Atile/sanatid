class AddIconClassesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :icon_class, :string
    add_column :product_components, :icon_class, :string
    add_column :product_component_families, :icon_class, :string
  end
end
