class AddChargifyConfirmedAtForProductOffers < ActiveRecord::Migration
  def change
    add_column :product_offers, :chargify_activated_at, :datetime
  end
end
