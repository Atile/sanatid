class AddCommissionLineIdToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :commission_line_id, :integer
  end
end
