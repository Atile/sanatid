class CreateFacilityImages < ActiveRecord::Migration
  def change
    create_table :facility_images do |t|
      t.string :image
      t.string :name
      t.text :description
      t.integer :company_id
      t.integer :weight, default: 0

      t.timestamps
    end
  end
end
