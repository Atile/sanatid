class AddTempFieldsToBookings < ActiveRecord::Migration
  def change
    add_column :calendar_events, :name, :string
    add_column :calendar_events, :email, :string
    add_column :calendar_events, :phone, :string
    add_column :calendar_events, :comment, :text
    add_column :calendar_events, :menu_item_id, :integer
  end
end
