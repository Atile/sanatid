class UseStatesOnClipsInstead < ActiveRecord::Migration
  def change
    remove_column :clips, :confirmed
    remove_column :clips, :confirmed_at

    add_column :clips, :status, :string, default: 'undecided'
    add_column :clips, :status_updated_at, :datetime
  end
end
