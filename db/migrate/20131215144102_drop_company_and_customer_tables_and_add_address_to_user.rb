class DropCompanyAndCustomerTablesAndAddAddressToUser < ActiveRecord::Migration
  def change
    add_column :users, :address, :string
    add_column :users, :latitude, :float
    add_column :users, :longitude, :float

    drop_table :companies
    drop_table :customers
  end
end
