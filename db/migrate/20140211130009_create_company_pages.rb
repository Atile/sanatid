class CreateCompanyPages < ActiveRecord::Migration
  def change
    create_table :company_pages do |t|
      t.string :title
      t.text :content
      t.integer :company_id
      t.integer :weight
      t.integer :parent_page_id

      t.timestamps
    end
  end
end
