class CreateProductFamilies < ActiveRecord::Migration
  def change
    create_table :product_families do |t|
      t.string :name
      t.text :description
      t.integer :chargify_id
      t.text :chargify_handle

      t.timestamps
    end
  end
end
