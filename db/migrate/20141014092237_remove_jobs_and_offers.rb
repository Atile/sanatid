class RemoveJobsAndOffers < ActiveRecord::Migration
  def change
    drop_table :reservations
    drop_table :job_questions
    drop_table :jobs
    drop_table :jobs_services
    drop_table :offers



    remove_column :commission_lines, :job_id
    remove_column :messages, :job_id
    remove_column :reviews, :job_id
    remove_column :vouchers, :offer_id
  end
end
