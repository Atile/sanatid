class CreateProductOffers < ActiveRecord::Migration
  def change
    create_table :product_offers do |t|
      t.boolean :confirmed
      t.date :confirmed_at
      t.integer :staff_id
      t.integer :company_id

      t.timestamps
    end
  end
end
