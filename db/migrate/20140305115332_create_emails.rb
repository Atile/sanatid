class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :machine_name
      t.string :subject
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
