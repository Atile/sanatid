class AddManyServiceFieldsToManyToCompanies < ActiveRecord::Migration
  def change
    create_table :service_fields_users
    add_column :service_fields_users, :service_field_id, :integer
    add_column :service_fields_users, :company_id, :integer
  end
end
