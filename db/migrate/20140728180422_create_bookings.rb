class CreateBookings < ActiveRecord::Migration
  def change
    add_column :calendar_events, :type, :string
    add_column :calendar_events, :status, :string
    add_column :calendar_events, :order_id, :integer
    add_column :calendar_events, :customer_id, :integer
  end
end
