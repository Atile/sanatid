class RemoveWalletsAndTransactions < ActiveRecord::Migration
  def change
    drop_table :transactions
    drop_table :wallets
  end
end
