class CreateJobsAndServices < ActiveRecord::Migration
  def change
    create_table :jobs_services do |t|
      t.belongs_to :service
      t.belongs_to :job
    end
    remove_column :jobs, :service_id
  end
end
