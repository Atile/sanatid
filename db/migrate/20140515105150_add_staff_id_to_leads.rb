class AddStaffIdToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :staff_id, :integer
    add_column :leads, :archived, :boolean, default: false

    Lead.update_all(archived: false)
  end
end
