class AddPublishedToServicesAndServiceFields < ActiveRecord::Migration
  def change
    add_column :services, :published, :boolean
    add_column :service_fields, :published, :boolean
  end
end
