class AddCouponIdToProductOffers < ActiveRecord::Migration
  def change
    add_column :product_offers, :coupon_id, :integer
  end
end
