class CreateInvoiceLineItems < ActiveRecord::Migration
  def change
    create_table :invoice_line_items do |t|
      t.integer :invoice_id
      t.integer :quantity
      t.string :quantity_type
      t.float :total
      t.boolean :tax
      t.string :currency
      t.string :description
      t.integer :product_subscription_id

      t.timestamps
    end
  end
end
