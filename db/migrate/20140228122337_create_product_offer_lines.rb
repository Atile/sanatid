class CreateProductOfferLines < ActiveRecord::Migration
  def change
    create_table :product_offer_lines do |t|
      t.integer :product_offer_id
      t.integer :product_id
      t.decimal :previous_price
      t.decimal :price
      t.boolean :renewable
      t.boolean :auto_renewable
      t.string :duration

      t.timestamps
    end
  end
end
