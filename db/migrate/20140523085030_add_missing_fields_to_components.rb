class AddMissingFieldsToComponents < ActiveRecord::Migration
  def change
    add_column :product_components, :price, :integer
    add_column :product_component_families, :product_family_id, :integer
  end
end
