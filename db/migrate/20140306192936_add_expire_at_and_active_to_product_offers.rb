class AddExpireAtAndActiveToProductOffers < ActiveRecord::Migration
  def change
    add_column :product_offers, :expire_at, :datetime
    add_column :product_offers, :active, :boolean
  end
end
