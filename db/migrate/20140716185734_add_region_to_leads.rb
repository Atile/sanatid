class AddRegionToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :region, :string

    regions = YAML.load_file('config/regions/da.yml')
    Lead.all.each do |lead|
      if lead.city.present?
        regions.flat_map do |region, postals|
          postals.flat_map do |postal|
            if postal == lead.city
              lead.update_attributes(region: region)
              next
            end
          end
        end
      end
    end
  end
end
