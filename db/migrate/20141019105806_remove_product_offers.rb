class RemoveProductOffers < ActiveRecord::Migration
  def change
    drop_table :product_offers
    drop_table :product_offers_products
  end
end
