class AddWinningOfferToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :winning_offer_id, :integer
  end
end
