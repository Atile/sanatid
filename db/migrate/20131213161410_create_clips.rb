class CreateClips < ActiveRecord::Migration
  def change
    create_table :clips do |t|
      t.integer :voucher_id

      t.timestamps
    end
  end
end
