class AddOpeningHoursToUsers < ActiveRecord::Migration
  def change
    add_column :users, :opening_hours, :text
  end
end
