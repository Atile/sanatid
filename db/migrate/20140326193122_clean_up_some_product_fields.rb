class CleanUpSomeProductFields < ActiveRecord::Migration
  def change
    drop_table :product_offer_lines

    create_table :product_offers_products do |t|
      t.belongs_to :product
      t.belongs_to :product_offer
    end
    add_index :product_offers_products, :product_id
    add_index :product_offers_products, :product_offer_id

    remove_column :product_subscriptions, :product_offer_line_id
    add_column :product_subscriptions, :product_id, :integer
  end
end
