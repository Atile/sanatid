class AddCvrAndCompanyEmailToCompanies < ActiveRecord::Migration
  def change
    add_column :users, :cvr, :integer
    add_column :users, :company_email, :string
  end
end
