class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :subject
      t.text :content
      t.integer :recipient_id
      t.integer :dispatcher_id
      t.integer :job_id

      t.timestamps
    end
  end
end
