class AddCustomerIdToApiKeys < ActiveRecord::Migration
  def change
    add_column :api_keys, :customer_id, :integer
  end
end
