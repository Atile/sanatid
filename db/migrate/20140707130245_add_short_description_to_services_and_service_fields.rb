class AddShortDescriptionToServicesAndServiceFields < ActiveRecord::Migration
  def change
    add_column :services, :short_description, :text
    add_column :service_fields, :short_description, :text
  end
end