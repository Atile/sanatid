class AddRelationToServiceFieldFromLead < ActiveRecord::Migration
  def change
    remove_column :leads, :service_field
    add_column :leads, :service_field_id, :integer
  end
end
