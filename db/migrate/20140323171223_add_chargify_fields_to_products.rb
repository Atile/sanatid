class AddChargifyFieldsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :chargify_id, :integer
    add_column :products, :chargify_handle, :string
  end
end
