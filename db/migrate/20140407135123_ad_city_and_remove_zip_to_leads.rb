class AdCityAndRemoveZipToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :city, :integer
    remove_column :leads, :zip
  end
end
