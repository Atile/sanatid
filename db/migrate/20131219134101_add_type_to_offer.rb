class AddTypeToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :offer_type, :string
  end
end
