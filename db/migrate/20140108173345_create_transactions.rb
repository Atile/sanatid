class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.float :amount
      t.integer :buyer_wallet_id
      t.integer :seller_wallet_id
      t.integer :job_id

      t.timestamps
    end
  end
end
