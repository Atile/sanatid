class AddMaxOfferCountToProducts < ActiveRecord::Migration
  def change
    add_column :products, :offer_limit, :integer
  end
end
