class AddPriceToProductSubscriptions < ActiveRecord::Migration
  def change
    add_column :product_subscriptions, :price, :float
    add_column :product_subscriptions, :staff_id, :integer
  end
end
