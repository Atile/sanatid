class ChangeCompanyidTypeToIntegerForVouchers < ActiveRecord::Migration

  def up
    connection.execute(%q{
    alter table vouchers
    alter column company_id
    type integer using cast(company_id as integer)
  })
  end
end
