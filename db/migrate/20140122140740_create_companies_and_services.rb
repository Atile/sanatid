class CreateCompaniesAndServices < ActiveRecord::Migration
  def change
    create_table :services_users do |t|
      t.belongs_to :service
      t.belongs_to :company
    end
  end
end
