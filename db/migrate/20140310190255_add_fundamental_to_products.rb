class AddFundamentalToProducts < ActiveRecord::Migration
  def change
    add_column :products, :fundamental, :boolean, default: false
  end
end
