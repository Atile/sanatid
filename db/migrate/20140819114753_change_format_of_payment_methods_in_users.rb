class ChangeFormatOfPaymentMethodsInUsers < ActiveRecord::Migration
  def up
    change_column :users, :payment_methods, :text, default: "--- []\n"

    Company.all.each { |c| c.update_attributes(payment_methods: '---\n- :name: Bankoverførsel\n  :enabled: false\n- :name: Kreditkort\n  :enabled: false\n- :name: Mobile Pay\n  :enabled: false\n- :name: Kontant\n  :enabled: false\n') }
  end

  def down
    change_column :users, :payment_methods, :string, default: {}
  end
end
