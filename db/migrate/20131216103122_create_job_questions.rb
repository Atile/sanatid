class CreateJobQuestions < ActiveRecord::Migration
  def change
    create_table :job_questions do |t|
      t.integer :job_id
      t.integer :company_id
      t.string :question
      t.string :answer

      t.timestamps
    end
  end
end
