class CreateProductComponents < ActiveRecord::Migration
  def change
    create_table :product_components do |t|
      t.string :name
      t.text :description
      t.integer :chargify_id
      t.integer :product_component_family_id

      t.timestamps
    end
  end
end
