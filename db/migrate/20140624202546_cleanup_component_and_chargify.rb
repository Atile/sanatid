class CleanupComponentAndChargify < ActiveRecord::Migration
  def change
    drop_table :coupons
    drop_table :product_component_families
    drop_table :product_components

    remove_columns :product_subscriptions, :item_id, :item_type, :chargify_subscription_id
    add_column :product_subscriptions, :product_id, :integer

    remove_columns :product_families, :chargify_id, :chargify_handle
    remove_columns :product_offers, :chargify_activated_at, :coupon_id
    remove_columns :products, :chargify_id, :chargify_handle
  end
end
