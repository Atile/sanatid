class AddFlowActiveForServiceFieldsAndServices < ActiveRecord::Migration
  def change
    add_column :service_fields, :flow_active, :boolean, default: false
    add_column :services, :flow_active, :boolean, default: false
  end
end
