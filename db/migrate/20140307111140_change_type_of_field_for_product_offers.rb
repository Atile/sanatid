class ChangeTypeOfFieldForProductOffers < ActiveRecord::Migration
  def up
    change_column :product_offers, :confirmed_at, :datetime
    change_column :product_subscriptions, :expires_at, :datetime
    change_column :product_subscriptions, :renewal_at, :datetime
    change_column :product_subscriptions, :next_renewal_at, :datetime
  end

  def down
    change_column :product_offers, :confirmed_at, :date
    change_column :product_subscriptions, :expires_at, :date
    change_column :product_subscriptions, :renewal_at, :date
    change_column :product_subscriptions, :next_renewal_at, :date
  end
end
