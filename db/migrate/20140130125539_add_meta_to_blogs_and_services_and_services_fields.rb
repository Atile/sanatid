class AddMetaToBlogsAndServicesAndServicesFields < ActiveRecord::Migration
  def change
    add_column :service_fields, :meta_description, :string
    add_column :services, :meta_description, :string
    add_column :blogs, :meta_description, :string
  end
end
