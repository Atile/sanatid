class RemoveActiveFromNewsletterSubscribers < ActiveRecord::Migration
  def change
    remove_column :newsletter_subscribers, :active
  end
end
