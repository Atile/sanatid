class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :reference_id
      t.string :state
      t.datetime :start_date
      t.datetime :end_date
      t.datetime :due_date
      t.datetime :paid_at
      t.integer :company_id

      t.timestamps
    end
  end
end
