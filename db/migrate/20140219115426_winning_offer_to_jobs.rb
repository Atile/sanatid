class WinningOfferToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :winning_offer_id, :integer
    remove_column :offers, :winning_offer_id
  end
end
