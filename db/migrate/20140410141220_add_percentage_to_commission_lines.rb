class AddPercentageToCommissionLines < ActiveRecord::Migration
  def change
    add_column :commission_lines, :percentage, :integer
  end
end
