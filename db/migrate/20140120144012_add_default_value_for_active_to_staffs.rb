class AddDefaultValueForActiveToStaffs < ActiveRecord::Migration
  def change
    change_column :staffs, :active, :boolean, default: true
  end
end
