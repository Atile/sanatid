class AddFieldsToProductComponents < ActiveRecord::Migration
  def change
    add_column :product_component_families, :weight, :integer
    add_column :product_components, :weight, :integer
    add_column :product_components, :handle, :string
    add_column :product_components, :billing_period, :string
    change_column :product_components, :price, :float
  end
end
