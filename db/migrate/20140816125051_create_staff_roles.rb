class CreateStaffRoles < ActiveRecord::Migration
  def change
    create_table :staff_roles do |t|
      t.string :name

      t.timestamps
    end

    %w(seller supporter writer administrator).each do |role|
      StaffRole.create(name: role)
    end
  end
end
