class AddNewFieldsToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :service_field_id, :integer
    add_column :jobs, :available_next, :boolean
    add_column :jobs, :available_from, :datetime
  end
end
