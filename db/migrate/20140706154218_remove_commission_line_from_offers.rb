class RemoveCommissionLineFromOffers < ActiveRecord::Migration
  def change
    remove_column :offers, :commission_line_id
  end
end
