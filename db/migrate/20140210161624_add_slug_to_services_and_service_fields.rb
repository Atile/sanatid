class AddSlugToServicesAndServiceFields < ActiveRecord::Migration
  include Slugifier

  def change
    add_column :services, :slug, :string
    add_column :service_fields, :slug, :string

    Service.all.each do |service|
      service.slug = self.generate_slug(service.name)
      service.save
    end

    ServiceField.all.each do |service_field|
      service_field.slug = self.generate_slug(service_field.name)
      service_field.save
    end
  end
end
