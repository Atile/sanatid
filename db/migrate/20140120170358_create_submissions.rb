class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.string :form
      t.text :message
      t.string :email
      t.string :subject
      t.string :name

      t.timestamps
    end
  end
end
