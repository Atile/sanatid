class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.string :duration
      t.integer :commission
      t.boolean :active
      t.string :billing_period

      t.timestamps
    end
  end
end
