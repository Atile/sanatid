class AddAcceptedAtToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :accepted_at, :datetime
  end
end
