class AddPaymentMethodsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :payment_methods, :string
  end
end
