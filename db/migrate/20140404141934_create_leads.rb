class CreateLeads < ActiveRecord::Migration
  def change
    create_table :leads do |t|
      t.string :name
      t.string :company_name
      t.string :phone
      t.string :homepage
      t.string :email
      t.string :service_field
      t.string :address
      t.integer :zip
      t.integer :cvr

      t.timestamps
    end
  end
end
