class AddChargifySubscriptionIdToProductSubscriptions < ActiveRecord::Migration
  def change
    add_column :product_subscriptions, :chargify_subscription_id, :integer
  end
end
