class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.integer :offer_id
      t.integer :customer_id
      t.string :company_id
      t.float :price
      t.integer :clip_count

      t.timestamps
    end
  end
end
