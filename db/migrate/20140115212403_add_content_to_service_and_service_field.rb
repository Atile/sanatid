class AddContentToServiceAndServiceField < ActiveRecord::Migration
  def change
    add_column :service_fields, :content, :string
    add_column :services, :content, :string
  end
end
