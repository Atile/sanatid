class AddAbunchOfAttributesForStaffs < ActiveRecord::Migration
  def change
    add_column :staffs, :description, :text
    add_column :staffs, :job_title, :string
    add_column :staffs, :image, :string
    add_column :staffs, :twitter, :string
    add_column :staffs, :linkedin, :string
    add_column :staffs, :published, :boolean
    add_column :staffs, :active, :boolean
  end
end
