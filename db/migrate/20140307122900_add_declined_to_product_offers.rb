class AddDeclinedToProductOffers < ActiveRecord::Migration
  def change
    add_column :product_offers, :declined, :boolean, default: false
  end
end
