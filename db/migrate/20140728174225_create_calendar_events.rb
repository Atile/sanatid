class CreateCalendarEvents < ActiveRecord::Migration
  def change
    create_table :calendar_events do |t|
      t.integer :company_id
      t.datetime :starts_at
      t.datetime :ends_at
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
