class AddCategoryToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :category, :string
    Lead.update_all(category: 'scraper')
  end
end
