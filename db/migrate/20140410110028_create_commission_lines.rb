class CreateCommissionLines < ActiveRecord::Migration
  def change
    create_table :commission_lines do |t|
      t.integer :year
      t.integer :month
      t.integer :company_id
      t.float :amount
      t.boolean :payed

      t.timestamps
    end
  end
end
