class AddParentToPage < ActiveRecord::Migration
  def change
    add_column :pages, :parent_id, :integer
    add_column :pages, :category, :string

    Page.update_all(category: 'footer')
  end
end
