class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :job_id
      t.datetime :expires_at
      t.integer :company_id
      t.string :reservation_type

      t.timestamps
    end
  end
end
