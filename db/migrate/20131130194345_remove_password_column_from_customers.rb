class RemovePasswordColumnFromCustomers < ActiveRecord::Migration
  def self.up
    remove_column :customers, :password
  end
end
