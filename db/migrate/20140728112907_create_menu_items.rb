class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|
      t.integer :company_id
      t.string :name
      t.text :description
      t.integer :weight
      t.float :price
      t.integer :duration

      t.timestamps
    end
  end
end
