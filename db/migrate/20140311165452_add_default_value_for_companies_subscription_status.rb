class AddDefaultValueForCompaniesSubscriptionStatus < ActiveRecord::Migration
  def change
    change_column :users, :subscription_status, :string, default: 'passive'
  end
end
