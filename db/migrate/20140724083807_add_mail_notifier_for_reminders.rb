class AddMailNotifierForReminders < ActiveRecord::Migration
  def change
    add_column :reminders, :notify, :boolean, default: false
    add_column :reminders, :notified, :boolean, default: false
  end
end
