class RefactorCommissionLines < ActiveRecord::Migration
  def change
    remove_column :commission_lines, :payed
    remove_column :commission_lines, :year
    remove_column :commission_lines, :month
    remove_column :commission_lines, :amount
    add_column :commission_lines, :job_id, :integer
    add_column :commission_lines, :description, :string
    add_column :commission_lines, :invoice_id, :integer
    add_column :commission_lines, :total, :float
    add_column :commission_lines, :tax, :boolean
    add_column :commission_lines, :currency, :string
  end
end
