class RenameServiceCategoryToServiceField < ActiveRecord::Migration
  def change
    rename_table :service_categories, :service_fields
    rename_column :services, :service_category_id, :service_field_id
  end
end
