class CreateProductComponentFamilies < ActiveRecord::Migration
  def change
    create_table :product_component_families do |t|
      t.string :name
      t.text :description
      t.string :handle

      t.timestamps
    end
  end
end
