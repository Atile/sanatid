class PriceShouldBeOnOfferInsteadAndAddDescriptionToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :description, :string
    add_column :offers, :price, :float
    remove_column :vouchers, :price
  end
end
