class UpdateSomeColumnNames < ActiveRecord::Migration
  def change
    rename_column :product_offer_lines, :auto_renewable, :auto_renew
    remove_column :product_offer_lines, :renewable

    rename_column :product_subscriptions, :renewal_at, :renewed_at
  end
end
