class CreateReminders < ActiveRecord::Migration
  def change
    create_table :reminders do |t|
      t.datetime :date
      t.text :comment
      t.integer :lead_id
      t.integer :staff_id

      t.timestamps
    end
  end
end
