class AddPolyAssociationsToProductSubscriptions < ActiveRecord::Migration
  def change
    remove_column :product_subscriptions, :product_id
    add_column :product_subscriptions, :item_id, :integer
    add_column :product_subscriptions, :item_type, :string
  end
end
