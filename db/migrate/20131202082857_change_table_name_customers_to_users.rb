class ChangeTableNameCustomersToUsers < ActiveRecord::Migration
  def change
    rename_table :users, :temp
    rename_table :customers, :users
    rename_table :temp, :customers
  end
end
