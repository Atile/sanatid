class CreateLeadEvents < ActiveRecord::Migration
  def change
    create_table :lead_events do |t|
      t.string :event_type
      t.integer :lead_id
      t.integer :staff_id
      t.text :comment

      t.timestamps
    end
  end
end
