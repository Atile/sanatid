class AddSpecificFieldsForCoupons < ActiveRecord::Migration
  def change
    add_column :coupons, :percentage, :integer
    add_column :coupons, :amount, :integer
    add_column :coupons, :duration, :integer
  end
end
