class ChangeApiKeysCustomeridToUserId < ActiveRecord::Migration
  def change
    rename_column :api_keys, :customer_id, :user_id
  end
end
