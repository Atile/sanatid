class ChangeDurationToIntegerOnProducts < ActiveRecord::Migration
  def up
    execute 'ALTER TABLE products ALTER COLUMN duration TYPE integer USING (duration::integer)'
    execute 'ALTER TABLE product_offer_lines ALTER COLUMN duration TYPE integer USING (duration::integer)'
  end

  def down
    execute 'ALTER TABLE products ALTER COLUMN duration TYPE integer USING (duration::text)'
    execute 'ALTER TABLE product_offer_lines ALTER COLUMN duration TYPE integer USING (duration::text)'
  end
end
