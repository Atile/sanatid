class AddProductFamilyIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :product_family_id, :integer
  end
end
