class AddDefaultJsonStructureToOpeningHoursForCompanies < ActiveRecord::Migration
  def change
    change_column :users, :opening_hours, :string, default: {monday: nil, tuesday: nil, wednesday: nil, thursday: nil, friday: nil, saturday: nil, sunday: nil}
    Company.all.each { |company| company.update_attributes(opening_hours: {monday: nil, tuesday: nil, wednesday: nil, thursday: nil, friday: nil, saturday: nil, sunday: nil}) if company.opening_hours.nil? }
  end
end
