class AddSlugAndPublicToLeads < ActiveRecord::Migration
  def change
    add_column :leads, :slug, :string
    add_column :leads, :public, :boolean, default: true

    Lead.all.each { |l| l.update_attributes(slug: l.company_name.parameterize)}
  end
end
