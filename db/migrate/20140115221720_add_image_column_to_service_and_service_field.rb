class AddImageColumnToServiceAndServiceField < ActiveRecord::Migration
  def change
    add_column :services, :image, :string
    add_column :service_fields, :image, :string
  end
end
