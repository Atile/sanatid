class ChangeColummnTypeOfContent < ActiveRecord::Migration
  def change
    change_column :services, :content, :text
    change_column :service_fields, :content, :text
  end
end
