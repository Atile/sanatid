class ChangeColumnTypeOfDescription < ActiveRecord::Migration
  def change
    change_column :services, :description, :text
    change_column :service_fields, :description, :text
  end
end
