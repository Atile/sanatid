class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :rating
      t.text :content
      t.integer :company_id
      t.integer :customer_id

      t.timestamps
    end
  end
end
