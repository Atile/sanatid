class AddMissingFieldsToProductOffers < ActiveRecord::Migration
  def change
    add_column :product_offers, :description, :text
    add_column :product_offer_lines, :billing_period, :text
    add_column :product_offer_lines, :commission, :integer
  end
end
