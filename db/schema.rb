# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141020110633) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "api_keys", force: true do |t|
    t.string   "access_token"
    t.string   "scope"
    t.datetime "expired_at"
    t.datetime "created_at"
    t.integer  "user_id"
  end

  add_index "api_keys", ["access_token"], name: "index_api_keys_on_access_token", unique: true, using: :btree

  create_table "blogs", force: true do |t|
    t.string   "title"
    t.text     "summary"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
    t.boolean  "published"
    t.string   "slug"
    t.integer  "staff_id"
    t.string   "meta_description"
  end

  create_table "calendar_events", force: true do |t|
    t.integer  "company_id"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.string   "status"
    t.integer  "order_id"
    t.integer  "customer_id"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "comment"
    t.integer  "menu_item_id"
  end

  create_table "clips", force: true do |t|
    t.integer  "voucher_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",            default: "undecided"
    t.datetime "status_updated_at"
  end

  create_table "commission_lines", force: true do |t|
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "percentage"
    t.string   "description"
    t.integer  "invoice_id"
    t.float    "total"
    t.boolean  "tax"
    t.string   "currency"
  end

  create_table "company_pages", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "company_id"
    t.integer  "weight"
    t.integer  "parent_page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "emails", force: true do |t|
    t.string   "machine_name"
    t.string   "subject"
    t.string   "title"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
  end

  create_table "facility_images", force: true do |t|
    t.string   "image"
    t.string   "name"
    t.text     "description"
    t.integer  "company_id"
    t.integer  "weight",      default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoice_line_items", force: true do |t|
    t.integer  "invoice_id"
    t.integer  "quantity"
    t.string   "quantity_type"
    t.float    "total"
    t.boolean  "tax"
    t.string   "currency"
    t.string   "description"
    t.integer  "product_subscription_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: true do |t|
    t.string   "reference_id"
    t.string   "state"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "due_date"
    t.datetime "paid_at"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lead_events", force: true do |t|
    t.string   "event_type"
    t.integer  "lead_id"
    t.integer  "staff_id"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "reason"
  end

  create_table "leads", force: true do |t|
    t.string   "name"
    t.string   "company_name"
    t.string   "phone"
    t.string   "homepage"
    t.string   "email"
    t.string   "address"
    t.integer  "cvr"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "city"
    t.string   "category"
    t.integer  "staff_id"
    t.boolean  "archived",         default: false
    t.string   "region"
    t.string   "slug"
    t.boolean  "public",           default: true
    t.integer  "service_field_id"
    t.float    "latitude"
    t.float    "longitude"
  end

  create_table "menu_items", force: true do |t|
    t.integer  "company_id"
    t.string   "name"
    t.text     "description"
    t.integer  "weight"
    t.float    "price"
    t.integer  "duration"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.text     "subject"
    t.text     "content"
    t.integer  "recipient_id"
    t.integer  "dispatcher_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "newsletter_subscribers", force: true do |t|
    t.string   "email"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pages", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.boolean  "published"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "parent_id"
    t.string   "category"
    t.integer  "weight",     default: 0
  end

  create_table "product_families", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_subscriptions", force: true do |t|
    t.datetime "expires_at"
    t.datetime "renewed_at"
    t.datetime "next_renewal_at"
    t.boolean  "active"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_id"
    t.float    "price"
    t.integer  "staff_id"
  end

  create_table "products", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price"
    t.integer  "duration"
    t.integer  "commission"
    t.boolean  "active"
    t.string   "billing_period"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "fundamental",       default: false
    t.integer  "product_family_id"
    t.integer  "weight",            default: 0
    t.string   "icon_class"
    t.string   "handle"
  end

  create_table "reminders", force: true do |t|
    t.datetime "date"
    t.text     "comment"
    t.integer  "lead_id"
    t.integer  "staff_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "notify",     default: false
    t.boolean  "notified",   default: false
  end

  create_table "reviews", force: true do |t|
    t.integer  "rating"
    t.text     "content"
    t.integer  "company_id"
    t.integer  "customer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "service_fields", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "content"
    t.string   "image"
    t.boolean  "published"
    t.string   "meta_description"
    t.string   "slug"
    t.string   "practitioner_term"
    t.boolean  "flow_active",       default: false
    t.text     "short_description"
  end

  create_table "service_fields_users", force: true do |t|
    t.integer "service_field_id"
    t.integer "company_id"
  end

  create_table "services", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "service_field_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "content"
    t.string   "image"
    t.boolean  "published"
    t.string   "meta_description"
    t.string   "slug"
    t.string   "practitioner_term"
    t.boolean  "flow_active",       default: false
    t.text     "short_description"
  end

  create_table "services_users", force: true do |t|
    t.integer "service_id"
    t.integer "company_id"
  end

  create_table "staff_roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "staffs", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.integer  "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.string   "job_title"
    t.string   "image"
    t.string   "twitter"
    t.string   "linkedin"
    t.boolean  "published"
    t.boolean  "active",          default: true
    t.integer  "staff_role_id"
  end

  create_table "submissions", force: true do |t|
    t.string   "form"
    t.text     "message"
    t.string   "email"
    t.string   "subject"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "uploads", force: true do |t|
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.integer  "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.string   "type"
    t.string   "address"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "cvr"
    t.string   "company_email"
    t.string   "company_name"
    t.string   "slug"
    t.text     "description"
    t.string   "subscription_status", default: "passive"
    t.string   "opening_hours",       default: "---\n:monday: \n:tuesday: \n:wednesday: \n:thursday: \n:friday: \n:saturday: \n:sunday: \n"
    t.string   "banner"
    t.string   "logo"
    t.boolean  "active",              default: true
    t.integer  "lead_id"
    t.string   "homepage"
    t.text     "payment_methods",     default: "--- []\n"
  end

  create_table "vouchers", force: true do |t|
    t.integer  "customer_id"
    t.integer  "company_id"
    t.integer  "clip_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
