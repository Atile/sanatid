# Create service fields, with services.

massage = ServiceField.create name: 'Massage', description: 'Massage er godt for krop og alt i hele verden.', content: 'Her står der noget spændene om Massage'
feet_massage = Service.create name: 'Fod massage', description: 'Massage på fusserne', content: 'Det er dejligt', service_field: massage
oil_massage = Service.create name: 'Olie masssage', description: 'Noget med baby olie', content: 'Det kan vi li', service_field: massage
thai_massage = Service.create name: 'Thai massage', description: 'Ja, det ved vi vidst godt hvad er for noget', content: 'nom nom', service_field: massage

coaching = ServiceField.create name: 'Coaching', description: 'Lær dig selv bedre at kende', description: 'Det bliver du glad for i længden'
adhd_coaching = Service.create name: 'ADHD Coaching', description: 'Bliv klogere på hvorfor du har det damp i hovede', content: 'tihihi', service_field: coaching
business_coaching = Service.create name: 'Business Coaching', description: 'Lær at styre dit egen firma', content: 'Noget vi ikke har brug for', service_field: coaching
stress_coaching = Service.create name: 'Stress Coaching', description: 'Skær ned på det med stress', content: 'Godt for hovedet', service_field: coaching

therapi = ServiceField.create name: 'Terapi', description: 'Noget med nogle møder', content: 'mere behøves ikke at blive sagt'
akupunktur = Service.create name: 'Akupunktur', description: 'Få strikke pinde i ryggen', content: 'godt for blod omløbet', service_field: therapi
kiroprakktik = Service.create name: 'Kiropraktik', description: 'Kræk dine knogler på nye måder', content: 'Massere af smerte', service_field: therapi
familie_terapi = Service.create name: 'Familie terapi', description: 'Tag bukserne på i hjemmet', content: 'Aldrig mere tøffelhelt', service_field: therapi

puts 'Created services'

# Create some users.
customer_torben = Customer.create name: 'Torben Hansen', email: 'cu1@dev.com', phone: 18282828, password: '123', password_confirmation: '123'
customer_frank = Customer.create name: 'Frank Nielsen', email: 'cu2@dev.com', phone: 28282828, password: '123', password_confirmation: '123'
customer_mads = Customer.create name: 'Mads Andersen', email: 'cu3@dev.com', phone: 38282828, password: '123', password_confirmation: '123'

puts 'Created customers'

company_klaas = Company.create name: 'Klaas Fisker', email: 'com1@dev.com', phone: 11111111, company_name: 'Klaas massage', address: 'Frederikssunds vej 243, 2700', password: '123', password_confirmation: '123'
company_klaas.services << [feet_massage, oil_massage]
company_klaas.save

company_jonas = Company.create name: 'Jonas Larsen', email: 'com2@dev.com', phone: 22222222, company_name: 'Super coaching & Terapi', address: 'Peter Bangs Vej 87, 2000 Frederiksberg', password: '123', password_confirmation: '123'
company_jonas.services << [feet_massage, oil_massage, thai_massage, adhd_coaching, business_coaching, stress_coaching]
company_jonas.save

company_finn = Company.create name: 'Finn flintesten', email: 'com3@dev.com', phone: 33333333, company_name: 'We do everything', address: 'Roskildevej 303, 2610 Rødovre', password: '123', password_confirmation: '123'
company_finn.services << [feet_massage, oil_massage, thai_massage, adhd_coaching, business_coaching, stress_coaching, akupunktur, familie_terapi, kiroprakktik]
company_finn.save

company_casper = Company.create name: 'Casper senaften', email: 'com4@dev.com', phone: 44444444, company_name: 'Senaften terapi center', address: 'Virumvej 18, 2830 Virum', password: '123', password_confirmation: '123'
company_casper.services << [familie_terapi, akupunktur, familie_terapi]
company_casper.save

company_munkeby = Company.create name: 'Munkeby jegharenkatsomhoved', email: 'com5@dev.com', phone: 55555555, company_name: 'Amager coaching centrum', address: 'Amagerbrogade 61, 2300 København', password: '123', password_confirmation: '123'
company_munkeby.services << [familie_terapi, adhd_coaching, business_coaching, stress_coaching]
company_munkeby.save

company_svend = Company.create name: 'Svend fissesen', email: 'com6@dev.com', phone: 666666, company_name: 'København wellness ', address: 'Nørregade 40, 1164 København K,', password: '123', password_confirmation: '123'
company_svend.services << [familie_terapi, adhd_coaching, business_coaching, stress_coaching, akupunktur, kiroprakktik, feet_massage, oil_massage, thai_massage]
company_svend.save

puts 'Created companies'

# Create some jobs, xofx states offer count.

frank_job_3of3 = Job.create description: 'Jeg ønsker at modtage tilbud på that massage, hvis i forstår sådan lille en (Finger banker på næsen)', customer: customer_frank, services: [thai_massage], service_field: massage, available_next: true, address: 'Nørrebrogade 42'
frank_job_2of3 = Job.create description: 'Jeg lider af ondt i maven, og migræne, jeg har brug for hjælp', customer: customer_frank, service_field: coaching, available_next: true, address: 'Nørrebrogade 42'

torben_job_1of3 = Job.create description: 'Vi har problemer i familien, jeg bliver ved med at slå min kone', customer: customer_torben, service_field: therapi, services: [familie_terapi], available_next: 4.days.from_now, address: 'Bygholmvej 24, 2720 vanløse'
torben_job_2of3 = Job.create description: 'Jeg har brug for tilbud på adhd coaching, min kone er en hystade', customer: customer_torben, service_field: coaching, services: [adhd_coaching, stress_coaching], available_next: 10.days.from_now, address: 'Bygholmvej 24, 2720 vanløse'

mads_job_3of3 = Job.create description: 'Ønsker at prøve noget nyt', customer: customer_mads, service_field: massage, available_next: true, address: 'Hasselvej 2, 2600 Glostrup'
mads_job_2of3 = Job.create description: 'Har brug for at få smadret min ryg asap', customer: customer_mads , service_field: massage, services: [kiroprakktik, akupunktur, familie_terapi], available_next: true, address: 'Hasselvej 2, 2600 Glostrup'

puts 'Created jobs'
# When functionality is house, create completed jobs.


# Create some offers

Offer.create company: company_jonas, job: frank_job_3of3, description: 'Jeg har en sød thai dulle, der gerne vil massere dig', price: 299.95, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_jonas, job: frank_job_3of3, description: 'Jeg har en sød thai dulle, der gerne vil massere dig', price: 999.95, offer_type: 'voucher', voucher: Voucher.new(clip_count: 4)

Offer.create company: company_finn, job: frank_job_3of3, description: 'Ved lige hvad du har brug for', price: 555.00, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_finn, job: frank_job_3of3, description: 'Ved lige hvad du har brug for', price: 2255.00, offer_type: 'voucher', voucher: Voucher.new(clip_count: 5)

Offer.create company: company_svend, job: frank_job_3of3, description: 'Noget af det gode', price: 999.95, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_svend, job: frank_job_3of3, description: 'Ved lige hvad du har brug for', price: 7999.99, offer_type: 'voucher', voucher: Voucher.new(clip_count: 10)

puts 'Created 3/3 offers'

Offer.create company: company_munkeby, job: frank_job_2of3, description: 'Det lyder som om du har brug for stress coaching, jeg mener du har brug for 8 sessions, og har prøvet at skabe en fornuftig pris til dig', price: 5499.99, offer_type: 'voucher', voucher: Voucher.new(clip_count: 10)

Offer.create company: company_jonas, job: frank_job_2of3, description: 'Kan tilbyde dig en prøve session af stress coaching', price: 499.99, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_jonas, job: frank_job_2of3, description: 'Et klippekort, så lover jeg dig du er stress fri efter', price: 7499.99, offer_type: 'voucher', voucher: Voucher.new(clip_count: 10)

puts 'Created 2/3 offers'

Offer.create company: company_casper, job: torben_job_1of3, description: 'Jeg kan hjælpe dig med at slå ordentligt til', price: 299.95, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_casper, job: torben_job_1of3, description: 'Hvis du har brug for den store pakke', price: 3999.99, offer_type: 'voucher', voucher: Voucher.new(clip_count: 20)

puts 'Created 1/3 offer'

Offer.create company: company_munkeby, job: torben_job_2of3, description: 'Jeg kan give din kvinde det hun har brug for', price: 199.95, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_munkeby, job: torben_job_2of3, description: 'Kan også gøre det mere end en gang', price: 799.96, offer_type: 'voucher', voucher: Voucher.new(clip_count: 5)

Offer.create company: company_finn, job: torben_job_2of3, description: 'Lyder som om din kone, skal bruge en seriøs omgang coaching', price: 3799.96, offer_type: 'voucher', voucher: Voucher.new(clip_count: 8)

puts 'Created 2/3 offers'

Offer.create company: company_klaas, job: mads_job_3of3, description: 'Kan tilbyde dig en omgang fod massage, eller olie massage', price: 399.95, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_klaas, job: mads_job_3of3, description: 'Samt et gruppe tilbud', price: 3599.96, offer_type: 'voucher', voucher: Voucher.new(clip_count: 10)

Offer.create company: company_svend, job: mads_job_3of3, description: 'Frit valg på mit menu kort', price: 299.95, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_svend, job: mads_job_3of3, description: 'Frit valg på mit menu kort', price: 2399.96, offer_type: 'voucher', voucher: Voucher.new(clip_count: 10)

Offer.create company: company_jonas, job: mads_job_3of3, description: 'Alt på kortet', price: 359.95, offer_type: 'single', voucher: Voucher.new(clip_count: 1)
Offer.create company: company_jonas, job: mads_job_3of3, description: 'Alt på kortet', price: 2999.96, offer_type: 'voucher', voucher: Voucher.new(clip_count: 10)

puts 'Created 3/3 offers'

Offer.create company: company_svend, job: mads_job_2of3, description: 'En ordentlig omgang knæk i din ryg, over flere sessions', price: 2399.96, offer_type: 'voucher', voucher: Voucher.new(clip_count: 6)

Offer.create company: company_finn, job: mads_job_2of3, description: '8 gange igennem smerte helvede, velbekomme', price: 3799.96, offer_type: 'voucher', voucher: Voucher.new(clip_count: 8)

puts 'Created 2/3 offers'

Blog.create title: 'Sanatid test blog', summary: 'Summary of this awesome blog', content: 'Fine content, of this fine test blog, how great is that!?', meta_description: 'This should go into a meta tag!', published: true
Blog.create title: 'Sanatid awesome test blog', summary: 'Test blogging by Sanatid', content: 'Yes we will rock this world!', meta_description: 'Rocking the meta description', published: true
