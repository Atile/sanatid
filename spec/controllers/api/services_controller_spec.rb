require 'spec_helper'

describe Api::ServicesController do
  before do
    @service = FactoryGirl.create :service
    @service_2 = FactoryGirl.create :service, name: 'Test service 2'
  end

  describe 'viewing a service' do
    before { get :show, {id: @service.id, format: :json} }

    let(:result) { JSON.parse response.body }

    it('Should contain the correct service field id') { result['service']['id'].should eq(@service.id) }
    it('Should contain the correct title') { result['service']['name'].should eq(@service.name) }
  end

  describe 'viewing a service by slug' do
    before { get :index, {slug: @service.slug, format: :json} }

    let(:result) { JSON.parse response.body }

    it('Should contain the correct service id') { result['services'][0]['id'].should eq(@service.id)}
    it('Should contain the correct title') { result['services'][0]['name'].should eq(@service.name)}
  end

  describe 'listing all services' do
    before { get :index, { format: :json } }

    let(:result) { JSON.parse response.body }

    it('Should contain all service id') do
      result['services'].each do |service|
        [@service.id, @service_2.id].should include(service['id'])
      end
    end
    it('Should contain all service field name') do
      result['services'].each do |service|
        [@service.name, @service_2.name].should include(service['name'])
      end
    end
  end
end
