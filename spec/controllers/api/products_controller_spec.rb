require 'spec_helper'

describe Api::ProductsController do
  before do
    @product_1 = FactoryGirl.create(:product)
    @product_2 = FactoryGirl.create(:product)
  end

  describe 'when a user fetches all products' do
    before do
      get :index, {format: :json}
    end

    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should return the two created products') { expect(result['products'].count).to eql(2) }
  end

  describe 'when a user shows a specific product' do
    before do
      get :show, {id: @product_1.id, format: :json}
    end

    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should return requested product') { expect(result['product']['id']).to eql(@product_1.id) }
  end
end
