require 'spec_helper'

describe Api::CompanyPagesController do
  before do
    @company = FactoryGirl.create :company, address: 'Hillerødvej København NV'
    @company_page = FactoryGirl.create :company_page, company: @company
    @api_key = @company.session_api_key
  end

  describe 'when creating a new company page' do
    before do
      post :create, {
        company_page: {
          title: 'Test of a company page',
          content: 'and some random crap content',
          company_id: @company.id,
          weight: 2
        }
      }, {'Authorization' => "Bearer #{@api_key.access_token}"}
    end

    let(:result) { JSON.parse response.body }

    it('should return 201 status code') { expect(response.status).to eq(201) }
    it('should return the id of the new page') { expect(result['company_page']['id']).to be_present}
  end

  describe 'when updating a company page' do
    before do
      post :update, {
          id: @company_page.id,
          company_page: {
              title: 'Updated test of a company page',
              content: 'Updated and some random crap content',
              company_id: @company.id,
              weight: 2
          }
      }, {'Authorization' => "Bearer #{@api_key.access_token}"}

      @company_page.reload
    end

    it('should contain the updated title') { expect(@company_page.title).to eq('Updated test of a company page') }
    it('should contain the updated content') { expect(@company_page.content).to eq('Updated and some random crap content') }
  end

  describe 'when viewing a company pages' do
    before do
      get :show, { format: :json, id: @company_page.id }, {'Authorization' => "Bearer #{@api_key.access_token}"}
    end

    let(:result) { JSON.parse(response.body) }

    it('should return success') { expect(response.status).to eq(200) }
    it('it should contain the correct id') { expect(result['company_page']['id']).to eq(@company_page.id) }
  end

  describe 'when a company deletes a compage page' do
    before do
      delete :destroy, { format: :json, id: @company_page.id }, {'Authorization' => "Bearer #{@api_key.access_token}"}
    end

    it('should return no content') { expect(response.status).to eq(204) }
    it('should delete the company page') { expect { CompanyPage.find(@company_page) }.to raise_error }
  end
end
