require 'spec_helper'

describe Api::NewsletterSubscribersController do

  before do
    create(:email, machine_name: 'newsletter_confirmation', subject: 'bla bla', content: 'bla bla')
    @customer = FactoryGirl.create :customer
    @api_key = @customer.session_api_key
    @subscriber = FactoryGirl.create :newsletter_subscriber
  end

  describe 'when viewing a subscriber' do
    before do
      get :show, { id: @subscriber.id, format: :json }, { Authorization: "Bearer #{@api_key.access_token}" }
    end

    let(:result) { JSON.parse(response.body) }

    it('should contain a subscription') { result['newsletter_subscriber']['id'].should eq(@subscriber.id) }
  end

  describe 'when signing up as subscriber' do
    before do
      post :create, { newsletter_subscriber: { email: 'test@testemail.com' } }
    end

    it('should be persisted') { response.status.should eq(201) }
    it('should send a notification to that email') do
      expect(NewsletterMailer).to receive(:newsletter_confirmation).and_call_original
      post :create, { newsletter_subscriber: { email: 'test_2@testemail.com' } }
    end
  end

  describe 'when signing up to newsletter, as a registered user' do
    before do
      @company = FactoryGirl.create(:company)
      post :create, { newsletter_subscriber: { user_id: @company.id, email: @company.email }}
    end

    it('should create a new subscription, with company as subscriber') { NewsletterSubscriber.last.should eq(@company.newsletter_subscriber) }
    it('should return success') { response.status.should eq(201) }
  end

  describe 'when unsubscribing' do
    before do
      @subscription_delete = FactoryGirl.create :newsletter_subscriber
      delete :destroy, {id: @subscription_delete.id, newsletter_subscriber: { email: @subscription_delete.email } }
    end

    it('newsletter subscription should not exist') { NewsletterSubscriber.find_by_email(@subscription_delete.email).should be_nil }
    it('should return success') { response.status.should eq(200) }
  end

  describe 'when unsubscribing as a registered user' do
    before do
      @company_subscription_delete = FactoryGirl.create(:company, newsletter_subscriber: FactoryGirl.create(:newsletter_subscriber))
      @subscription = @company_subscription_delete.newsletter_subscriber
      delete :destroy, {id: @subscription.id }, { Authorization: "Bearer #{@company_subscription_delete.session_api_key.access_token}" }
    end

    it('newsletter subscription should not exist') { NewsletterSubscriber.find_by_id(@subscription.id).should be_nil }
    it('should return success') { response.status.should eq(200) }
  end
end
