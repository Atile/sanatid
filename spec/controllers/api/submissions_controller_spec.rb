require 'spec_helper'

describe Api::SubmissionsController do
  describe 'send new submission' do
    before do
      post :create, { submission: {
        form: 'home.contact',
        message: 'Some test message',
        email: 'test@email.com',
        subject: 'test',
        name: 'Martin Elvar'
      } }
    end

    let(:submission) { Submission.last }

    it('should return created') { response.status.should eq(201)}
    it('have the same message, as the created') { submission.message.should eq('Some test message') }
  end
end
