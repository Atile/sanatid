require 'spec_helper'

describe Api::ServiceFieldsController do
  before do
    @service_field_1 = FactoryGirl.create :service_field
    @service_field_2 = FactoryGirl.create :service_field, name: 'another service field'
  end

  describe 'viewing a service field' do
    before { get :show, {id: @service_field_1.id, format: :json} }

    let(:result) { JSON.parse response.body }

    it('Should contain the correct service field id') { result['service_field']['id'].should eq(@service_field_1.id)}
    it('Should contain the correct title') { result['service_field']['name'].should eq(@service_field_1.name)}
    it('Should contain a sub service') { result['services'][0]['name'].should eq(@service_field_1.services.first.name) }
  end

  describe 'viewing a service field by slug' do
    before { get :index, {slug: @service_field_1.slug, format: :json} }

    let(:result) { JSON.parse response.body; }

    it('Should contain the correct service field id') { result['service_fields'][0]['id'].should eq(@service_field_1.id)}
    it('Should contain the correct title') { result['service_fields'][0]['name'].should eq(@service_field_1.name)}
    it('Should contain a sub service') { result['services'][0]['name'].should eq(@service_field_1.services.first.name) }
  end

  describe 'listing all services fields' do
    before { get :index, { format: :json } }

    let(:result) { JSON.parse response.body }

    it('Should contain all service field id') do
      result['service_fields'].each do |service_field|
        [@service_field_1.id, @service_field_2.id].should include(service_field['id'])
      end
    end
    it('Should contain all service field name') do
      result['service_fields'].each do |service_field|
        [@service_field_1.name, @service_field_2.name].should include(service_field['name'])
      end
    end
  end
end
