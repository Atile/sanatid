require 'spec_helper'

describe Api::CustomersController do

  before { @customer = FactoryGirl.create :customer}
  subject { @customer }

  describe 'when a customer registers with valid data' do
    before do
      create :email, machine_name: 'welcome_customer', subject: 'Welcome', content: '{{customer-name}}'
      post 'create', {
        customer: {
          name: 'test user',
          email: 'test@email.com',
          password: 'secret',
          password_confirmation: 'secret'
        }
      }
    end

    let(:result) { JSON.parse(response.body) }

    it 'should have reference the correct customer' do
      result['api_key']['user_id'].should_not eq(0)
    end

    it 'should get a access token which consist of 32 chars' do
      result['api_key']['access_token'].should =~ /\S{32}/
    end

    it 'should send out a welcome email to the user' do
      expect(CustomerMailer).to receive(:welcome_customer).and_call_original
      post 'create', {
        customer: {
          name: 'test user',
          email: 'test_email@email.com',
          password: 'secret',
          password_confirmation: 'secret'
        }
      }
    end
  end

  describe 'when a customer registers with invalid data' do
    before do
      post 'create', {
        customer: {
          name: '',
          email: '',
          password: 'secret',
          password_confirmation: 'typoed'
        }
      }
    end

    let(:result) { JSON.parse(response.body) }

    it 'should receive the correct number errors' do
      result['errors'].count.should eq(3)
    end
  end

  describe 'when a visitor visits a restricted area without an token' do
    before { get 'show', { id: @customer.id, format: :json  } }
    it ('should return a access denied') { response.status.should eq(401) }
  end

  describe 'when a customer visits with a expired token' do
    before do
      api_key = @customer.api_keys.session.create
      api_key.expired_at = 30.days.ago
      api_key.save
      get 'show', { id: @customer.id, format: :json  }, {'Authorization' => "Bearer #{api_key.access_token}"}
    end

    it ('should return a access denied') { response.status.should eq(401) }
  end

  describe 'when a customer visits its profile a valid token' do
    before do
      api_key = @customer.api_keys.session.create

      get 'show', { id: @customer.id, format: :json }, {'Authorization' => "Bearer #{api_key.access_token}"}
    end

    it ('should return success') { response.status.should eq(200) }
  end
end