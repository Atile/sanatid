require 'spec_helper'

describe Api::PagesController do
  before { @page = FactoryGirl.create :page }

  describe 'viewing a page' do
    before { get :show, {id: @page.id, format: :json} }

    let(:result) { JSON.parse response.body}

    it('Should contain the correct page') { result['page']['id'].should eq(@page.id)}
    it('Should contain the correct title') { result['page']['title'].should eq(@page.title)}
  end

  describe 'viewing a page by slug' do
    before { get :index, {slug: @page.slug, format: :json} }

    let(:result) { JSON.parse response.body}

    it('Should contain the correct page') { result['pages'][0]['id'].should eq(@page.id)}
    it('Should contain the correct title') { result['pages'][0]['title'].should eq(@page.title)}
  end
end
