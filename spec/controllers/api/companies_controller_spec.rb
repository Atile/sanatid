require 'spec_helper'

describe Api::CompaniesController do
  before do
    @company = FactoryGirl.create :company
    @api_key = @company.api_keys.session.create
  end

  describe 'when a company visits its profile' do
    before do
      get 'show', { id: @company.id, format: :json }, {'Authorization' => "Bearer #{@api_key.access_token}"}
    end

    let(:result) { JSON.parse response.body }

    it ('should return success') { response.status.should eq(200) }
    it ('should contain a company cvr') { result['company']['cvr'].should eq(@company.cvr) }
  end

  describe 'when a anonymous visits a company profile' do
    before do
      get 'show', { id: @company.id, format: :json }
    end

    let(:result) { JSON.parse response.body }

    it ('should return success') { response.status.should eq(200) }
    it ('should not contain vulnerable information') { result['company']['cvr'].should_not eq(@company.id) }
  end

  describe 'when a anonymous visits a company profile using a slug' do
    before do
      get :index, { slug: @company.slug, format: :json }
    end

    let(:result) { JSON.parse response.body }

    it ('should return success') { expect(response.status).to eq(200) }
    it ('should not contain vulnerable information') { expect(result['companies'].first['cvr']).to be_blank }
  end

  describe 'when a user have tried a demo, and therefor signed up as a lead' do
    before do
      post :create, {format: :json, company: {phone: '23232323', company_name: 'test company name', name: 'Test person', email: 'test@testemail3434.dk'}}
    end

    let(:lead) { Lead.last }

    it('should return created') { expect(response.status).to eql(201) }
    it('should have created a lead') { expect(lead).not_to be_nil }
  end

  describe 'when a company is updating its profile' do
    before do
      @service_1 = FactoryGirl.create :service
      @service_2 = FactoryGirl.create :service, name: 'Mooore test service'

      put :update, {
        id: @company.id,
        format: :json,
          company: {
            name: 'New test company name',
            cvr: '1312830183',
            service_ids: [@service_1.id, @service_2.id]
          }
      }, {'Authorization' => "Bearer #{@api_key.access_token}"}

      @company.reload
    end

    it('should return 204') {response.status.should eq(204) }
    it('should have a updated name') { @company.name.should eq('New test company name') }
    it('should have a updated cvr') { @company.cvr.should eq(1312830183) }
    it('should have a added services') do
      @company.services.each do |service|
        [@service_1.id, @service_2.id].should include(service.id)
      end
    end
  end

  describe 'when fetching frontpage promotions' do
    before do
      promotion_product = FactoryGirl.create(:product, handle: 'frontpage-promotion-2')
      new_sub =  FactoryGirl.create(:active_subscription)
      new_sub.product = promotion_product
      @company.product_subscriptions << new_sub

      get :frontpage_promotions, {id: 0, format: :json}
    end

    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should return the company in the correct row') { expect(result['frontpage_promotion']['second_row_ids'].include?(@company.id)).to be_truthy }
  end

  describe 'when fetching flow promotions' do
    before do
      promotion_product = FactoryGirl.create(:product, handle: 'flow-promotion-2')
      new_sub =  FactoryGirl.create(:active_subscription)
      new_sub.product = promotion_product
      @company.product_subscriptions << new_sub

      # Cords for "Sønderjyllands Alle 25, 2000 Frederiksberg"; which is within 15km.
      @company.latitude = 55.681584
      @company.longitude = 12.4961
      @company.save

      # Cords for "Ålekistevej 42, 2720 Vanløse"; also within 15km, but not promoted.
      @random_company = FactoryGirl.create(:company)
      @random_company.latitude = 55.68131899999999
      @random_company.longitude = 12.484555
      @random_company.save

      # Cords for "Langdraget 27, Vanløse"
      get :flow_promotion, {format: :json, lat: 55.68188199999999, lon: 12.489395 }
    end

    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should return the company in the correct row') { expect(result['flow_promotions'][0]['second_row_ids'].include?(@company.id)).to be_truthy }
    it('should return non promoted company aswell') { expect(result['flow_promotions'][0]['fourth_row_ids'].include?(@random_company.id)).to be_truthy }
  end
end
