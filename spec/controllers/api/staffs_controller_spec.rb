require 'spec_helper'

describe Api::StaffsController do
  before do
    @staff = FactoryGirl.create :staff
    @staff_2 = FactoryGirl.create :staff, name: 'Martin Elvar', email: 'elvar@sanatid.dk'
  end

  describe 'listing all staff members' do
    before { get :index, { format: :json } }

    let(:result) { JSON.parse response.body }

    it('Should contain all staff member ids') do
      result['staffs'].each do |staff|
        [@staff.id, @staff_2.id].should include(staff['id'])
      end
    end
    it('Should contain all staff names') do
      result['staffs'].each do |staff|
        [@staff.name, @staff_2.name].should include(staff['name'])
      end
    end
  end
end
