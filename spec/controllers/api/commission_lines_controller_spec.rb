require 'spec_helper'

describe Api::CommissionLinesController do
  describe 'when fetching commission lines' do
    before do
      @company = FactoryGirl.create(:company)
      @commission_line_1 = FactoryGirl.create(:commission_line, company: @company)
      @commission_line_2 = FactoryGirl.create(:commission_line, company: @company)

      get 'index', {format: :json}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end

    let(:result) { JSON.parse(response.body) }

    it('should return the 2 commission lines') { result['commission_lines'].count.should eq(2) }
    it('should return 201 status code') { response.status.should eq(200) }
  end
end
