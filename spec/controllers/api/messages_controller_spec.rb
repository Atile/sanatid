require 'spec_helper'

describe Api::MessagesController do
  before do
    @job = FactoryGirl.create :accepted_job
    @company = @job.winning_offer.company
    @customer = @job.customer
  end

  describe 'when a company sends a message' do
    before do
      create :email, machine_name: 'customer_received_message', subject: 'bla bla', content: 'bla bla'

      @message = FactoryGirl.build(:message)
      post :create, {format: :json, message: {recipient_id: @customer.id, job_id: @job.id, content: @message.content }}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end

    let (:result) { JSON.parse response.body }

    it('should return created') { expect(response.status).to eql(201) }
    it('should return the saved message') { expect(result['message']['content']).to eql(@message.content) }
    it('should have the correct dispatcher') { expect(result['message']['dispatcher']['id']).to eql(@company.id) }
    it('should send notice email to the receiving customer') do
      expect(MessageMailer).to receive(:customer_received_message).with(instance_of Message).and_call_original
      post :create, {format: :json, message: {recipient_id: @customer.id, job_id: @job.id, content: @message.content }}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end
  end

  describe 'when a customer sends a message' do
    before do
      create :email, machine_name: 'company_received_message', subject: 'bla bla', content: 'bla bla'
      @message = FactoryGirl.build(:message)
      post :create, {format: :json, message: {recipient_id: @company.id, job_id: @job.id, content: @message.content }}, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end

    let (:result) { JSON.parse response.body }

    it('should return created') { expect(response.status).to eql(201) }
    it('should return the saved message') { expect(result['message']['content']).to eql(@message.content) }
    it('should have the correct dispatcher') { expect(result['message']['dispatcher']['id']).to eql(@customer.id) }
    it('should send notice email to the receiving company') do
      expect(MessageMailer).to receive(:company_received_message).with(instance_of Message).and_call_original
      post :create, {format: :json, message: {recipient_id: @company.id, job_id: @job.id, content: @message.content }}, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end
  end

  describe 'when a user tries to send a message, on a unauthorized job' do
    before do
      @message = FactoryGirl.build(:message)
      @malicious_user = FactoryGirl.create(:company)
      post :create, {format: :json, message: {recipient_id: @customer.id, job_id: @job.id, content: @message.content }}, {'Authorization' => "Bearer #{@malicious_user.session_api_key.access_token}"}
    end

    it('should return access denied') { expect(response.status).to eql(403) }
    it('should have a empty result') { expect(response.body).to be_blank }
  end

  describe 'when a user view messages on a job' do
    before do
      @message = FactoryGirl.create(:message, recipient: @company, dispatcher: @customer, job: @job)
      @message_2 = FactoryGirl.create(:message, recipient: @customer, dispatcher: @company, job: @job)
      post :index, {format: :json, id: @message.id, message: {job_id: @job.id} }, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end

    let (:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should return the two messages') { expect(result['messages'].count).to eql(2) }
  end

  describe 'when a unauthorized user tries to view messages on a job' do
    before do
      @message = FactoryGirl.build(:message, recipient: @company, dispatcher: @customer, job: @job)

      @malicious_user = FactoryGirl.create(:company)
      post :index, {format: :json, id: @message.id, message: {job_id: @job.id} }, {'Authorization' => "Bearer #{@malicious_user.session_api_key.access_token}"}
    end

    it('should return access denied') { expect(response.status).to eql(403) }
    it('should have a empty result') { expect(response.body).to be_blank }
  end
end
