require 'spec_helper'

describe Api::ReviewsController do
  describe 'when customer creates new review' do
    before do
      create(:email, machine_name: 'new_review')
      @job = FactoryGirl.create :accepted_job
      @customer = @job.customer
      review = FactoryGirl.build :review

      post :create, {format: :json, review: {company_id: @job.winning_offer.company.id, content: review.content, rating: review.rating, job_id: @job.id, customer_id: @customer.id}}, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end

    let (:result) { JSON.parse response.body }

    it('should return created') { expect(response.status).to eql(201) }
    it('should persist the review for the correct job') { expect(result['review']['id']).to eql(@job.review.id) }
    it('should send a email to the company, about a new review') do
      @job = FactoryGirl.create :accepted_job
      @customer = @job.customer
      review = FactoryGirl.build :review
      expect(ReviewMailer).to receive(:new_review).and_call_original
      post :create, {format: :json, review: {company_id: @job.winning_offer.company.id, content: review.content, rating: review.rating, job_id: @job.id, customer_id: @customer.id}}, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end
  end

  describe 'when a customer tries to create a review, for a non related job' do
    before do
      @job = FactoryGirl.create :accepted_job
      @customer = FactoryGirl.create :customer
      review = FactoryGirl.build :review

      post :create, {format: :json, review: {company_id: @job.winning_offer.company.id, content: review.content, rating: review.rating, job_id: @job.id, customer_id: @customer.id}}, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end

    let (:result) { JSON.parse response.body }

    it('should return access denied') { expect(response.status).to eql(403) }
    it('should not persist the review') { expect(@job.review).to be_nil }
  end

  describe 'when a anonymous views a review' do
    before do
      @job = FactoryGirl.create(:accepted_job)
      @review = FactoryGirl.create(:review, job: @job)
      get :show, {format: :json, id: @review.id }
    end

    let (:result) { JSON.parse response.body }

    it('should return 200') { expect(response.status).to eql(200) }
    it('should contain a review') { expect(result['review']['content']).to eql(@review.content)}
  end
end
