require 'spec_helper'

describe Api::VouchersController do
  before do
    @company = FactoryGirl.create :company
    @job = FactoryGirl.create :job
    @access_key = @company.session_api_key
    @offer = FactoryGirl.create :offer, company: @company, job: @job
  end

  describe 'when creating a voucher to a offer' do
    before do
      post 'create', { voucher: {
        offer_id: @offer.id,
        company_id: @company.id,
        clip_count: 1,
      } }, { Authorization: "Bearer #{@access_key.access_token}" }
    end

    it('it should be persisted') { response.status.should eq(201) }
  end

  describe 'when creating a invalid voucher' do
    before do
      post 'create', { voucher: {
        company_id: @company.id,
        clip_count: 10,
      } }, { Authorization: "Bearer #{@access_key.access_token}" }
    end

    it('should yield a error') { response.status.should eq(422) }
  end

  describe 'when showing a voucher' do
    before do
      @company.vouchers << FactoryGirl.create(:voucher, offer: @offer)
      @voucher = @company.vouchers.first
      get 'show', {id: @voucher, format: :json}, { Authorization: "Bearer #{@access_key.access_token}" }
    end

    let(:result) { JSON.parse response.body }

    it('should contain the voucher requestet') { result['voucher']['id'].should eq(@voucher.id)}
  end
end

