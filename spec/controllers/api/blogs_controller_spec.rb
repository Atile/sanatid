require 'spec_helper'

describe Api::BlogsController do

  before do
    @blog = FactoryGirl.create :blog
  end

  describe 'when a visitor request blog index' do
    before do
      FactoryGirl.create :blog, title: 'Anoother test blog'
      get :index, format: :json
    end

    let(:result) { JSON.parse(response.body) }
    it('should contain multiple blogs') { result['blogs'].count.should eq(2) }

  end

  describe 'when a visitor request a specific blog post' do
    before do
      get :show, id: @blog.id, format: :json
    end

    let(:result) { JSON.parse(response.body) }
    it('should contain single blog') { result['blog']['id'].should eq(@blog.id) }
  end
end
