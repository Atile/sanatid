require 'spec_helper'

describe Api::SessionsController do
  before { @customer = FactoryGirl.create :customer }

  describe 'when a user authenticates with incorrect credentials' do
    before do
      email = 'incorrect@mail.com'
      password = 'incorrectPassword'

      post 'create', { session: { email: email, password: password }}
    end

    it('should return a status 401') { response.status.should eq(401) }
  end

  describe 'when a user authenticates with correct credentials' do
    before do
      email = @customer.email
      password = 'awesomepassword'

      post 'create', { session: { email: email, password: password } }
    end

    let(:result) { JSON.parse(response.body) }

    it('should have a token of 32 chars') { result['api_key']['access_token'].should =~ /\S{32}/ }
    it('should have a token with the correct customer reference') { result['api_key']['user_id'].should eq(@customer.id) }
  end
end
