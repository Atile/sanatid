require 'spec_helper'

describe Api::SitemapsController do
  describe 'GET index' do
    it 'populates a array of page urls' do
      @pages = []
      service_field = FactoryGirl.create(:service_field)
      @pages << "wiki/#{service_field.slug}"
      @pages << "wiki/#{service_field.slug}/#{service_field.services.last.slug}"
      @pages << "blog/#{FactoryGirl.create(:blog).slug}"
      company = FactoryGirl.create(:company)
      @pages << "#{company.slug}"
      company.company_pages.each { |page| @pages << "#{company.slug}/#{page.id}" }
      get :index, {format: :xml}
      expect(assigns(:pages)).to eq(@pages)
    end

    it 'renders the :index view' do
      get :index, {format: :xml}
      expect(response).to render_template :index
    end
  end
end
