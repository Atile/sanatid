require 'spec_helper'

describe Api::ProductSubscriptionsController do
  describe 'when a company fetches his subscriptions' do
    before do
      @company = FactoryGirl.create :company
      # Add another product subscription, other than the one added from the factory.
      @company.product_subscriptions << FactoryGirl.create(:active_subscription)

      @company_2 = FactoryGirl.create :company

      get :index, {format: :json}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end

    let(:result) { JSON.parse response.body }

    it ('should cointain 2 product subscriptions') { expect(result['product_subscriptions'].length).to eql(2) }
    it ('not contain another companies subscription') do
      ids = result['product_subscriptions'].map { |ps| ps['id'] }
      contains_id = ids.include?(@company_2.product_subscriptions.first.id)
      expect(contains_id).to be false
    end
  end

  describe 'when a company fetches a subscription' do
    before do
      @company = FactoryGirl.create :company
      get :show, {id: @company.product_subscriptions.first.id, format: :json}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end

    let(:result) { JSON.parse response.body }

    it ('should contain the correct subscription') { expect(result['product_subscription']['id']).to eql(@company.product_subscriptions.first.id)}
  end

  describe 'when a company fetches another companys subscription' do
    before do
      @company = FactoryGirl.create :company
      @company_2 = FactoryGirl.create :company

      get :show, {id: @company_2.product_subscriptions.first.id, format: :json}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end

    let(:result) { JSON.parse response.body }

    it ('should return access denied') { expect(response.status).to eql(403)}
  end

  describe 'when signing up to a new subscription' do
    before do
      Timecop.freeze
      @company = FactoryGirl.create :company_without_subscription
      @product = FactoryGirl.create :product
      get :signup_subscription, {id: @product.id, format: :json}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
      @company.reload
    end

    after { Timecop.return }

    let(:subscription) { ProductSubscription.last }
    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should create a new subscription') { expect(subscription).not_to be_nil }
    it('should set the renewed_at to the start of the day') { expect(subscription.renewed_at).to be_the_same_time_as(Time.now.beginning_of_day) }
    it('should set next renewal to be in 30 days from now, at end of the day') { expect(subscription.next_renewal_at).to be_the_same_time_as(30.days.from_now.end_of_day) }
    it('should return the company') { expect(result['company']['id']).to eql(@company.id) }
    it('should set the company subscription status to active') { expect(@company.subscription_status).to eq('active') }
  end

  describe 'when canceling a subscription' do
    before do
      @company = FactoryGirl.create :company
      @subscription = @company.product_subscriptions.last
      @next_renewal_at = @subscription.next_renewal_at
      get :cancel_subscription, {id: @subscription.id, format: :json}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
      @subscription.reload
    end

    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should set the expire date as the next renewal date') { expect(@subscription.expires_at).to be_the_same_time_as(@next_renewal_at) }
    it('should nil the next renewal date') { expect(@subscription.next_renewal_at).to be_nil }
    it('should return the company') { expect(result['company']['id']).to eql(@company.id) }
  end

  describe 'when changing the product of a subscription' do
    before do
      @company = FactoryGirl.create :company
      # Set the creation date of the subscription, to now. As we need the invoice line item, to match the creation date.
      @company.product_subscriptions.first.update_attributes(renewed_at: Time.now, next_renewal_at: 30.days.from_now)
      @company.active_invoice.add_line_item(@company.product_subscriptions.first)

      Timecop.freeze(3.days.from_now)
      @product = FactoryGirl.create :product
      get :change_subscription_product, {id: @company.product_subscriptions.first.id, format: :json, product_subscription: {product_id: @product.id}}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end

    after { Timecop.return }

    let(:subscription) { @company.product_subscriptions.last.reload }
    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should update the product of the subscription') { expect(subscription.product).to eq(@product) }
    it('should return the company') { expect(result['company']['id']).to eql(@company.id) }
    it('should have added a new line item to companies invoice') do
      expect(@company.active_invoice.invoice_line_items.count).to eql(2)
    end
    it('invoice should days quantity should still equal 30 days') do
      total_quantity = @company.active_invoice.invoice_line_items.inject(0) { |sum, li| sum + li.quantity }
      expect(total_quantity).to eql(30)
    end
  end

  describe '#change_subscription_product' do
    let(:company) { create(:company) }
    let(:ps) { company.product_subscriptions.first}

    describe 'When company changes product, multiple times in one day' do
      before do
        ps.update_attributes(renewed_at: Time.now, next_renewal_at: 30.days.from_now)
        company.active_invoice.add_line_item(ps)

        product_1 = create(:product, name: 'Medium', price: 750)
        product_2 = create(:product, name: 'Big', price: 1200)

        Timecop.freeze(1.hour.from_now)
        get :change_subscription_product, {id: ps.id, format: :json, product_subscription: {product_id: product_1.id}}, {'Authorization' => "Bearer #{company.session_api_key.access_token}"}

        Timecop.freeze(1.hour.from_now)
        get :change_subscription_product, {id: ps.id, format: :json, product_subscription: {product_id: product_2.id}}, {'Authorization' => "Bearer #{company.session_api_key.access_token}"}

        Timecop.freeze(1.hour.from_now)
        get :change_subscription_product, {id: ps.id, format: :json, product_subscription: {product_id: product_1.id}}, {'Authorization' => "Bearer #{company.session_api_key.access_token}"}
      end

      after { Timecop.return }

      it('should have 4 invoice line items') { expect(company.active_invoice.invoice_line_items.count).to eql(4) }
      it 'should still consist off 30 days total' do
        total_quantity = company.active_invoice.invoice_line_items.inject(0) { |sum, li| sum + li.quantity }
        expect(total_quantity).to eql(30)
      end
      it 'should still have a total of 750' do
        total_total = company.active_invoice.invoice_line_items.inject(0) { |sum, li| sum + li.total }
        expect(total_total).to eql(750.00)
      end
    end

    describe 'When company changes product multiple times over a month' do
      before do
        ps.update_attributes(renewed_at: Time.now, next_renewal_at: 30.days.from_now)
        company.active_invoice.add_line_item(ps)

        product_1 = create(:product, name: 'Medium', price: 750)
        product_2 = create(:product, name: 'Big', price: 1200)

        Timecop.freeze(10.days.from_now)
        get :change_subscription_product, {id: ps.id, format: :json, product_subscription: {product_id: product_1.id}}, {'Authorization' => "Bearer #{company.session_api_key.access_token}"}

        Timecop.freeze(5.days.from_now)
        get :change_subscription_product, {id: ps.id, format: :json, product_subscription: {product_id: product_2.id}}, {'Authorization' => "Bearer #{company.session_api_key.access_token}"}

        Timecop.freeze(5.days.from_now)
        get :change_subscription_product, {id: ps.id, format: :json, product_subscription: {product_id: product_1.id}}, {'Authorization' => "Bearer #{company.session_api_key.access_token}"}
      end

      it('should contain 4 line items') { expect(company.active_invoice.invoice_line_items.count).to eql(4) }
      it 'should still consist off 30 days total' do
        total_quantity = company.active_invoice.invoice_line_items.inject(0) { |sum, li| sum + li.quantity }
        expect(total_quantity).to eql(30)
      end
      it 'should still have a total 741.67' do
        total_total = company.active_invoice.invoice_line_items.inject(0) { |sum, li| sum + li.total }
        expect(total_total).to eql(741.67)
      end
    end
  end

  describe 'when reactivating a subscription' do
    before do
      @company = FactoryGirl.create :company
      @company.product_subscriptions << create(:canceled_subscription)
      @subscription = @company.product_subscriptions.last
      @expires_at = @subscription.expires_at
      get :reactivate_subscription, {id: @subscription.id, format: :json }, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
      @subscription.reload
    end

    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should set the next renewal date as the expire date') { expect(@subscription.next_renewal_at).to be_the_same_time_as(@expires_at) }
    it('should nil the expires_at date') { expect(@subscription.expires_at).to be_nil }
    it('should return the company') { expect(result['company']['id']).to eql(@company.id) }
  end
end
