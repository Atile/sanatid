require 'spec_helper'

describe Api::ProductFamiliesController do
  describe 'when a user fetches all product families' do
    before do
      @pf = FactoryGirl.create :product_family_with_products
      get :index, { format: :json }
    end

    let(:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should contain the family') { expect(result['product_families'][0]['id']).to eql(@pf.id) }
    it('should contain the family products') { expect(result['products'].count).to eql(@pf.products.count) }
  end
end

