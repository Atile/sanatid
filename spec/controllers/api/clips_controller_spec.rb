require 'spec_helper'

describe Api::ClipsController do

  describe 'when company is creating a new clip' do
    before do
      create(:email, machine_name: 'clip_confirmation')
      @job = FactoryGirl.create :accepted_job
      @customer = @job.customer
      @company = @job.winning_offer.company
      @voucher = @job.winning_offer.voucher
      @voucher.update_attributes customer: @customer
      @current_clip_count = @voucher.clips.count
      post :create, {format: :json, clip: {status: 'confirmed', voucher_id: @voucher.id}}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end

    let (:new_clip) { Clip.last }

    it('should persist clip') { expect(Clip.count).to eql(@current_clip_count + 1) }
    it('status should be default state') { expect(new_clip.status).to eql('undecided')}
    it('should return success') { expect(response.status).to eql(201) }
    it('should belong to the correct voucher') { expect(new_clip.voucher).to eql(@voucher) }
    it('should send an email to the customer') do
      expect(VoucherMailer).to receive(:clip_confirmation).with(instance_of Clip).and_call_original
      post :create, {format: :json, clip: {status: 'confirmed', voucher_id: @voucher.id}}, {'Authorization' => "Bearer #{@company.session_api_key.access_token}"}
    end
  end

  describe 'when a customer list its clips' do
    before do
      @job = FactoryGirl.create :accepted_job
      @customer = @job.customer
      @voucher = @job.winning_offer.voucher
      @voucher.customer = @customer
      @voucher.save

      post :index, {format: :json}, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end

    let (:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should contain all clips associated to customer') { expect(result['clips'].count).to eql(Clip.joins(:voucher).where(vouchers: { customer_id: @customer }).count) }
  end

  describe 'when a customer list its undecided clips' do
    before do
      @job = FactoryGirl.create :accepted_job
      @customer = @job.customer
      @voucher = @job.winning_offer.voucher
      @voucher.customer = @customer
      @voucher.clips << Clip.new(status: 'confirmed')
      @voucher.save

      post :index, {format: :json, filter: 'undecided'}, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end

    let (:result) { JSON.parse response.body }

    it('should return success') { expect(response.status).to eql(200) }
    it('should only return clips which is undecided') do

      result['clips'].reject! { |clip| clip['status'] == 'undecided' }
      expect(result['clips'].count).to eql(0)
    end
  end


  describe 'when a customer confirms/decline a clip' do
    before do
      @job = FactoryGirl.create :accepted_job
      @customer = @job.customer
      @voucher = @job.winning_offer.voucher
      @voucher.customer = @customer
      @voucher.save
      @clip_to_be_confirmed = @voucher.clips.first

      post :update, {id: @clip_to_be_confirmed.id, format: :json, clip: {status: 'confirmed', voucher_id: 123}}, {'Authorization' => "Bearer #{@customer.session_api_key.access_token}"}
    end

    let (:updated_clip) { @clip_to_be_confirmed.reload }

    it('should return success') { expect(response.status).to eql(200) }
    it('should change the staus of the clip to confirmed') { expect(updated_clip.status).to eql('confirmed') }
    it('should not have updated the voucher id, provided within the params') { expect(updated_clip.voucher).to eql(@voucher) }
  end
end
