# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    name "Sanatid medlemskab lille"
    description "Medlemskab af sanatid"
    price 500.0
    duration nil
    commission 5
    active true
    billing_period "monthly"
    fundamental true
  end
end
