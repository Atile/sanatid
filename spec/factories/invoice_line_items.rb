# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice_line_item do
    quantity 5
    quantity_type 'days'
    total 100.00
    tax true
    currency 'dkk'
    description '5 days of something'
  end
end
