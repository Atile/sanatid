# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :page do
    title 'Test page'
    content 'With some awesome test content'
    published true
  end
end
