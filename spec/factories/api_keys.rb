# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :api_key do
    scope "session"
    expired_at "2013-11-30 12:58:09"
    created_at "2013-11-30 12:58:09"
  end
end
