FactoryGirl.define do
  factory :message do
    content 'Some important message, concerning a job'
  end
end
