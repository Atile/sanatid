# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :service_field do
    name "Test coaching"
    meta_description 'A great search optimized description of test coaching'
    description "something awesome about test coaching"
    content "Some awesome content about test coaching, holy fuck this is great"
    published true

    after(:build) do |sf|
      sf.services << build(:service)
    end
  end
end
