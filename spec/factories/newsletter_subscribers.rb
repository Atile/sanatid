FactoryGirl.define do
  factory :newsletter_subscriber do
    sequence :email do |n|
      "newsletter_test_#{n}@test.com"
    end
  end
end
