FactoryGirl.define do
  factory :company do
    name 'Elvar'
    company_name 'Elvar A/S'
    slug 'elvar-a-s'
    sequence :email do |n|
      "test_company_#{n}@sanatid.dk"
    end
    phone 88888888
    password 'test'
    password_confirmation 'test'
    address 'Rønnebærvej 5'
    latitude 55.69377
    longitude 12.512678
    subscription_status 'active'

    after(:build) do |company|
      company.services = [build(:service)]
      company.product_subscriptions << build(:active_subscription)
      company.company_pages << create(:company_page)
      company.init_invoice
    end

    factory :company_without_subscription do
      subscription_status 'passive'
      after(:build) do |company|
        company.product_subscriptions = []
      end
    end
  end
end
