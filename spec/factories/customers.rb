FactoryGirl.define do
  factory :customer do
    name 'Elvar'
    sequence :email do |n|
      "test_customer_#{n}@sanatid.dk"
    end
    phone 88888888
    password 'awesomepassword'
    password_confirmation 'awesomepassword'
  end
end
