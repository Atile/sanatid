# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :review do
    rating 5
    content 'This is some test content for a review'
  end
end
