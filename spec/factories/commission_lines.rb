# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :commission_line do
    total 399.99
    percentage 10
    tax true
    currency 'dkk'
    company
  end
end
