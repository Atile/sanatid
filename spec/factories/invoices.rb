# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice do
    state 'open'
    start_date Time.now
    end_date 30.days.from_now
    due_date nil

    factory :invoice_with_items do
      after(:build) do |invoice|
        invoice.invoice_line_items << create(:invoice_line_item)
      end
    end
  end
end
