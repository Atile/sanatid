# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :submission do
    form 'home.contact'
    message 'My text message'
    email 'test@email.com'
    subject 'awesome test mode'
    name 'Martin Elvar'
  end
end
