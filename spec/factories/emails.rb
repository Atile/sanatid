# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :email do
    title 'Test email'
    subject 'test subject'
    content 'test content'
  end
end
