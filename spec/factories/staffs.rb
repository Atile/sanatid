# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :staff do
    name 'David Hammer'
    email 'hammer@elvar.dk'
    phone 89898989
    password '123'
    password_confirmation '123'
  end
end
