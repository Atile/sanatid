# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_subscription do
    active true

    after(:build) do |ps|
      ps.product = build(:product)
    end

    factory :active_subscription do
      renewed_at 3.days.ago.beginning_of_day
      next_renewal_at 1.month.from_now.end_of_day - 3.day
    end

    factory :canceled_subscription do
      renewed_at 20.days.ago.beginning_of_day
      next_renewal_at nil
      expires_at 10.days.from_now.end_of_day
    end

    factory :inactive_subscription do
      expires_at 5.days.ago.end_of_day
      next_renewal_at nil
      active false
    end
  end
end
