# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company_page do
    title 'My funky test page about coaching'
    content 'come have some'
    weight 1
  end
end
