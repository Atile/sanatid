# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :service do
    name 'Test service'
    slug 'test-service'
    meta_description 'Load test description'
    description 'Some awesome description about test service'
    content 'some test content for this particular test service'
    published true
  end
end
