# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name 'Elvar'
    email 'my@testmail.com'
    phone 88888888
    password 'awesomepassword'
    password_confirmation 'awesomepassword'
  end
end
