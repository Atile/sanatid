FactoryGirl.define do
  factory :blog do
    title 'test title'
    summary 'test summary'
    content 'test content'
    slug 'test-slug'
    meta_description 'test meta description'
    published true
  end
end
