# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product_family do
    name 'Test product family'
    description 'This is some informative description, of this product family'

    factory :product_family_with_products do
      after(:create) do |pf|
        (0..2).each { pf.products << create(:product) }
      end
    end
  end
end
