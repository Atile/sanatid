require 'spec_helper'

RSpec.describe Company, :type => :model do
  describe '#init_invoice' do
    let(:company) { create(:company) }

    context 'when creating a new invoice, due to a new fundamental sub' do
      before do
        Timecop.freeze
        company.product_subscriptions << create(:active_subscription)
        company.init_invoice
      end

      after { Timecop.return }

      let(:sub) { company.product_subscriptions.first }
      let(:invoice) {company.invoices.last}

      it('should create a new invoice') { expect(invoice.id).to be_present }
      its('start and end date should match the one of the subscription') do
        expect(invoice.start_date).to be_the_same_time_as(sub.renewed_at.beginning_of_day)
        expect(invoice.end_date).to be_the_same_time_as(30.days.from_now.end_of_day)
      end
    end

    context 'when creating a new invoice, for another period' do
      before do
        company.product_subscriptions << create(:active_subscription)
        company.init_invoice
        Timecop.freeze(31.days.from_now)
        company.init_invoice
      end

      after { Timecop.return }

      let(:invoice) {company.invoices.last}

      it('should create a new invoice') { expect(invoice.id).to be_present }
      it('should create another invoice') { expect(company.invoices.count).to eq(2) }
      its('start and end date, should be based of the previous invoice') do
        first = company.invoices.first
        expect(invoice.end_date).to eql(first.end_date + 31.days)
      end
      its('start date should be the day after the previous invoice ended') do
        first = company.invoices.first
        expect(invoice.start_date).to eql(first.end_date + 1.day)
      end
    end
  end

  describe '#active invoice' do
    let(:company) { create(:company) }

    context 'when accessing invoice' do
      before do
        company.product_subscriptions << create(:active_subscription)
        company.init_invoice
        Timecop.freeze(31.days.from_now)
        @correct_invoice = company.init_invoice
      end

      it('should return the correct invoice') { expect(company.active_invoice).to eq(@correct_invoice) }
    end

    context 'when accessing invoice, with no current invoice' do
      before do
        company.product_subscriptions << create(:active_subscription)
      end

      let(:invoice) { company.active_invoice }

      it('should create a new invoice') { expect(invoice.id).to be_present }
    end
  end

  describe '#find_by_frontpage_promotion' do
    let(:company) { create(:company) }
    let(:unpromoted_company) { create(:company) }

    context 'when finding frontpage promoted companies' do
      before do
        product = create(:product, handle: 'frontpage-promotion-1')
        subscription = create(:active_subscription)
        subscription.update_attributes(product: product)
        company.product_subscriptions << subscription
      end

      let(:promotions) { Company.find_by_frontpage_promotion(1, 2) }

      it('should return the promoted company') { expect(promotions.include?(company)).to be_truthy }
      it('should not display unpromoted company') { expect(promotions.include?(unpromoted_company)).to be_falsey }
    end
  end

  describe '#find_by_flow_promotion' do
    let(:company) { create(:company) }
    let(:unpromoted_company) { create(:company) }

    context 'when finding frontpage promoted companies' do
      before do
        product = create(:product, handle: 'flow-promotion-1')
        subscription = create(:active_subscription)
        subscription.update_attributes(product: product)
        company.product_subscriptions << subscription
      end

      let(:promotions) { Company.find_by_flow_promotion(1, 2, [55.69377, 12.512678]) }

      it('should return the promoted company') { expect(promotions.include?(company)).to be_truthy }
      it('should not display unpromoted company') { expect(promotions.include?(unpromoted_company)).to be_falsey }
    end
  end
end
