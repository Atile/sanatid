require 'spec_helper'

describe Review do
  before do
    @job = create(:job)
    @review = build(:review, job: @job)
  end

  it 'should yield a error, if the provided rating is not between 1 and 6' do
    @review.rating = 8
    expect(@review.save).to be_falsey
  end

  it 'should yield a error, if the provided job already have a associated review' do
    create :review, rating: 5, job: @job
    @job.reload

    expect(@review.save).to be_falsey
  end
end
