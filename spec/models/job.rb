require 'spec_helper'

describe Job do
  before { @job = FactoryGirl.create :job, address: 'Rønnebærvej 5, 2400 København NV' }

  subject { @job }

  describe 'when a job is fetched by address' do
    describe 'when tried fetched from within 10 hours, and withing 5 km' do
      let(:fetched) { Job.near_address('Sallingvej 64, 2720 Vanløse') }

      it('should') { fetched.count.should eq(1)}
    end

    describe 'when tried fetched from within 10 hours, and withing 14 km' do
      before { sleep 1 }
      let(:fetched) { Job.near_address('Baltorpbakken')}

      it('should') { fetched.should eq([])}
    end

    describe 'when tried fetched from within 12 hours, and withing 16 km' do
      before do
        Timecop.travel(14.hours.from_now)
        sleep 1
        @job = FactoryGirl.create :job, customer: FactoryGirl.create(:customer, email: 'newdistinct@shitmail.com')
      end

      after do
        Timecop.return
      end

      let(:fetched) { Job.near_address('Horsbred') }

      it('should fetch the job') { fetched.count.should eq(1) }
    end

    describe 'when tried fetched from within 12 hours, and withing 24 km' do
      before do
        sleep 1
      end

      let(:fetched) { Job.near_address('Vassingerødvej') }

      it('should not fetch the job') { fetched.should eq([]) }
    end

    describe 'when tried fetched from within 2 days, and withing 26 km' do
      before do
        Timecop.travel(37.hours.from_now)
        sleep 1
        @job = FactoryGirl.create :job, customer: FactoryGirl.create(:customer, email: 'newdistinct2341783876@shitmail.com')
      end

      after do
        Timecop.return
      end

      let(:fetched) { Job.near_address('Husmandsvej, Taastrup, Danmark') }

      it('should fetch the job') { fetched.count.should eq(1) }
    end

    describe 'when tried fetched from within 12 hours, and withing 34 km' do
      before do
        sleep 1
      end

      let(:fetched) { Job.near_address('Kamstrupvej, Roskilde') }

      it('should not fetch the job') { fetched.should eq([]) }
    end

    describe 'when tried fetched from within 4 days, and withing 50 km' do
      before do
        Timecop.travel(52.hours.from_now)
        sleep 1
        @job = FactoryGirl.create :job, customer: FactoryGirl.create(:customer, email: 'newdistinct2341783876@shitmail.com')
      end

      after do
        Timecop.return
      end

      let(:fetched) { Job.near_address('Høbjergvej, Helsinge, Danmark') }

      it('should fetch the job') { fetched.count.should eq(1) }
    end

    describe 'when tried fetched from within 12 hours, and withing 24 km' do
      before do
        sleep 1
      end

      let(:fetched) { Job.near_address('Nordmarksvej, Sorø, Danmark') }

      it('should not fetch the job') { fetched.should eq([]) }
    end
  end
end