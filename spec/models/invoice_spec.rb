require 'spec_helper'

RSpec.describe Invoice, :type => :model do
  describe '#add_line_item' do
    let(:invoice) { create(:invoice) }
    let(:ps) { create(:active_subscription, renewed_at: DateTime.now, next_renewal_at: 30.days.from_now)}

    context 'when adding a new line item' do
      before do
        invoice.add_line_item(ps)
      end

      let(:line_item) { invoice.invoice_line_items.last }

      it('should match quantity with the length of the product') { expect(line_item.quantity).to eql(ps.product.billing_period_in_days) }
      it('should have a total, that matches the total of the product') { expect(line_item.total.to_d).to eql(ps.product.price) }
    end

    context 'when adding a new line item, in the middle of a period' do
      before do
        ps.update_attributes(renewed_at: 15.days.ago, next_renewal_at: 15.days.from_now)
        invoice.add_line_item(ps)
      end

      let(:line_item) { invoice.invoice_line_items.last }
      let(:remaining_total) { ((ps.product.price / ps.product.billing_period_in_days) * 15).round(2) }

      it('should have a quantity of 15') { expect(line_item.quantity).to eql(15) }
      its('total should match the remaining period') { expect(remaining_total).to eql(line_item.total.to_d) }
    end
  end

  describe '#replace_line_item' do
    let(:invoice) { create(:invoice) }
    let(:ps) { create(:active_subscription, renewed_at: DateTime.now, next_renewal_at: 30.days.from_now)}

    context 'when replacing a line item' do
      before do
        invoice.add_line_item(ps)
        Timecop.freeze(5.days.from_now)
        invoice.replace_line_item(invoice.invoice_line_items.last)
      end

      after { Timecop.return }

      let(:line_item) { invoice.invoice_line_items.last }
      let(:new_total) { ((ps.product.price / ps.product.billing_period_in_days) * 5).round(2) }

      its('total should match 5 days of subscription') { expect(line_item.total.to_d).to eql(new_total) }
      its('quantity should equal 5 days') { expect(line_item.quantity).to eql(5) }
    end
  end

  describe '#send_invoices' do
    context 'when sending out invoices' do
      let(:invoice) { create(:invoice_with_items, start_date: 31.days.ago, end_date: 1.day.ago, company: create(:company)) }

      before do
        create(:email, machine_name: 'send_invoice')
        invoice.reload
      end

      it('should set due date, and reference id') do
        Invoice.send_invoices
        invoice.reload

        expect(invoice.due_date).not_to be_nil
        expect(invoice.reference_id).not_to be_nil
      end
      it('should send a mail') do
        expect(InvoiceMailer).to receive(:send_invoice).and_call_original
        Invoice.send_invoices
      end
      it('should not send the same invoice more than once') do
        Invoice.send_invoices
        expect(InvoiceMailer).not_to receive(:send_invoice).and_call_original
        Invoice.send_invoices
      end
    end
  end
end
