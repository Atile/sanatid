require 'spec_helper'

describe ProductSubscription do
  describe '#refrest_subscriptions' do
    let(:company) { create(:company_without_subscription, subscription_status: 'active') }

    context 'when a subscription is expired' do
      before do
        company.product_subscriptions << create(:inactive_subscription, active: true)
        ProductSubscription.refresh_subscriptions
        company.reload
      end

      let(:subscription) { company.product_subscriptions.last }

      it('should set the status of the subscription to inactive') { expect(subscription.active).to be_falsey }
      it('should set the subscription status of the company to expired') { expect(company.subscription_status).to eq('expired') }
    end

    context 'when a subscription is due to renewal' do
      before do
        Timecop.freeze
        company.product_subscriptions << create(:active_subscription, renewed_at: 31.days.ago.beginning_of_day, next_renewal_at: 1.day.ago.end_of_day)
        ProductSubscription.refresh_subscriptions
        company.reload
      end

      after { Timecop.return }

      let(:subscription) { company.product_subscriptions.last }

      it('should set the renewed_at date to the previous next_renewal_at date') { expect(subscription.renewed_at).to be_the_same_time_as(1.day.ago.end_of_day) }
      it('should set the next_renewal_at to match the billing period of the product') { expect(subscription.next_renewal_at).to be_the_same_time_as(1.day.ago.end_of_day + 30.days) }
      it('should add a new line item, to companys invoice') { expect(company.active_invoice.invoice_line_items.count).to eql(1) }
    end
  end

  describe '#collect_monthly_commission' do
    let(:job) { create(:accepted_job) }
    let(:company) { job.winning_offer.company }

    context 'when a job is completed, and commission is ran' do
      let(:unaccepted_job) { create(:job) }

      before do
        create(:offer, company: company, job: unaccepted_job)
        ProductSubscription.collect_monthly_commission
        unaccepted_job.reload
        company.reload
      end

      it('should add a commission line companys invoice') { expect(company.active_invoice.commission_lines.count).to eq(1) }
      it('should not collect commission of unaccepted job') { expect(unaccepted_job.commission_line).to be_nil }
      it('should not add the same line twice, if ran twice') do
        ProductSubscription.collect_monthly_commission
        company.reload
        expect(company.active_invoice.commission_lines.count).to eq(1)
      end
    end
  end
end
