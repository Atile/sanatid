require 'spec_helper'

describe ApiKey do
  before {  @customer = FactoryGirl.create :customer; @api_key = @customer.api_keys.session.create }

  describe 'When a access token is generated' do
    let(:api_key) { ApiKey.create(scope: 'session', user_id: @customer.id) }

    it 'should not be a new record' do
      expect(api_key.new_record?).to be false
    end

    it 'should consist of 32 chars' do
      expect(api_key.access_token).to match /\S{32}/
    end
  end

  describe 'When a token is expired within the session scope' do
    let(:api_key) { ApiKey.create(scope: 'session', user_id: @customer.id) }

    it 'should be expired after 5 hours' do
      expect(api_key.expired_at).to be_within(5.hours).of(Time.now)
    end
  end

  describe 'When a token is expired within the api scope' do
    let(:api_key) { ApiKey.create(scope: 'api', user_id: @customer.id) }

    it 'should be expired after 30 days' do
      expect(api_key.expired_at).to be_within(30.days).of(Time.now)
    end
  end
end
