require 'spec_helper'

describe Customer do
  before { @customer = FactoryGirl.create :customer }

  subject { @customer }

  describe 'When customer gets an access token' do
    let(:api_key) { @customer.session_api_key }

    it 'should have reference the correct customer' do
      api_key.user_id.should eq(@customer.id)
    end

    it 'should consist of 32 chars' do
      api_key.access_token.should =~ /\S{32}/
    end
  end
end
