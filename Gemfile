source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.5'

group :doc do
  gem 'sdoc', require: false
end

group :test, :development do
  gem 'debugger'
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'meta_request'
  gem 'fuubar', '2.0.0.rc1'
  gem 'phantomjs', '>= 1.8.1.1'
  gem 'turbo_dev_assets'
  gem 'hirb'
end

group :test do
  gem 'simplecov', require: false
  gem 'poltergeist', git: 'https://github.com/jonleighton/poltergeist.git'
  gem 'capybara', '2.2.0'
  gem 'launchy'
  gem 'selenium-webdriver'
  gem 'timecop'
  gem 'fakeweb'
  gem 'email_spec'
  gem 'rspec-its'
end

gem 'newrelic_rpm'
gem 'pg'
gem 'compass-rails'
gem 'sass-rails', '~> 4.0.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.1'
gem 'autoprefixer-rails'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'bcrypt-ruby', '~> 3.1.0'
gem 'less-rails'
gem 'semantic-ui-rails'
gem 'ember-rails', '~> 0.14.1'
gem 'active_model_serializers'
gem 'geocoder'
gem 'rmagick', :require => 'RMagick'
gem 'carrierwave'
gem 'therubyracer', platforms: :ruby
gem 'rack-cors', :require => 'rack/cors'
gem 'whenever', require: false
gem 'daemons'
gem 'delayed_job_active_record'
gem 'jquery-fileupload-rails'
gem 'kaminari'
gem 'mechanize'
gem 'nokogiri'
gem 'gibbon'
gem 'leaflet-rails'
gem 'leaflet-markercluster-rails'
gem 'dalli'
gem 'wkhtmltopdf-binary'
gem 'wicked_pdf'
gem 'sanitize'
gem 'redcarpet'
gem 'bower-rails'
gem 'font-awesome-rails'
gem 'gridle'

group :development do
  gem 'rack-mini-profiler', require: false
  gem 'bullet'
  gem 'capistrano',  '~> 3.1'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-rvm'
end